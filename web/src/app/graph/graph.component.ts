import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';

import * as CanvasJS from '../../assets/canvasjs.min';


export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY-MM',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};


@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.sass']
})
export class GraphComponent implements OnInit {

  dt = new FormControl('')

  tempList = [];
  humList = [];
  soilList = [];

  chartTempHum
  chartSoil

  farm

  subFarm
  

  csvTemp = []
  csvSoil = []
  constructor(public http: HttpClient , public route: ActivatedRoute) { 
    this.route.params.subscribe(params => {
      console.log("params" + JSON.stringify(params))

      this.farm = params['farm']
      this.subFarm = params['sub']
    })

  }

  ngOnInit(): void {
    this.setUp();
  }


  export(){

    this.downloadFile(this.csvTemp);
    this.downloadFileSoil(this.csvSoil);
  }

  setUp(){

    this.chartTempHum = new CanvasJS.Chart("chartTempHum", {
      animationEnabled: true,
      exportEnabled: true,
      zoomEnabled: true,
     

      legend: {
        cursor: "pointer",
        verticalAlign: "top",
        horizontalAlign: "center",
        dockInsidePlotArea: true,
        itemclick: (e) => {

          if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
          } else {
            e.dataSeries.visible = true;
          }
          this.chartTempHum.render();

        }
      },

      data: [
        {
          color: "blue",
          type: "spline",
          name: "อุณหภูมิ",
          showInLegend: true,
          markerSize: 10,
          yValueFormatString: "# °C",
          dataPoints: this.tempList
        },

        {
          color: "green",
          type: "spline",
          name: "ความชื้นในอากาศ",
          showInLegend: true,
          markerSize: 10,
          yValueFormatString: "# °C",
          dataPoints: this.humList
        },


      ]
    });

    this.chartTempHum.render();


    this.chartSoil = new CanvasJS.Chart("chartSoil", {
      animationEnabled: true,
      exportEnabled: true,
      zoomEnabled: true,
     

      legend: {
        cursor: "pointer",
        verticalAlign: "top",
        horizontalAlign: "center",
        dockInsidePlotArea: true,
        itemclick: (e) => {

          if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
          } else {
            e.dataSeries.visible = true;
          }
          this.chartSoil.render();

        }
      },

      data: [

        {
          color: "orange",
          type: "spline",
          name: "ความชื้นในดิน",
          showInLegend: true,
          markerSize: 10,
          yValueFormatString: "# ",
          dataPoints: this.soilList
        },

      ]
    });

    this.chartSoil.render();

   
  }

  dateSelect() {

    this.tempList = [];
    this.humList = [];
    this.soilList = [];



    this.setUp();


    this.http.get("http://139.59.125.83:3122/log/day/temp/" + this.farm  + "/" + this.subFarm + "/" +  moment(this.dt.value).format('YYYY-MM-DD'), {})

      .subscribe(

        data => {

          console.log(JSON.stringify(data))

                  

          if (data["status"] == 200) {


            this.csvTemp = data["message"];
            for (var i = 0; i < Object.keys(data["message"]).length; i++) {

              var rawTemp = {};
              var rawHum = {};

            
              rawTemp["label"] = data["message"][i]["dtf"]
              rawTemp["y"] = data["message"][i]["temp"];
              this.tempList.push(rawTemp);

              rawHum["label"] = data["message"][i]["dtf"]
              rawHum["y"] = data["message"][i]["hum"];
              this.humList.push(rawHum);

            }


            this.chartTempHum.data[0] = this.tempList;
            this.chartTempHum.data[1] = this.humList;
            this.chartTempHum.render();

          }
        }

      )

      this.http.get("http://139.59.125.83:3122/log/day/soil/" + this.farm  + "/" + this.subFarm + "/" +  moment(this.dt.value).format('YYYY-MM-DD'), {})

      .subscribe(

        data => {

          console.log(JSON.stringify(data))

                
          if (data["status"] == 200) {


            this.csvSoil = data["message"];
            for (var i = 0; i < Object.keys(data["message"]).length; i++) {

              var rawSoil = {};

              rawSoil["label"] = data["message"][i]["dtf"]
              rawSoil["y"] = data["message"][i]["soil"];
              this.soilList.push(rawSoil);

            }

            this.chartSoil.data[0] = this.soilList;
            this.chartSoil.render();

          }
        }

      )




  }

  monthSelect(){

    this.tempList = [];
    this.humList = [];
    this.soilList = [];



    this.setUp();


    this.http.get("http://139.59.125.83:3122/log/month/temp/" + this.farm  + "/" + this.subFarm + "/" +  moment(this.dt.value).format('YYYY-MM-01'), {})

      .subscribe(

        data => {

          console.log(JSON.stringify(data))

                  

          if (data["status"] == 200) {


            this.csvTemp = data["message"];
            for (var i = 0; i < Object.keys(data["message"]).length; i++) {

              var rawTemp = {};
              var rawHum = {};

            
              rawTemp["label"] = data["message"][i]["dtf"]
              rawTemp["y"] = data["message"][i]["temp"];
              this.tempList.push(rawTemp);

              rawHum["label"] = data["message"][i]["dtf"]
              rawHum["y"] = data["message"][i]["hum"];
              this.humList.push(rawHum);

            }


            this.chartTempHum.data[0] = this.tempList;
            this.chartTempHum.data[1] = this.humList;
            this.chartTempHum.render();

          }
        }

      )

      this.http.get("http://139.59.125.83:3122/log/month/soil/" + this.farm  + "/" + this.subFarm + "/" +  moment(this.dt.value).format('YYYY-MM-01'), {})

      .subscribe(

        data => {

          console.log(JSON.stringify(data))

                
          if (data["status"] == 200) {


            this.csvSoil = data["message"];
            for (var i = 0; i < Object.keys(data["message"]).length; i++) {

              var rawSoil = {};

              rawSoil["label"] = data["message"][i]["dtf"]
              rawSoil["y"] = data["message"][i]["soil"];
              this.soilList.push(rawSoil);

            }

            this.chartSoil.data[0] = this.soilList;
            this.chartSoil.render();

          }
        }

      )

  }

  yearSelect() {

    this.tempList = [];
    this.humList = [];
    this.soilList = [];



    this.setUp();


    this.http.get("http://139.59.125.83:3122/log/year/temp/" + this.farm  + "/" + this.subFarm + "/" +  moment(this.dt.value).format('YYYY'), {})

      .subscribe(

        data => {

          console.log(JSON.stringify(data))

                  

          if (data["status"] == 200) {


            this.csvTemp = data["message"];
            for (var i = 0; i < Object.keys(data["message"]).length; i++) {

              var rawTemp = {};
              var rawHum = {};

            
              rawTemp["label"] = data["message"][i]["dtf"]
              rawTemp["y"] = data["message"][i]["temp"];
              this.tempList.push(rawTemp);

              rawHum["label"] = data["message"][i]["dtf"]
              rawHum["y"] = data["message"][i]["hum"];
              this.humList.push(rawHum);

            }


            this.chartTempHum.data[0] = this.tempList;
            this.chartTempHum.data[1] = this.humList;
            this.chartTempHum.render();

          }
        }

      )

      this.http.get("http://139.59.125.83:3122/log/year/soil/" + this.farm  + "/" + this.subFarm + "/" +  moment(this.dt.value).format('YYYY'), {})

      .subscribe(

        data => {

          console.log(JSON.stringify(data))

                
          if (data["status"] == 200) {


            this.csvSoil = data["message"];
            for (var i = 0; i < Object.keys(data["message"]).length; i++) {

              var rawSoil = {};

              rawSoil["label"] = data["message"][i]["dtf"]
              rawSoil["y"] = data["message"][i]["soil"];
              this.soilList.push(rawSoil);

            }

            this.chartSoil.data[0] = this.soilList;
            this.chartSoil.render();

          }
        }

      )

  }


  downloadFileSoil(data, filename = 'log-soil' ) {
    let csvData = this.ConvertToCSV(data, ['soil', 'dtf']);
    console.log(csvData)
    let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
    let dwldLink = document.createElement("a");
    let url = URL.createObjectURL(blob);
    let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
    if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
      dwldLink.setAttribute("target", "_blank");
    }
    dwldLink.setAttribute("href", url);
    dwldLink.setAttribute("download", filename + ".csv");
    dwldLink.style.visibility = "hidden";
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink);
  }


  downloadFile(data, filename = 'log-temp' ) {
    let csvData = this.ConvertToCSV(data, ['temp', 'hum' , 'dtf']);
    console.log(csvData)
    let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
    let dwldLink = document.createElement("a");
    let url = URL.createObjectURL(blob);
    let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
    if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
      dwldLink.setAttribute("target", "_blank");
    }
    dwldLink.setAttribute("href", url);
    dwldLink.setAttribute("download", filename + ".csv");
    dwldLink.style.visibility = "hidden";
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink);
  }

  ConvertToCSV(objArray, headerList) {
    let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    let str = '';
    let row = 'S.No,';

    for (let index in headerList) {
      row += headerList[index] + ',';
    }
    row = row.slice(0, -1);
    str += row + '\r\n';
    for (let i = 0; i < array.length; i++) {
      let line = (i + 1) + '';
      for (let index in headerList) {
        let head = headerList[index];

        line += ',' + array[i][head];
      }
      str += line + '\r\n';
    }
    return str;
  }



}
