import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubFarmComponent } from './sub-farm.component';

describe('SubFarmComponent', () => {
  let component: SubFarmComponent;
  let fixture: ComponentFixture<SubFarmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubFarmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubFarmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
