import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-sub-farm',
  templateUrl: './sub-farm.component.html',
  styleUrls: ['./sub-farm.component.sass']
})
export class SubFarmComponent implements OnInit {

  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;

  data = [];
  dataList = [];
  filteredData = [];
  rows = [];
  columns = [

    { prop: "farm" },
    { prop: "subFarm" },
    { prop: "gt" },
    { prop: "gs" },
    { prop: "gf" },
    { prop: "gw" },
    { prop: "gl" },
  ];


  id

  constructor(public http: HttpClient, public route: ActivatedRoute, public router: Router) {

    this.route.params.subscribe(params => {
      console.log("params" + JSON.stringify(params))
      this.id = params['id']
      this.callAPI();
    })

  }


  ngOnInit(): void {
  }

  detail(sub) {

    this.router.navigateByUrl('/detail/' + this.id + "/" + sub);
  }

  callAPI() {



    this.http.get("http://127.0.0.1:3122/sub-farm/" + this.id, {})
      .subscribe(
        data => {


          for (var i = 0; i < Object.keys(data["message"]).length; i++) {

            var raw = {}

            raw['farm'] = data["message"][i]["id_farm"]
            raw['subFarm'] = data["message"][i]["id_sub"]
            raw['gt'] = data["message"][i]["status_t"] == true ? "ทำงาน" : "ไม่ทำงาน"
            raw['gs'] = data["message"][i]["status_s"] == true ? "ทำงาน" : "ไม่ทำงาน"
            raw['gf'] = data["message"][i]["status_f"] == true ? "ทำงาน" : "ไม่ทำงาน"
            raw['gw'] = data["message"][i]["status_w"] == true ? "ทำงาน" : "ไม่ทำงาน"
            raw['gl'] = data["message"][i]["status_l"] == true ? "ทำงาน" : "ไม่ทำงาน"

            this.dataList.push(raw);


          }
          this.data = this.dataList;
          this.filteredData = this.dataList;
          console.log(JSON.stringify(this.data))

        }
      )


  }

  filterDatatable(event) {
    // get the value of the key pressed and make it lowercase
    const val = event.target.value.toLowerCase();
    // get the amount of columns in the table
    const colsAmt = this.columns.length;
    // get the key names of each column in the dataset
    const keys = Object.keys(this.filteredData[0]);
    // assign filtered matches to the active datatable
    this.data = this.filteredData.filter(function (item) {
      // iterate through each row's column data
      for (let i = 0; i < colsAmt; i++) {
        // check for a match
        if (
          item[keys[i]]
            .toString()
            .toLowerCase()
            .indexOf(val) !== -1 ||
          !val
        ) {
          // found match, return true to add to result set
          return true;
        }
      }
    });

    console.log(JSON.stringify(this.data))
    // whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }


}
