import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-farm',
  templateUrl: './farm.component.html',
  styleUrls: ['./farm.component.sass']
})
export class FarmComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit(): void {
  }


  gotoSub(farm){
    this.router.navigateByUrl('/sub-farm/' +farm);
  }

}
