import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-noti',
  templateUrl: './noti.component.html',
  styleUrls: ['./noti.component.sass']
})
export class NotiComponent implements OnInit {

  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent
  rows = [];
  data = [];
  filteredData = [];
  dataList = []


  title = new FormControl("");
  form = new FormControl("");
  message = new FormControl("");

  columns = [

    { prop: "title" },
    { prop: "message" },
    { prop: "dt" },

  ];


  constructor(private http: HttpClient) { }

  ngOnInit(): void {

    this.callAPI();
  }

  save() {

    var data = {
      "title": this.title.value,
      "message": this.message.value,
      "form": this.form.value,

    }


    this.http.post("http://139.59.125.83:3122/notification", data)
      .subscribe(data => {

        this.dataList = [];
        this.data = [];
        this.filteredData = [];

        this.callAPI();

      })


  }

  callAPI() {

    this.http.get("http://139.59.125.83:3122/notification", {})

      .subscribe(

        data => {


          if (data["status"] == 204) {

            return;
          }
          var json = JSON.parse(JSON.stringify(data["message"]));
          for (var i = 0; i < Object.keys(json).length; i++) {

            var raw = {};

            raw["id"] = json[i]["id"];
            raw["title"] = json[i]["title"];
            raw["message"] = json[i]["message"]
            raw["dt"] = json[i]["dt"]

            this.dataList.push(raw);

          }

          this.data = this.dataList;
          this.filteredData = this.dataList;

        }
      )
  }

  access(value) {
    console.log(value);

    this.http.delete("http://139.59.125.83:3122/notification/" + value, {})

      .subscribe(

        data => {

          this.dataList = [];
          this.data = [];
          this.filteredData = [];

          this.callAPI()
        })
  }
  filterDatatable(event) {
    // get the value of the key pressed and make it lowercase
    const val = event.target.value.toLowerCase();
    // get the amount of columns in the table
    const colsAmt = this.columns.length;
    // get the key names of each column in the dataset

    console.log(JSON.stringify(this.filteredData));
    const keys = Object.keys(this.filteredData[0]);
    // assign filtered matches to the active datatable
    this.data = this.filteredData.filter(function (item) {
      // iterate through each row's column data
      for (let i = 0; i < colsAmt; i++) {
        // check for a match


        if (
          item[keys[i]]
            .toString()
            .toLowerCase()
            .indexOf(val) !== -1 ||
          !val
        ) {
          // found match, return true to add to result set
          return true;
        }
      }
    });

    console.log(JSON.stringify(this.data))
    // whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }


}
