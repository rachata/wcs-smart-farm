import { Component, OnInit } from '@angular/core';
import * as CanvasJS from '../../assets/canvasjs.min';
import { IMqttMessage, MqttService } from 'ngx-mqtt';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.sass']
})
export class DetailComponent implements OnInit {

  tempList = [];
  humList= [];
  soilList = [];


  private subscription: Subscription;
  public message: string;

  chart

  farm
  subFarm;

  temperature = 0;
  hum = 0;
  soil = 0;

  select(){

    this.router.navigateByUrl('/graph/' + this.farm + "/" + this.subFarm);

  }
  constructor(public router: Router, public route: ActivatedRoute, private _mqttService: MqttService) {


    this.route.params.subscribe(params => {
      console.log("params" + JSON.stringify(params))

      this.farm = params['farm']
      this.subFarm = params['sub']

      this.subscription = this._mqttService.observe("wcs/smart/" + this.farm).subscribe((message: IMqttMessage) => {
        this.message = message.payload.toString();

        console.log(message.payload.toString());


        var json = JSON.parse(this.message);

       
      })

      this.subscription = this._mqttService.observe("temp/" + this.farm+"/"+this.subFarm).subscribe((message: IMqttMessage) => {
        this.message = message.payload.toString();

        console.log(message.payload.toString());


        var json = JSON.parse(this.message);

       
        this.temperature = json["temperature"]
        this.hum = json["humidity"];



        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var MM = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        var hh = String(today.getHours()).padStart(2, '0');
        var mm = String(today.getMinutes()).padStart(2, '0');
        var ss = String(today.getSeconds()).padStart(2, '0');
        var dt  = dd + "/" + MM + "/" + yyyy + " " + hh + ":" + mm + ":" + ss

        var rawSoil = {};

        rawSoil["y"] = this.soil;
        rawSoil["label"] = hh + ":" + mm + ":" + ss;

        var rawHum = {};

        rawHum["y"] = this.hum;
        rawHum["label"] = hh + ":" + mm + ":" + ss;


        var rawTemp = {};

        rawTemp["y"] = this.temperature;
        rawTemp["label"] = hh + ":" + mm + ":" + ss;


        this.humList.push(rawHum);
        this.tempList.push(rawTemp);
        this.soilList.push(rawSoil);


        this.chart.data[0] = this.tempList;
        this.chart.data[1] = this.humList;
        this.chart.data[1] = this.soilList;

        this.chart.render();



      })


      this.subscription = this._mqttService.observe("soil/" + this.farm+"/"+this.subFarm).subscribe((message: IMqttMessage) => {
        this.message = message.payload.toString();

        console.log(message.payload.toString());


        var json = JSON.parse(this.message);

        this.soil = json["soil"]

        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var MM = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        var hh = String(today.getHours()).padStart(2, '0');
        var mm = String(today.getMinutes()).padStart(2, '0');
        var ss = String(today.getSeconds()).padStart(2, '0');
        var dt = dd + "/" + MM + "/" + yyyy + " " + hh + ":" + mm + ":" + ss


        var rawSoil = {};

        rawSoil["y"] = this.soil;
        rawSoil["label"] = hh + ":" + mm + ":" + ss;

        var rawHum = {};

        rawHum["y"] = this.hum;
        rawHum["label"] = hh + ":" + mm + ":" + ss;


        var rawTemp = {};

        rawTemp["y"] = this.temperature;
        rawTemp["label"] = hh + ":" + mm + ":" + ss;


        this.humList.push(rawHum);
        this.tempList.push(rawTemp);
        this.soilList.push(rawSoil);


        this.chart.data[0] = this.tempList;
        this.chart.data[1] = this.humList;
        this.chart.data[1] = this.soilList;

        this.chart.render();

      })
    })
  }

  ngOnInit(): void {

    this.chart = new CanvasJS.Chart("chartContainer", {
      animationEnabled: true,
      exportEnabled: true,
      zoomEnabled: true,

      legend: {
        cursor: "pointer",
        verticalAlign: "top",
        horizontalAlign: "center",
        dockInsidePlotArea: true,
        itemclick: (e) => {

          if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
          } else {
            e.dataSeries.visible = true;
          }
          this.chart.render();

        }
      },

      data: [

        {
          type: "spline",
          name: "อุณหภูมิ",
          showInLegend: true,
          markerSize: 10,
          yValueFormatString: "# °C",
          dataPoints: this.tempList
        },


        {
          type: "spline",
          name: "ความชื้นในอากาศ",
          showInLegend: true,
          markerSize: 10,
          yValueFormatString: "# %",
          dataPoints: this.humList
        },

        {
          type: "spline",
          name: "ความชื้นในดิน",
          showInLegend: true,
          markerSize: 10,
          yValueFormatString: "# ",
          dataPoints: this.soilList
        }



      ]
    });

    this.chart.render();

  }

}
