import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-req',
  templateUrl: './req.component.html',
  styleUrls: ['./req.component.sass']
})
export class ReqComponent implements OnInit {

  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent
  rows = [];
  data = [];
  filteredData = [];
  dataList = []
  

  columns = [

    { prop: "std" },
    { prop: "id" },
    { prop: "farm" },
    { prop: "sub" },
    { prop: "note" },
    { prop: "dt" },

  ];


  constructor(private http: HttpClient) { }

  ngOnInit(): void {

    this.callAPI();
  }

  callAPI() {

    this.http.get("http://139.59.125.83:3122/get-req/farm", {})

      .subscribe(

        data => {


          var json = JSON.parse(JSON.stringify(data["message"]));
          for (var i = 0; i < Object.keys(json).length; i++) {

            var raw = {};

            raw["id"] = i;
            raw["id_std"] = json[i]["id_std"];
            raw["std"] = json[i]["name"]
            raw["farm"] = json[i]["id_farm"]
            raw["sub"] = json[i]["id_sub_farm"]
            raw["note"] = json[i]["note"] == "" ? "-" : json[i]["note"]
            raw["dt"] = json[i]["dt"]

            this.dataList.push(raw);

          }

          this.data = this.dataList;
          this.filteredData = this.dataList;

        }
      )
  }

  access(value) {
    console.log(value);


    var std = this.dataList[value]["id_std"]
    var farm = this.dataList[value]["farm"]
    var sub = this.dataList[value]["sub"]

    this.http.post("http://139.59.125.83:3122/get-req/approve/" + std + "/" + farm + "/" + sub, {})

      .subscribe(

        data => {

          this.dataList = [];
          this.data = [];
          this.filteredData = [];

          this.callAPI()
        })
  }
  filterDatatable(event) {
    // get the value of the key pressed and make it lowercase
    const val = event.target.value.toLowerCase();
    // get the amount of columns in the table
    const colsAmt = this.columns.length;
    // get the key names of each column in the dataset

    console.log(JSON.stringify(this.filteredData));
    const keys = Object.keys(this.filteredData[0]);
    // assign filtered matches to the active datatable
    this.data = this.filteredData.filter(function (item) {
      // iterate through each row's column data
      for (let i = 0; i < colsAmt; i++) {
        // check for a match


        if (
          item[keys[i]]
            .toString()
            .toLowerCase()
            .indexOf(val) !== -1 ||
          !val
        ) {
          // found match, return true to add to result set
          return true;
        }
      }
    });

    console.log(JSON.stringify(this.data))
    // whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
  

}
