const app = require('express')();
const bodyParser = require('body-parser');
const OneSignal = require('onesignal-node');
const mysql = require('mysql');
const dateFormat = require('dateformat');
const multer = require('multer');
const clientNoti = new OneSignal.Client('6447352b-abed-44d0-9300-36473501dac4', 'NmRlMmY1OTItOTNjYS00ZWU1LTg1MDgtMjYxZTJhNzRhN2Ux');


const port = 3122;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

var con = mysql.createConnection({
    host: "139.59.125.83",
    user: "wcs",
    password: "@#WCSSmartFarm2020",
    database: "wcs_smart_farm"
});

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "*");
    res.header("Access-Control-Allow-Headers", "*");
    next();
});

var pathImg = '';

con.connect(function (err) {
    if (err) throw err;
});


var notification = {
    headings: {
        'en': ""
    },
    contents: {
        'en': "",
    },
    included_segments: [],
};


var Storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, "/var/www/html/image");
    },
    filename: function (req, file, callback) {

        pathImg = Date.now() + "_" + file.originalname;
        callback(null, pathImg);

    }
});

var upload = multer({
    storage: Storage
}).array("image", 3);



function getREQ(req) {
    console.log("[" + req.method + "] : " + req.path, "[params] : " + JSON.stringify(req.params), "[body] : " + JSON.stringify(req.body));
}



app.post("/detail-update/:idUser/:type", function (req, res) {

    getREQ(req);
    var idUser = req.params.idUser;
    var type = req.params.type;

    var name = req.body.name;
    var std = req.body.std;
    var level = req.body.level;
    var phone = req.body.phone;

    if (type == 0) {

        var query = "update detail_user set id_std = '"+std+"' , name = '"+name+"' , level = '"+level+"' , phone = '"+phone+"' WHERE id_user = " + idUser

        console.log(query);
        con.query(query, function (err, result, fields) {
            if (err) {
                console.log(err.stack);
                res.json({ "status": 500, message: err.stack })
                return;
            }

            var query = "update user set name = '"+name+"'  WHERE id = " + idUser

            con.query(query, function (err, result, fields) {
                if (err) {
                    console.log(err.stack);
                    res.json({ "status": 500, message: err.stack })
                    return;
                }
                res.json({ "status": 200 })
            })


           
        })
        

    }else{

        var query = "update detail_user_admin set name = '"+name+"' , phone = '"+phone+"' WHERE id_user = " + idUser

        con.query(query, function (err, result, fields) {
            if (err) {
                console.log(err.stack);
                res.json({ "status": 500, message: err.stack })
                return;
            }

            var query = "update user set name = '"+name+"'  WHERE id = " + idUser

            con.query(query, function (err, result, fields) {
                if (err) {
                    console.log(err.stack);
                    res.json({ "status": 500, message: err.stack })
                    return;
                }
                res.json({ "status": 200 })
            })
        })

    }

})

app.post("/detail/:idUser/:type", function (req, res) {


    var idUser = req.params.idUser;
    var type = req.params.type;


    if (type == 0) {
        var query = "select * from detail_user WHERE id_user = " + idUser;

        con.query(query, function (err, result, fields) {
            if (err) {
                console.log(err.stack);
                res.json({ "status": 500, message: err.stack })
                return;
            }

            if(Object.keys(result).length  >= 1){

                var raw = {};
                raw["type"] = 0;
                raw["idUser"] = result[0]["id_user"];
                raw["name"] = (result[0]["name"] == null || result[0]["name"] == undefined || result[0]["name"] == "") ? "" : result[0]["name"];
                raw["std"] = result[0]["id_std"];
                raw["level"] = result[0]["level"];
                raw["phone"] = (result[0]["phone"] == null || result[0]["phone"] == undefined || result[0]["phone"] == "") ? "" : result[0]["phone"];
                raw["image"] = (result[0]["image"] == null || result[0]["image"] == undefined || result[0]["image"] == "") ? "" : result[0]["image"];
    
    
                res.json({ "status": 200, message: raw })

            }else{
                res.json({ "status": 204})
            }

           


        })
    } else {

        var query = "select * from detail_user_admin WHERE id_user = " + idUser;

        con.query(query, function (err, result, fields) {
            if (err) {
                console.log(err.stack);
                res.json({ "status": 500, message: err.stack })
                return;
            }

            if(Object.keys(result).length  >= 1){
                var raw = {};
                raw["type"] = 1;
                raw["idUser"] = result[0]["id_user"];
                raw["name"] = (result[0]["name"] == null || result[0]["name"] == undefined || result[0]["name"] == "") ? "" : result[0]["name"];
                raw["std"] = "";
                raw["level"] = "";
                raw["phone"] = (result[0]["phone"] == null || result[0]["phone"] == undefined || result[0]["phone"] == "") ? "" : result[0]["phone"];
                raw["image"] = (result[0]["image"] == null || result[0]["image"] == undefined || result[0]["image"] == "") ? "" : result[0]["image"];
    
                res.json({ "status": 200, message: raw })
            }else{
                res.json({ "status": 204})
            }
           

        })


    }



})

app.post("/login", function (req, res) {
    getREQ(req);

    var username = req.body.username;
    var password = req.body.password;

    var query = "select * from user WHERE username =  '" + username + "' AND password = '" + password + "'";


    con.query(query, function (err, result, fields) {
        if (err) {
            console.log(err.stack);
            res.json({ "status": 500, message: err.stack })
            return;
        }

        if (Object.keys(result).length >= 1) {

            var type = result[0]["type"];

            if (type == 0) {

                var query = "select * from detail_user where id_user = " + result[0]["id"];

                con.query(query, function (err, result, fields) {
                    if (err) {
                        console.log(err.stack);
                        res.json({ "status": 500, message: err.stack })
                        return;
                    }

                    var raw = {};
                    raw["type"] = 0;
                    raw["idUser"] = result[0]["id_user"];
                    raw["name"] = (result[0]["name"] == null || result[0]["name"] == undefined || result[0]["name"] == "") ? "" : result[0]["name"];
                    raw["std"] = result[0]["id_std"];
                    raw["level"] = result[0]["level"];
                    raw["phone"] = (result[0]["phone"] == null || result[0]["phone"] == undefined || result[0]["phone"] == "") ? "" : result[0]["phone"];
                    raw["image"] = (result[0]["image"] == null || result[0]["image"] == undefined || result[0]["image"] == "") ? "" : result[0]["image"];



                    res.json({ "status": 200, message: raw })
                })

            } else {

                var query = "select * from detail_user_admin where id_user = " + result[0]["id"];

                con.query(query, function (err, result, fields) {
                    if (err) {
                        console.log(err.stack);
                        res.json({ "status": 500, message: err.stack })
                        return;
                    }

                    var raw = {};
                    raw["type"] = 1;
                    raw["idUser"] = result[0]["id_user"];
                    raw["name"] = (result[0]["name"] == null || result[0]["name"] == undefined || result[0]["name"] == "") ? "" : result[0]["name"];
                    raw["std"] = "";
                    raw["level"] = "";
                    raw["phone"] = (result[0]["phone"] == null || result[0]["phone"] == undefined || result[0]["phone"] == "") ? "" : result[0]["phone"];
                    raw["image"] = (result[0]["image"] == null || result[0]["image"] == undefined || result[0]["image"] == "") ? "" : result[0]["image"];

                    res.json({ "status": 200, message: raw })
                })

            }
        } else {

            res.json({ "status": 204 })
        }
    })
})

app.post("/register", function (req, res) {
    getREQ(req);

    var username = req.body.username;
    var password = req.body.password;

    var query = "select * from user WHERE username =  '" + username + "'";

    con.query(query, function (err, result, fields) {
        if (err) {
            console.log(err.stack);
            res.json({ "status": 500, message: err.stack })
            return;
        }

        if (Object.keys(result).length >= 1) {

            res.json({ "status": 204 })
        } else {

            query = "insert into user (username , password) values ('" + username + "' , '" + password + "')"

            con.query(query, function (err, result, fields) {
                if (err) {
                    console.log(err.stack);
                    res.json({ "status": 500, message: err.stack })
                    return;
                }

                var id = result["insertId"];

                query = "insert into detail_user (id_user , id_std , name , level , phone) values (" + id + ",'','' , '' , '')";

                con.query(query, function (err, result, fields) {
                    if (err) {
                        console.log(err.stack);
                        res.json({ "status": 500, message: err.stack })
                        return;
                    }
                    res.json({ "status": 200 })
                })


            })
        }


    })
})

app.post("/std/image", function (req, res) {
    getREQ(req);

    upload(req, res, function (err) {
        if (err) {

            return res.json({ "status": 500, message: err })
        }

        var idStd = req.body.idStd;

        query = "update detail_user set image = '" + pathImg + "'  WHERE id_std = '" + idStd + "'";
        console.log(query);
        con.query(query, function (err, result, fields) {
            if (err) {
                console.log(err.stack);
                res.json({ "status": 500, message: err.stack })
                return;
            }

            res.json({ "status": 200 })
        })

    });
});




app.post("/std/detail", function (req, res) {
    getREQ(req);

    var name = req.body.name;
    var idStd = req.body.idStd;
    var level = req.body.level;
    var phone = req.body.phone;

    var query = "select * from detail_user where id_std = '" + idStd + "'";

    con.query(query, function (err, result, fields) {
        if (err) {
            console.log(err.stack);
            res.json({ "status": 500, message: err.stack })
            return;
        }

        if (Object.keys(result).length >= 1) {

            query = "update detail_user set name = '" + name + "' , level = '" + level + "' , phone = '" + phone + "' WHERE id_std = '" + idStd + "'";
            console.log(query);
            con.query(query, function (err, result, fields) {
                if (err) {
                    console.log(err.stack);
                    res.json({ "status": 500, message: err.stack })
                    return;
                }

                res.json({ "status": 200 })
            })

        } else {

            query = "insert into detail_user (id_std , name , level , phone) values ('" + idStd + "' , '" + name + "' , '" + level + "' , '" + phone + "')"

            console.log(query);
            con.query(query, function (err, result, fields) {
                if (err) {
                    console.log(err.stack);
                    res.json({ "status": 500, message: err.stack })
                    return;
                }

                res.json({ "status": 200 })
            })
        }
    })
});



app.delete('/notification/:id', function (req, res) {

    getREQ(req)

    var query = "delete from noti where  id = " + req.params.id;

    con.query(query, function (err, result, fields) {
        if (err) {
            console.log(err.stack);
            res.json({ "status": 500, message: err.stack })
            return;
        }

        res.json({ "status": 200 })
    })
})



app.post('/notification', function (req, res) {

    getREQ(req);

    var title = req.body.title != undefined ? req.body.title : "";
    var message = req.body.message != undefined ? req.body.message : "";
    var image = req.body.image != undefined ? req.body.image : "";
    var url = req.body.url != undefined ? req.body.url : "";
    var form = req.body.form != undefined ? req.body.form : "";
    var dt = new Date();

    var strDateShow = dateFormat(dt, "dd/mm/yyyy HH:MM:ss")
    var strDateDatabase = dateFormat(dt, "yyyy/mm/dd HH:MM:ss")

    var query = "insert into noti(title , message , url , image , form , dt) values ('" + title + "' , '" + message + "' , '" + url + "' , '" + image + "'  , '" + form + "' , '" + strDateDatabase + "');";

    con.query(query, function (err, result, fields) {
        if (err) {
            console.log(err.stack);
            res.json({ "status": 500, message: err.stack })
            return;
        }

        notification.contents.en = message
        notification.headings.en = title
        notification.included_segments = ['Subscribed Users']

        var resp = clientNoti.createNotification(notification);

        console.log(JSON.stringify(resp));

        res.json({ "status": 200 })
    });
});

// log temp


app.post("/log/temp", function (req, res) {

    getREQ(req);

    var farm = req.body.farm;
    var subFarm = req.body.subFarm;

    var temp = req.body.temp;
    var hum = req.body.hum;

    var query = "insert into log_temp_hum (farm , sub_farm , temp , hum) values (" + farm + " , " + subFarm + " , " + temp + " , " + hum + ")";

    con.query(query, function (err, result, fields) {

        if (err) {
            console.log(err.stack);
            res.json({ "status": 500, message: err.stack })
            return;
        }

        res.json({ "status": 200 })
    });


});


app.get("/log/day/temp/:farm/:subFarm/:day", function (req, res) {

    getREQ(req);

    var farm = req.params.farm;
    var subFarm = req.params.subFarm;

    var day = req.params.day;

    var query = "SELECT  avg(temp) as temp  , avg(hum) as hum , DATE_FORMAT(dt,'%Y-%m-%d %H') as dtf  FROM log_temp_hum  where dt between '" + day + " 00:00:00' AND '" + day + "  23:59:59'  AND farm =  " + farm + " AND sub_farm = " + subFarm + " GROUP BY dtf order by dtf  ASC";

    con.query(query, function (err, result, fields) {

        if (err) {
            console.log(err.stack);
            res.json({ "status": 500, message: err.stack })
            return;
        }



        for (var i = 0; i < Object.keys(result).length; i++) {

            result[i]['dtf'] = dateFormat(result[i]['dtf'] + ":00:00", "HH")
        }


        res.json({ "status": 200, message: result })
    });


});

app.get("/log/month/temp/:farm/:subFarm/:month", function (req, res) {

    getREQ(req);

    var farm = req.params.farm;
    var subFarm = req.params.subFarm;

    var month = req.params.month;

    var query = "SELECT avg(temp) as temp  , avg(hum) as hum ,  DATE_FORMAT(dt,'%Y-%m-%d') as dtf   FROM log_temp_hum  where dt between '" + month + "' AND CONCAT(LAST_DAY('" + month + "')) AND farm =  " + farm + " AND sub_farm = " + subFarm + " GROUP BY dtf order by dtf  ASC ";

    con.query(query, function (err, result, fields) {

        if (err) {
            console.log(err.stack);
            res.json({ "status": 500, message: err.stack })
            return;
        }



        for (var i = 0; i < Object.keys(result).length; i++) {

            result[i]['dtf'] = dateFormat(result[i]['dtf'] + " 00:00:00", "dd")
        }


        res.json({ "status": 200, message: result })
    });


});


app.get("/log/year/temp/:farm/:subFarm/:year", function (req, res) {
    getREQ(req);

    var farm = req.params.farm;
    var subFarm = req.params.subFarm;

    var year = req.params.year;

    var query = "SELECT avg(temp) as temp  , avg(hum) as hum ,  DATE_FORMAT(dt,'%Y-%m') as dtf  FROM log_temp_hum  where dt between '" + year + "-01-01'  AND '" + year + "-12-31' AND farm =  " + farm + " AND sub_farm = " + subFarm + " GROUP BY dtf order by dtf  ASC";

    con.query(query, function (err, result, fields) {

        if (err) {
            console.log(err.stack);
            res.json({ "status": 500, message: err.stack })
            return;
        }



        for (var i = 0; i < Object.keys(result).length; i++) {

            result[i]['dtf'] = dateFormat(result[i]['dtf'] + "-01 00:00:00", "mm")
        }


        res.json({ "status": 200, message: result })
    });


});

app.get("/log/ad/temp/:farm/:subFarm/:start/:end", function (req, res) {
    getREQ(req);

    var farm = req.params.farm;
    var subFarm = req.params.subFarm;

    var start = req.params.start;
    var end = req.params.end;

    var query = "SELECT  avg(temp) as temp  , avg(hum) as hum , DATE_FORMAT(dt,'%Y-%m-%d %H') as dtf  FROM log_temp_hum  where dt between '" + start + "' AND '" + end + "'  AND farm =  " + farm + " AND sub_farm = " + subFarm + " GROUP BY dtf order by dtf  ASC";

    con.query(query, function (err, result, fields) {

        if (err) {
            console.log(err.stack);
            res.json({ "status": 500, message: err.stack })
            return;
        }

        res.json({ "status": 200, message: result })
    });


});





app.post("/log/soil", function (req, res) {

    getREQ(req);

    var farm = req.body.farm;
    var subFarm = req.body.subFarm;

    var soil = req.body.soil;


    var query = "insert into log_soil (farm , sub_farm , soil) values (" + farm + " , " + subFarm + " , " + soil + ")";

    con.query(query, function (err, result, fields) {

        if (err) {
            console.log(err.stack);
            res.json({ "status": 500, message: err.stack })
            return;
        }

        res.json({ "status": 200 })
    });


});



app.get("/log/day/soil/:farm/:subFarm/:day", function (req, res) {

    getREQ(req);

    var farm = req.params.farm;
    var subFarm = req.params.subFarm;

    var day = req.params.day;

    var query = "SELECT  avg(soil) as soil  , DATE_FORMAT(dt,'%Y-%m-%d %H') as dtf  FROM log_soil  where dt between '" + day + " 00:00:00' AND '" + day + "  23:59:59'  AND farm =  " + farm + " AND sub_farm = " + subFarm + " GROUP BY dtf order by dtf  ASC";

    con.query(query, function (err, result, fields) {

        if (err) {
            console.log(err.stack);
            res.json({ "status": 500, message: err.stack })
            return;
        }



        for (var i = 0; i < Object.keys(result).length; i++) {

            result[i]['dtf'] = dateFormat(result[i]['dtf'] + ":00:00", "HH")
        }


        res.json({ "status": 200, message: result })
    });


});



app.get("/log/month/soil/:farm/:subFarm/:month", function (req, res) {

    getREQ(req);

    var farm = req.params.farm;
    var subFarm = req.params.subFarm;

    var month = req.params.month;

    var query = "SELECT avg(soil) as soil  ,  DATE_FORMAT(dt,'%Y-%m-%d') as dtf   FROM log_soil  where dt between '" + month + "' AND CONCAT(LAST_DAY('" + month + "')) AND farm =  " + farm + " AND sub_farm = " + subFarm + " GROUP BY dtf order by dtf  ASC ";

    con.query(query, function (err, result, fields) {

        if (err) {
            console.log(err.stack);
            res.json({ "status": 500, message: err.stack })
            return;
        }



        for (var i = 0; i < Object.keys(result).length; i++) {

            result[i]['dtf'] = dateFormat(result[i]['dtf'] + " 00:00:00", "dd")
        }


        res.json({ "status": 200, message: result })
    });


});



app.get("/log/year/soil/:farm/:subFarm/:year", function (req, res) {

    getREQ(req);

    var farm = req.params.farm;
    var subFarm = req.params.subFarm;

    var year = req.params.year;

    var query = "SELECT avg(soil) as soil  ,   DATE_FORMAT(dt,'%Y-%m') as dtf  FROM log_soil where dt between '" + year + "-01-01'  AND '" + year + "-12-31' AND farm =  " + farm + " AND sub_farm = " + subFarm + " GROUP BY dtf order by dtf  ASC";

    con.query(query, function (err, result, fields) {

        if (err) {
            console.log(err.stack);
            res.json({ "status": 500, message: err.stack })
            return;
        }



        for (var i = 0; i < Object.keys(result).length; i++) {

            result[i]['dtf'] = dateFormat(result[i]['dtf'] + "-01 00:00:00", "mm")
        }


        res.json({ "status": 200, message: result })
    });


});

app.get("/log/ad/soil/:farm/:subFarm/:start/:end", function (req, res) {

    getREQ(req);

    var farm = req.params.farm;
    var subFarm = req.params.subFarm;

    var start = req.params.start;
    var end = req.params.end;

    var query = "SELECT  avg(soil) as soil  , DATE_FORMAT(dt,'%Y-%m-%d %H') as dtf  FROM log_soil  where dt between '" + start + "' AND '" + end + "'  AND farm =  " + farm + " AND sub_farm = " + subFarm + " GROUP BY dtf order by dtf  ASC";

    con.query(query, function (err, result, fields) {

        if (err) {
            console.log(err.stack);
            res.json({ "status": 500, message: err.stack })
            return;
        }

        res.json({ "status": 200, message: result })
    });


});






app.post("/req/farm", function (req, res) {

    getREQ(req);

    var farm = req.body.farm;
    var note = req.body.note;
    var subFarm = req.body.subFarm;

    var idStd = req.body.idStd;


    for (let i = 0; i < Object.keys(idStd).length; i++) {

        var query = "select * from request_sub_farm where id_farm  = " + farm + " AND  id_sub_farm = " + subFarm + " AND id_std = '" + idStd[i] + "'";


        let std = idStd[i];

        console.log(query);
        con.query(query, (err, result, fields) => {

            if (err) {
                console.log(err.stack);
                res.json({ "status": 500, message: err.stack })
                return;
            }

            if (Object.keys(result).length == 0) {

                query = "insert into request_sub_farm (id_farm , id_sub_farm , id_std , note) values ( " + farm + " , " + subFarm + " , '" + idStd[i] + "' , '" + note + "' )";

                con.query(query, (err, result, fields) => {

                    if (err) {
                        console.log(err.stack);
                        res.json({ "status": 500, message: err.stack })
                        return;
                    }
                })
            }
        }
        );

    }

    res.json({ "status": 200 })

});







function diff_minutes(dt2, dt1) {

    var diff = (dt2.getTime() - dt1.getTime()) / 1000;
    diff /= 60;
    return Math.abs(Math.round(diff));

}



app.get('/notification', function (req, res) {


    getREQ(req);

    var query = "select id , title , message , url , image , form , DATE_FORMAT(dt, '%d-%m-%Y %T') as 'dt'  from noti order by id desc";
    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        var key = Object.keys(result);

        if (key.length >= 1) {
            res.json({ "status": 200, message: result })
        } else {
            res.json({ "status": 204, message: "No Data" })
        }
    });

});

app.get('/farm', function (req, res) {


    getREQ(req);

    var query = "select id_farm , farm.name as 'fname' , image , event , user_event , DATE_FORMAT(dt, '%d-%m-%Y %T') as 'dt' , user.name as 'uname'  from farm left join user on farm.user_event = user.id";
    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        var key = Object.keys(result);

        if (key.length >= 1) {
            res.json({ "status": 200, message: result })
        } else {
            res.json({ "status": 204, message: "No Data" })
        }
    });

});


app.get('/web/sub-farm', function (req, res) {

    getREQ(req);

    var query = "select sub_farm.status , sub_farm.id , id_farm , id_sub , sub_farm.name  as 'fname', event , user.name , DATE_FORMAT(sub_farm.dt, '%d/%m/%Y %T') as 'dt' , status  , DATE_FORMAT(dt_gateway_temp, '%Y-%m-%d %T') as 'temp' , DATE_FORMAT(dt_gateway_soil, '%Y-%m-%d %T') as 'soil' , DATE_FORMAT(dt_gateway_water, '%Y-%m-%d %T') as 'water', DATE_FORMAT(dt_gateway_fog, '%Y-%m-%d %T') as 'fog' , DATE_FORMAT(dt_gateway_lamp, '%Y-%m-%d %T') as 'lamp' from sub_farm   left join user on user.id = sub_farm.user_event"
    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        var key = Object.keys(result);

        if (key.length >= 1) {
            for (var i = 0; i < key.length; i++) {


                result[i]["status_temp"] = "ไม่ทราบ"
                result[i]["status_soil"] = "ไม่ทราบ"
                result[i]["status_water"] = "ไม่ทราบ"
                result[i]["status_fog"] = "ไม่ทราบ"
                result[i]["status_lamp"] = "ไม่ทราบ"


                result[i]["status_t"] = false
                result[i]["status_s"] = false

                result[i]["status_w"] = false
                result[i]["status_f"] = false
                result[i]["status_l"] = false

                if (result[i]['temp'] != null || result[i]['soil'] != null || result[i]['fog'] != null || result[i]['lamp'] != null || result[i]['water'] != null) {


                    var dt1 = new Date(result[i]['temp']);
                    var dt2 = new Date();

                    var df = diff_minutes(dt2, dt1);
                    if (df <= 3) {
                        result[i]["status_t"] = true
                    } else {
                        result[i]["status_t"] = false
                    }
                    result[i]["status_temp"] = diff_minutes(dt2, dt1) + "นาที"


                    dt1 = new Date(result[i]['soil']);
                    dt2 = new Date();

                    df = diff_minutes(dt2, dt1);
                    if (df <= 3) {
                        result[i]["status_s"] = true
                    } else {
                        result[i]["status_s"] = false
                    }
                    result[i]["status_soil"] = diff_minutes(dt2, dt1) + "นาที"


                    dt1 = new Date(result[i]['water']);
                    dt2 = new Date();

                    df = diff_minutes(dt2, dt1);
                    if (df <= 3) {
                        result[i]["status_w"] = true
                    } else {
                        result[i]["status_w"] = false
                    }
                    result[i]["status_water"] = diff_minutes(dt2, dt1) + "นาที"


                    dt1 = new Date(result[i]['fog']);
                    dt2 = new Date();

                    df = diff_minutes(dt2, dt1);
                    if (df <= 3) {
                        result[i]["status_f"] = true
                    } else {
                        result[i]["status_f"] = false
                    }
                    result[i]["status_fog"] = diff_minutes(dt2, dt1) + "นาที"


                    dt1 = new Date(result[i]['lamp']);
                    dt2 = new Date();

                    df = diff_minutes(dt2, dt1);
                    if (df <= 3) {
                        result[i]["status_l"] = true
                    } else {
                        result[i]["status_l"] = false
                    }
                    result[i]["status_lamp"] = diff_minutes(dt2, dt1) + "นาที"




                } else {

                }



            }
            res.json({ "status": 200, message: result })
        } else {
            res.json({ "status": 204, message: "No Data" })
        }
    });


})
app.get('/sub-farm/:id', function (req, res) {

    getREQ(req);
    var id = req.params.id


    var query = "select sub_farm.status , sub_farm.id , id_farm , id_sub , sub_farm.name  as 'fname', event , user.name , DATE_FORMAT(sub_farm.dt, '%d/%m/%Y %T') as 'dt' , status  , DATE_FORMAT(dt_gateway_temp, '%Y-%m-%d %T') as 'temp' , DATE_FORMAT(dt_gateway_soil, '%Y-%m-%d %T') as 'soil' , DATE_FORMAT(dt_gateway_water, '%Y-%m-%d %T') as 'water', DATE_FORMAT(dt_gateway_fog, '%Y-%m-%d %T') as 'fog' , DATE_FORMAT(dt_gateway_lamp, '%Y-%m-%d %T') as 'lamp' from sub_farm   left join user on user.id = sub_farm.user_event where  id_farm = " + id;
    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        var key = Object.keys(result);

        if (key.length >= 1) {
            for (var i = 0; i < key.length; i++) {


                result[i]["status_temp"] = "ไม่ทราบ"
                result[i]["status_soil"] = "ไม่ทราบ"
                result[i]["status_water"] = "ไม่ทราบ"
                result[i]["status_fog"] = "ไม่ทราบ"
                result[i]["status_lamp"] = "ไม่ทราบ"


                result[i]["status_t"] = false
                result[i]["status_s"] = false

                result[i]["status_w"] = false
                result[i]["status_f"] = false
                result[i]["status_l"] = false

                if (result[i]['temp'] != null || result[i]['soil'] != null || result[i]['fog'] != null || result[i]['lamp'] != null || result[i]['water'] != null) {


                    var dt1 = new Date(result[i]['temp']);
                    var dt2 = new Date();

                    var df = diff_minutes(dt2, dt1);
                    if (df <= 3) {
                        result[i]["status_t"] = true
                    } else {
                        result[i]["status_t"] = false
                    }
                    result[i]["status_temp"] = diff_minutes(dt2, dt1) + "นาที"


                    dt1 = new Date(result[i]['soil']);
                    dt2 = new Date();

                    df = diff_minutes(dt2, dt1);
                    if (df <= 3) {
                        result[i]["status_s"] = true
                    } else {
                        result[i]["status_s"] = false
                    }
                    result[i]["status_soil"] = diff_minutes(dt2, dt1) + "นาที"


                    dt1 = new Date(result[i]['water']);
                    dt2 = new Date();

                    df = diff_minutes(dt2, dt1);
                    if (df <= 3) {
                        result[i]["status_w"] = true
                    } else {
                        result[i]["status_w"] = false
                    }
                    result[i]["status_water"] = diff_minutes(dt2, dt1) + "นาที"


                    dt1 = new Date(result[i]['fog']);
                    dt2 = new Date();

                    df = diff_minutes(dt2, dt1);
                    if (df <= 3) {
                        result[i]["status_f"] = true
                    } else {
                        result[i]["status_f"] = false
                    }
                    result[i]["status_fog"] = diff_minutes(dt2, dt1) + "นาที"


                    dt1 = new Date(result[i]['lamp']);
                    dt2 = new Date();

                    df = diff_minutes(dt2, dt1);
                    if (df <= 3) {
                        result[i]["status_l"] = true
                    } else {
                        result[i]["status_l"] = false
                    }
                    result[i]["status_lamp"] = diff_minutes(dt2, dt1) + "นาที"




                } else {

                }



            }
            res.json({ "status": 200, message: result })
        } else {
            res.json({ "status": 204, message: "No Data" })
        }
    });

});


app.delete('/my-farm/:idStd/:subFarm/:farm', function (req, res) {

    getREQ(req);
    var idStd = req.params.idStd
    var subFarm = req.params.subFarm
    var farm = req.params.farm;

    var query = "delete from request_sub_farm where id_sub_farm = " + subFarm + " AND id_std = '" + idStd + "' AND id_farm = " + farm;

    console.log(query);
    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        res.json({ "status": 200, message: result })

    })
})

app.get('/my-farm/:idStd', function (req, res) {


    getREQ(req);

    var idStd = req.params.idStd


    var query = "select sub_farm.id , sub_farm.id_farm , sub_farm.id_sub , sub_farm.name  as 'fname', event , detail_user.name , DATE_FORMAT(sub_farm.dt, '%d/%m/%Y %T') as 'dt'   , DATE_FORMAT(dt_gateway_temp, '%Y-%m-%d %T') as 'temp' , DATE_FORMAT(dt_gateway_soil, '%Y-%m-%d %T') as 'soil' , DATE_FORMAT(dt_gateway_water, '%Y-%m-%d %T') as 'water', DATE_FORMAT(dt_gateway_fog, '%Y-%m-%d %T') as 'fog' , DATE_FORMAT(dt_gateway_lamp, '%Y-%m-%d %T') as 'lamp' from request_sub_farm inner join sub_farm on request_sub_farm.id_farm = sub_farm.id_farm AND request_sub_farm.id_sub_farm =  sub_farm.id_sub inner join detail_user on request_sub_farm.id_std = detail_user.id_std where request_sub_farm.status = 1 AND request_sub_farm.id_std ='" + idStd + "'";
    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        var key = Object.keys(result);

        if (key.length >= 1) {
            for (var i = 0; i < key.length; i++) {


                result[i]["status_temp"] = "ไม่ทราบ"
                result[i]["status_soil"] = "ไม่ทราบ"
                result[i]["status_water"] = "ไม่ทราบ"
                result[i]["status_fog"] = "ไม่ทราบ"
                result[i]["status_lamp"] = "ไม่ทราบ"


                result[i]["status_t"] = false
                result[i]["status_s"] = false

                result[i]["status_w"] = false
                result[i]["status_f"] = false
                result[i]["status_l"] = false

                if (result[i]['temp'] != null || result[i]['soil'] != null || result[i]['fog'] != null || result[i]['lamp'] != null || result[i]['water'] != null) {


                    var dt1 = new Date(result[i]['temp']);
                    var dt2 = new Date();

                    var df = diff_minutes(dt2, dt1);
                    if (df <= 3) {
                        result[i]["status_t"] = true
                    } else {
                        result[i]["status_t"] = false
                    }
                    result[i]["status_temp"] = diff_minutes(dt2, dt1) + "นาที"


                    dt1 = new Date(result[i]['soil']);
                    dt2 = new Date();

                    df = diff_minutes(dt2, dt1);
                    if (df <= 3) {
                        result[i]["status_s"] = true
                    } else {
                        result[i]["status_s"] = false
                    }
                    result[i]["status_soil"] = diff_minutes(dt2, dt1) + "นาที"


                    dt1 = new Date(result[i]['water']);
                    dt2 = new Date();

                    df = diff_minutes(dt2, dt1);
                    if (df <= 3) {
                        result[i]["status_w"] = true
                    } else {
                        result[i]["status_w"] = false
                    }
                    result[i]["status_water"] = diff_minutes(dt2, dt1) + "นาที"


                    dt1 = new Date(result[i]['fog']);
                    dt2 = new Date();

                    df = diff_minutes(dt2, dt1);
                    if (df <= 3) {
                        result[i]["status_f"] = true
                    } else {
                        result[i]["status_f"] = false
                    }
                    result[i]["status_fog"] = diff_minutes(dt2, dt1) + "นาที"


                    dt1 = new Date(result[i]['lamp']);
                    dt2 = new Date();

                    df = diff_minutes(dt2, dt1);
                    if (df <= 3) {
                        result[i]["status_l"] = true
                    } else {
                        result[i]["status_l"] = false
                    }
                    result[i]["status_lamp"] = diff_minutes(dt2, dt1) + "นาที"




                } else {

                }



            }
            res.json({ "status": 200, message: result })
        } else {
            res.json({ "status": 204, message: result })
        }
    });

});



app.get('/get-req/farm', function (req, res) {


    getREQ(req);

    var query = "select id_farm , detail_user.name  , id_sub_farm , request_sub_farm.id_std , note  ,   DATE_FORMAT(dt, '%d-%m-%Y %T') as 'dt' from request_sub_farm inner join detail_user on detail_user.id_std = request_sub_farm.id_std where status =0";
    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }

        res.json({ "status": 200, message: result })
    })

})

app.get('/approve/farm', function (req, res) {


    getREQ(req);

    var query = "select id_farm , detail_user.name  , id_sub_farm , request_sub_farm.id_std , note  ,   DATE_FORMAT(dt, '%d-%m-%Y %T') as 'dt' from request_sub_farm inner join detail_user on detail_user.id_std = request_sub_farm.id_std where status =1";
    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }

        res.json({ "status": 200, message: result })
    })

})



app.post('/get-req/approve/:idStd/:idFarm/:idSubFarm', function (req, res) {


    var idStd = req.params.idStd
    var idFarm = req.params.idFarm
    var idSubFarm = req.params.idSubFarm

    getREQ(req);

    var query = "update request_sub_farm set status = 1 WHERE id_std = '" + idStd + "' AND id_farm =  " + idFarm + " AND id_sub_farm = " + idSubFarm

    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }

        res.json({ "status": 200 })
    })

})




app.get('/notification/:id', function (req, res) {

    getREQ(req);

    var id = req.params.id;
    var query = "select * from noti where id  = " + id;
    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        var key = Object.keys(result);

        if (key.length >= 1) {
            res.json({ "status": 200, message: result })
        } else {
            res.json({ "status": 204, message: "No Data" })
        }
    });

});


app.post('/create-sensing', function (req, res) {

    getREQ(req);

    var farm = req.body.farm
    var subFarm = req.body.subFarm
    var start = req.body.start
    var end = req.body.end
    var control = req.body.control
    var sensor = req.body.sensor
    var statusLogic = req.body.statusLogic;

    var query = "insert into remote_sensing (farm , sub_farm , value_sensing_start , value_sensing_end , sensor , control , status_logi) value (" + farm + ", " + subFarm + " , " + start + " , " + end + " , '" + sensor + "' , '" + control + "' , "+statusLogic+")"

    console.log(query);

    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        res.json({ "status": 200 })
    });

})

app.get('/sensing/update/:id/:status', function (req, res) {

    getREQ(req);

    var id = req.params.id
    var status = req.params.status;


    if (status == 1) {

        status = 0;

    } else {
        status = 1;
    }


    var query = "UPDATE remote_sensing set status = "+status+" WHERE id = " + id;

    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        res.json({ "status": 200 })
    });


})


app.get('/sensing/:farm/:subFarm/:control', function (req, res) {
    getREQ(req);

    var farm = req.params.farm;
    var subFarm = req.params.subFarm;
    var control = req.params.control;

    var query = "select sensor , id , farm , sub_farm , value_sensing_start as 'start' , value_sensing_end as 'end'  , control , status_logi , status from remote_sensing WHERE farm = " + farm + " AND sub_farm = " + subFarm + " AND control = '" + control + "'";


    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        res.json({ "status": 200, message: result })
    });
})

app.delete('/sensing/:id/', function (req, res) {


    getREQ(req);

    var id = req.params.id;

    var query = "delete from remote_sensing where id = " + id;

    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        res.json({ "status": 200, message: result })
    });
})






app.post('/create-timer', function (req, res) {

    getREQ(req);

    var farm = req.body.farm
    var subFarm = req.body.subFarm
    var start = req.body.start
    var end = req.body.end
    var type = req.body.type
    var status = req.body.status;

    var query = "insert into timer (farm , sub_farm , start_dt , end_dt , control , status_logi) value ( " + farm + ", " + subFarm + " , '" + start + "' , '" + end + "' , '" + type + "' , " + status + ")"


    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        res.json({ "status": 200 })
    });

})

app.get('/timer/:farm/:subFarm/:control', function (req, res) {
    getREQ(req);

    var farm = req.params.farm;
    var subFarm = req.params.subFarm;
    var control = req.params.control;

    var query = "select id , farm , sub_farm , DATE_FORMAT(start_dt,'%d-%m-%Y %T' ) as 'start' , DATE_FORMAT(end_dt,'%d-%m-%Y %T' ) as 'end'  , control , status_logi , status from timer WHERE farm = " + farm + " AND sub_farm = " + subFarm + " AND control = '" + control + "'";


    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        res.json({ "status": 200, message: result })
    });
})

app.get('/timer/:id/:status', function (req, res) {
    getREQ(req);

    var id = req.params.id;
    var status = req.params.status;


    if (status == 1) {

        status = 0;

    } else {
        status = 1;
    }
    console.log(status)


    var query = "update timer set status = " + status + " WHERE id = " + id;

    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        res.json({ "status": 200, message: result })
    });
})


app.get('/select-control/:farm/:subFarm', function (req, res) {
    getREQ(req);

    var farm = req.params.farm;
    var subFarm = req.params.subFarm;

    var query = "select * from sub_farm where id_farm  =  " + farm + " AND id_sub = " + subFarm;


    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        res.json({ "status": 200, message: result[0]["select_control"] })
    });

})

app.get('/select-control/:farm/:subFarm/:status', function (req, res) {
    getREQ(req);

    var farm = req.params.farm;
    var subFarm = req.params.subFarm;
    var status = req.params.status;


    if (status == 1) {

        status = 0;

    } else {
        status = 1;
    }


    var query = "update sub_farm set select_control = " + status + " WHERE id_farm = " + farm + " AND id_sub = " + subFarm;


    console.log(query);
    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        res.json({ "status": 200, message: result })
    });

})





app.delete('/timer/:id/', function (req, res) {


    getREQ(req);

    var id = req.params.id;

    var query = "delete from timer where id = " + id;

    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        res.json({ "status": 200, message: result })
    });
})

app.post('/result', function (req, res) {

    getREQ(req);

    var farm = req.body.farm
    var subFarm = req.body.subFarm
    var temp = req.body.temp
    var hum = req.body.hum
    var soil = req.body.soil
    var note = req.body.note;

    var price_out = (req.body.price_out == null || req.body.price_out == undefined || req.body.price_out == "") ? 0.0 : req.body.price_out;
    var price_in = (req.body.price_in == null || req.body.price_in == undefined || req.body.price_in == "") ? 0.0 : req.body.price_in;

    var std = req.body.std;


    var query = "insert into result (farm ,sub_farm,temp,hum,soil,note,price_out,price_in ) value (" + farm + "," + subFarm + ",'" + temp + "','" + hum + "','" + soil + "','" + note + "' , " + price_out + " , " + price_in + ")";

    console.log(query);
    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        res.json({ "status": 200, message: result["insertId"] })
    });


})

app.post("/result/image", function (req, res) {
    getREQ(req);

    upload(req, res, function (err) {
        if (err) {

            return res.json({ "status": 500, message: err })
        }

        var id = req.body.id;

        id = parseInt(id);

        query = "update result set img = '" + pathImg + "'  WHERE id = " + id;
        console.log(query);
        con.query(query, function (err, result, fields) {
            if (err) {
                console.log(err.stack);
                res.json({ "status": 500, message: err.stack })
                return;
            }

            res.json({ "status": 200 })
        })

    });
});


app.get('/result/:farm/:subFarm', function (req, res) {

    getREQ(req);

    var farm = req.params.farm
    var subFarm = req.params.subFarm

    var query = "select id , farm , sub_farm , temp , hum , soil , note , price_out , price_in , img , DATE_FORMAT(dt, '%d/%m/%Y %T') as 'dt'  from result  WHERE farm = " + farm + " AND sub_farm = " + subFarm + " order by id desc ";

    console.log(query);
    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        res.json({ "status": 200, message: result })
    });


})


app.listen(port, function () {
    console.log('Starting node.js on port ' + port);
});

