#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#include <ArduinoJson.h>
#include "DHT.h"

#define DHTPIN 4
#define DHTTYPE DHT22

#define LED 16
#define LEDLOOP 2

#define SMART 1
#define SENSOR 3
#define INTERVAL 5000
#define TYPE "temp"

#define mqtt_server "35.240.153.210"
#define mqtt_port 1883
#define mqtt_user "win"
#define mqtt_password "@#Win2020"


const char* ssid = "WCS-WiFi";
const char* password = "woranari2021";


WiFiClient espClient;
PubSubClient client(espClient);


DHT dht(DHTPIN, DHTTYPE);

void setup() {
  pinMode(LED , OUTPUT);
  pinMode(LEDLOOP , OUTPUT);
  
  Serial.begin(9600);
  Serial.println(F("DHTxx test!"));

  dht.begin();

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(LED , HIGH);
    delay(200);
    digitalWrite(LED , LOW);
    delay(200);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("");
  Serial.println("IP");
  Serial.println(WiFi.localIP());
  digitalWrite(LED , HIGH);
  digitalWrite(LEDLOOP , LOW);
  client.setServer(mqtt_server, mqtt_port);


}

void loop() {

  delay(INTERVAL);

  String clientID  = "";

  clientID.concat(TYPE);
  clientID.concat("/");
  clientID.concat(SMART);
  clientID.concat("/");
  clientID.concat(SENSOR);


  if (!client.connected()) {
    Serial.print("Attempting MQTT connection…");



    char bf[200];
    clientID.toCharArray(bf, 200);
    
    if (client.connect(bf ,  mqtt_user, mqtt_password)) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc = ");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
      return;
    }
  }


  float h = dht.readHumidity();
  float t = dht.readTemperature();

  if (isnan(h) || isnan(t)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }

  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();

  root["temperature"] = t;
  root["humidity"] = h;
  root["smart"] = SMART;
  root["sensor"] = SENSOR;
  root["type"] = TYPE;

  String outData = "";
  root.printTo(outData);

  char bufferID[200];
  char bufferData[200];

  clientID.toCharArray(bufferID, 200);
  outData.toCharArray(bufferData, 200);

  client.publish(bufferID, bufferData);
  client.publish("sensor", bufferData);
  client.loop();

}
