#include "Wire.h"

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <SPI.h>

#define T 25

#define ADDWATER (0x48)
#define ADDLAMP (0x4C)

#define LAMP 15
#define WATER1 13
#define FOG 12
#define WATER3   14
#define WATER2 16

#define SMART 3
#define TYPE "control"



#define mqtt_server "35.240.153.210"
#define mqtt_port 1883
#define mqtt_user "win"
#define mqtt_password "@#Win2020"


const char* ssid = "WCS-WiFi";
const char* password = "woranari2021";


bool statusRelay = false;

byte aF, aW1, aW2, aW3 , aLamp;

//byte adcvalue0, adcvalue1, adcvalue2, adcvalue3 , adcvalue4;


int appControlW1;
int appControlW2;
int appControlW3;
int appControlF;
int appControlL;



boolean statusMobileW1 = false;
boolean statusRawW1 = false;
boolean statusIOW1 = false;


boolean statusMobileW2 = false;
boolean statusRawW2 = false;
boolean statusIOW2 = false;


boolean statusMobileW3 = false;
boolean statusRawW3 = false;
boolean statusIOW3 = false;


boolean statusMobileF = false;
boolean statusRawF = false;
boolean statusIOF = false;


boolean statusMobileL = false;
boolean statusRawL = false;
boolean statusIOL = false;


unsigned long timer = 0;


WiFiClient espClient;
PubSubClient client(espClient);

void setup() {
  pinMode(WATER1 , OUTPUT);
  pinMode(WATER2 , OUTPUT);
  pinMode(WATER3 , OUTPUT);
  pinMode(FOG  , OUTPUT);
  pinMode(LAMP , OUTPUT);

  digitalWrite(WATER1 , LOW);
  digitalWrite(WATER2 , LOW);
  digitalWrite(WATER3 , LOW);
  digitalWrite(FOG , LOW);
  digitalWrite(LAMP , LOW);


  Wire.begin();
  Serial.begin(9600);


  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  String mac = WiFi.macAddress();

  client.setServer(mqtt_server, mqtt_port);

  client.setCallback(callback);

}

void callback(char* topic, byte* payload, unsigned int length) {


  String timerSub = "";

  timerSub.concat("timer");
  timerSub.concat("/");
  timerSub.concat(SMART);


  String control = "";

  control.concat("control/");
  control.concat(SMART);

  String t = String(topic);

  if (t == timerSub) {

    char responseData[200];

    for (int i = 0; i < length; i++) {
      responseData[i] = (char)payload[i];
    }

    Serial.println(responseData);
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(responseData);


    int w1 = 2;
    int w2 = 2;
    int w3 = 2;
    int l = 2;
    int f = 2;




    if (root.containsKey("W1") == true ) {

      w1 = root["W1"];

      Serial.println(aW1);
      Serial.println(w1);

      Serial.print(statusRelay);
      if (aW1 >= T) {
        Serial.println("W1 ON");

        if (w1 == 0) {

          if (statusIOW1 == false) {
            digitalWrite(WATER1  , 1);
            statusIOW1 = true;
          } else {
            digitalWrite(WATER1  , 0);
            statusIOW1 = false;
          }

        }


      } else {
        Serial.println("W1 OFF");

        if (w1 == 1) {
          if (statusIOW1 == true) {
            digitalWrite(WATER1  , 0);
            statusIOW1 = false;
          } else {
            digitalWrite(WATER1  , 1);
            statusIOW1 = true;
          }
        }

      }
    }

    if (root.containsKey("W2") == true ) {

      Serial.println(aW2);
      w2 = root["W2"];
      if (aW2 >= T) {

        if (w2 == 0) {

          if (statusIOW2 == false) {
            digitalWrite(WATER2  , 1);
            statusIOW2 = true;
          } else {
            digitalWrite(WATER2  , 0);
            statusIOW2 = false;
          }

        }


      } else {

        if (w2 == 1) {
          if (statusIOW2 == true) {
            digitalWrite(WATER2  , 0);
            statusIOW2 = false;
          } else {
            digitalWrite(WATER2  , 1);
            statusIOW2 = true;
          }
        }

      }
    }



    if (root.containsKey("W3") == true ) {

      Serial.println(aW3);
      w3 = root["W3"];
      if (aW3 >= T) {

        if (w3 == 0) {

          if (statusIOW3 == false) {
            digitalWrite(WATER3  , 1);
            statusIOW3 = true;
          } else {
            digitalWrite(WATER3  , 0);
            statusIOW3 = false;
          }


        }


      } else {

        if (w3 == 1) {
          if (statusIOW3 == true) {
            digitalWrite(WATER3  , 0);
            statusIOW3 = false;
          } else {
            digitalWrite(WATER3  , 1);
            statusIOW3 = true;
          }

        }

      }
    }


    if (root.containsKey("F") == true ) {

      Serial.println(aF);
      f = root["F"];
      if (aF >= T) {

        if (f == 0) {

          if (statusIOF == false) {
            digitalWrite(FOG  , 1);
            statusIOF = true;
          } else {
            digitalWrite(FOG  , 0);
            statusIOF = false;
          }

        }


      } else {

        if (f == 1) {
          if (statusIOF == true) {
            digitalWrite(FOG  , 0);
            statusIOF = false;
          } else {
            digitalWrite(FOG  , 1);
            statusIOF = true;
          }

        }

      }
    }



    if (root.containsKey("L") == true ) {

      l = root["L"];
      if (l == 0) {
        digitalWrite(LAMP  , 0);
      }

      if (l == 1) {
        digitalWrite(LAMP  , 1);
      }

    }


  } else if (t ==  control) {
    char responseData[200];

    for (int i = 0; i < length; i++) {
      responseData[i] = (char)payload[i];
    }


    Serial.println(responseData);
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(responseData);


    appControlW1 = root["W1"];
    appControlW2 = root["W2"];
    appControlW3 = root["W3"];
    appControlF = root["F"];
    appControlL = root["L"];

    if (appControlW1 == 1) {

      statusIOW1 = !statusIOW1;
      statusMobileW1 = true;

      statusRelay = statusIOW1;


      digitalWrite(WATER1  , statusIOW1);

      Serial.print("S1 : ");
      Serial.println(statusIOW1);

    }

    if (appControlW2 == 1) {

      statusIOW2 = !statusIOW2;
      statusMobileW2 = true;
      digitalWrite(WATER2  , statusIOW2);

      Serial.print("S2 : ");
      Serial.println(statusIOW2);

    }

    if (appControlW3 == 1) {

      statusIOW3 = !statusIOW3;
      statusMobileW3 = true;
      digitalWrite(WATER3  , statusIOW3);

      Serial.print("S3 : ");
      Serial.println(statusIOW3);

    }

    if (appControlF == 1) {

      statusIOF = !statusIOF;
      statusMobileF = true;
      //    digitalWrite(16  , statusIOS1);

      digitalWrite(FOG  , statusIOF);
      Serial.print("F : ");
      Serial.println(statusIOF);

    }

    if (appControlL == 1) {

      statusIOL = !statusIOL;
      statusMobileL = true;
      digitalWrite(LAMP  , statusIOL);

      Serial.print("L : ");
      Serial.println(statusIOL);

    }

  } else {
    char responseData[200];

    for (int i = 0; i < length; i++) {
      responseData[i] = (char)payload[i];
    }


    Serial.println(responseData);
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(responseData);


    int lamp = root["lamp"];

    digitalWrite(LAMP  , lamp);

  }








}


void loop() {



  client.loop();

  String clientID  = "";

  clientID.concat(TYPE);
  clientID.concat("/");
  clientID.concat(SMART);



  String timerSub = "";

  timerSub.concat("timer");
  timerSub.concat("/");
  timerSub.concat(SMART);


  String lamp = "";


  lamp.concat(TYPE);
  lamp.concat("/lamp/");
  lamp.concat(SMART);


  if (!client.connected()) {
    Serial.print("Attempting MQTT connection…");


    char bf[200];
    clientID.toCharArray(bf, 200);

    char dt[200];
    timerSub.toCharArray(dt, 200);

    char lp[200];
    lamp.toCharArray(lp, 200);



    if (client.connect(bf ,  mqtt_user, mqtt_password)) {

      client.subscribe(bf);
      client.subscribe(dt);
      client.subscribe(lp);

      Serial.println("connected");
    } else {
      Serial.print("failed, rc = ");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
      return;
    }
  }



  if (millis() - timer > 200) {
    timer = millis();
    char JSONmessageBuffer[100];

    StaticJsonBuffer<300> JSONbuffer;
    JsonObject& JSONencoder = JSONbuffer.createObject();


    Wire.beginTransmission(ADDWATER);
    Wire.write(0x04);
    Wire.endTransmission();
    Wire.requestFrom(ADDWATER, 4);

    aW2 = Wire.read();
    aW1 = Wire.read();
    aF = Wire.read();
    aW3 = Wire.read();

    Wire.beginTransmission(ADDLAMP);
    Wire.write(0x04);
    Wire.endTransmission();
    Wire.requestFrom(ADDLAMP, 1);

    aLamp = Wire.read();

    //    Serial.print(aW1);
    //    Serial.print(" ,");
    //    Serial.print(aW2);
    //    Serial.print(" ,");
    //    Serial.print(aW3);
    //    Serial.print(" ,");
    //    Serial.print(aF);
    //    Serial.print(" ,");
    //    Serial.print(aLamp);
    //    Serial.println();

    Wire.endTransmission();


    boolean w1;
    boolean w2;
    boolean w3;

    boolean f;
    boolean l;



    if (aW1 >= T) {
      w1 = true;
    } else {
      w1 = false;
    }

    if (aW2 >= T) {
      f = true;
    } else {
      f = false;
    }


    if (aW3 >= T) {
      w3 = true;
    } else {
      w3 = false;
    }


    if (aF >= T) {
     w2 = true;
    } else {
      w2 = false;
    }

    if (aLamp >= T) {
      l = true;
    } else {
      l = false;
    }

    String controlW1 = "Mobile";
    String controlW2 = "Mobile";
    String controlW3 = "Mobile";

    String controlF = "Mobile";
    String controlL = "Mobile";



    if (statusMobileW1  != true  && statusRawW1 != w1) {


    } else {

      statusRawW1 = w1;
      statusMobileW1 = false;
    }



    if (statusMobileW2  != true  && statusRawW2 != w2) {

    } else {

      statusRawW2 = w2;
      statusMobileW2 = false;
    }

    if (statusMobileW3  != true  && statusRawW3 != w3) {

    } else {

      statusRawW3 = w3;
      statusMobileW3 = false;
    }

    if (statusMobileF  != true  && statusRawF != f) {

    } else {

      statusRawF = f;
      statusMobileF = false;
    }

    if (statusMobileL  != true  && statusRawL != l) {

    } else {

      statusRawL = l;
      statusMobileL = false;
    }



    JSONencoder["W1"] =  w1;
    JSONencoder["W2"] =  w2;
    JSONencoder["W3"] =  w3;
    JSONencoder["F"] =  f;
    JSONencoder["L"] =  l;




    String topic  = "";

    topic.concat("wcs/smart/");
    topic.concat(SMART);

    char bfTopic[200];
    topic.toCharArray(bfTopic, 200);



    JSONencoder.printTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));
    client.publish(bfTopic, JSONmessageBuffer);


  }
}
