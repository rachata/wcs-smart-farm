import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wcs_smart_farm/app_theme.dart';
import 'package:wcs_smart_farm/config.dart';
import 'package:wcs_smart_farm/model/notification.dart';

class AlertPage extends StatefulWidget {
  const AlertPage({Key key}) : super(key: key);

  @override
  AlertPageState createState() => AlertPageState();
}

class AlertPageState extends State<AlertPage> with TickerProviderStateMixin {
  final ScrollController scrollController = ScrollController();

  Animation<double> topBarAnimation;
  double topBarOpacity = 0.0;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  ProgressDialog pr;


  bool stateNoti = false;
  List<Widget> noti = [];
  bool stateShow = true;

  @override
  void initState() {
    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Download,
      textDirection: TextDirection.ltr,
      isDismissible: false,
    );

    pr.style(
        message: "Please wait...",
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        insetAnimCurve: Curves.easeInOut,
        progressTextStyle: TextStyle(
            fontFamily: AppTheme.fontName,
            color: Colors.black,
            fontSize: 13.0,
            fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            fontFamily: AppTheme.fontName,
            color: Colors.black,
            fontSize: 19.0,
            fontWeight: FontWeight.w600));

    AnimationController animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);

    topBarAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: animationController,
            curve: Interval(0, 0.5, curve: Curves.fastOutSlowIn)));

    scrollController.addListener(() {
      if (scrollController.offset >= 24) {
        if (topBarOpacity != 1.0) {
          setState(() {
            topBarOpacity = 1.0;
          });
        }
      } else if (scrollController.offset <= 24 &&
          scrollController.offset >= 0) {
        if (topBarOpacity != scrollController.offset / 24) {
          setState(() {
            topBarOpacity = scrollController.offset / 24;
          });
        }
      } else if (scrollController.offset <= 0) {
        if (topBarOpacity != 0.0) {
          setState(() {
            topBarOpacity = 0.0;
          });
        }
      }
    });

    this.getNotification();

    super.initState();
  }

  void getNotification() async {
//    await pr.show();

    Response response = await get("${Config.baseURL}/notification");

    print("response.body.toString() ${response.body.toString()}");
    final jsonResponse = json.decode(response.body.toString());

    WCSNotification notification = WCSNotification.fromJson(jsonResponse);

    print(notification.status);
    if (notification.status == 200) {
      setState(() {
        stateNoti = true;
      });
      createListNotification(notification);

//      pr.hide();
    } else {
      setState(() {
        stateNoti = false;
      });

//      pr.hide();
    }
  }

  void createListNotification(WCSNotification notification) {
    print("createListNotification");

    for (int i = 0; i < notification.message.length; i++) {
      print("createListNotification ${i}");

      setState(() {
        noti.add(
          Container(
              child: Padding(
            padding: EdgeInsets.all(0),
            child: Card(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  side: new BorderSide(color: AppTheme.wcsPrimary, width: 2.0),
                  borderRadius: BorderRadius.circular(12.0),
                ),
                elevation: 5,
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                margin: const EdgeInsets.only(top: 5.0),
                                child: new Text(
                                  "${notification.message[i].title}",
                                  style: TextStyle(
                                      fontFamily: AppTheme.fontName,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700),
                                ),
                              ),
                            ],
                          ))
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            "${notification.message[i].dt}",
                            style: TextStyle(
                                color: AppTheme.grey,
                                fontWeight: FontWeight.w400,
                                fontFamily: AppTheme.fontName,
                                fontSize: 12),
                          )
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                margin: const EdgeInsets.only(top: 5.0),
                                child: new Text(
                                  "${notification.message[i].message}",
                                  style: TextStyle(
                                      fontFamily: AppTheme.fontName,
                                      fontSize: 16),
                                ),
                              ),
                            ],
                          ))
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            "รายละเอียด",
                            style: TextStyle(
                                color: Colors.red,
                                fontWeight: FontWeight.w600,
                                fontFamily: AppTheme.fontName,
                                fontSize: 16),
                          )
                        ],
                      ),
                    ],
                  ),
                )),
          )),
        );
      });
      stateShow = true;
    }

    print("stateShow ${stateShow} state ${stateNoti}");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "แจ้งเตือน",
          style: TextStyle(fontFamily: AppTheme.fontName),
        ),
        backgroundColor: AppTheme.wcsPrimary,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
          child: Padding(
        padding: EdgeInsets.all(10),
        child: stateNoti == true
            ? Column(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: stateShow == false ? [] : noti,
                  ),
                ],
              )
            : Center(
                child: Column(
                children: [
                  Text(
                    "ไม่มีการแจ้งเตือนในขณะนี้",
                    style: TextStyle(
                        color: AppTheme.wcsRed,
                        fontFamily: AppTheme.fontName,
                        fontSize: 18,
                        fontWeight: FontWeight.w800),
                  ),
                  Loading(
                      indicator: BallPulseIndicator(),
                      size: 100.0,
                      color: AppTheme.wcsRed),
                ],
              )),
      )),
    );
  }
}
