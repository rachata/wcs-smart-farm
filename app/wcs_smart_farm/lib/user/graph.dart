import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_material_pickers/flutter_material_pickers.dart';
import 'package:http/http.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:wcs_smart_farm/app_theme.dart';
import 'package:wcs_smart_farm/config.dart';
import 'package:wcs_smart_farm/model/LogTempHum.dart';
import 'package:wcs_smart_farm/user/detail.dart';
import 'package:intl/intl.dart';

class GraphPage extends StatefulWidget {
  final int farm;
  final int subFarm;

  const GraphPage({Key key, this.farm, this.subFarm}) : super(key: key);

  @override
  GraphPageState createState() => GraphPageState();
}

class GraphPageState extends State<GraphPage> with TickerProviderStateMixin {
  final ScrollController scrollController = ScrollController();

  Animation<double> topBarAnimation;
  double topBarOpacity = 0.0;

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  int _value = 0;

  List<GraphData> temp = [];
  List<GraphData> hum = [];
  List<GraphData> soil = [];

  List<String> years = [
    "2020",
    "2021",
    "2022",
    "2023",
    "2024",
    "2025",
    "2026",
    "2027",
    "2028",
    "2029",
    "2030"
  ];
  var selectYear = "";

  String start = "";
  String end;

  ProgressDialog pr;

  makeGraphRealTimeSoil() {
    return SfCartesianChart(
        zoomPanBehavior:
            ZoomPanBehavior(enablePanning: true, enablePinching: true),
        plotAreaBackgroundColor: Colors.white70,
        primaryYAxis: NumericAxis(
            labelFormat: '{value}',
            title: AxisTitle(
                text: '',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: AppTheme.fontName,
                    fontSize: 16,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        primaryXAxis: CategoryAxis(
            labelRotation: 60,
            title: AxisTitle(
                text: '',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: AppTheme.fontName,
                    fontSize: 20,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        // Chart title
        title: ChartTitle(
            text: 'กราฟความชื้นในดิน',
            textStyle: ChartTextStyle(
                fontFamily: AppTheme.fontName,
                color: AppTheme.wcsPrimary,
                fontSize: 14,
                fontWeight: FontWeight.w600)),
        // Enable legend
        legend: Legend(isVisible: true, position: LegendPosition.bottom),

        // Enable tooltip
        tooltipBehavior: TooltipBehavior(enable: true),
        series: <ChartSeries<GraphData, String>>[
          SplineSeries<GraphData, String>(
            markerSettings: MarkerSettings(
                isVisible: true,
                // Marker shape is set to diamond
                shape: DataMarkerType.circle),
            isVisible: true,
            animationDuration: 0.5,
            width: 2,
            name: "ความชื้นในดิน",
            dataSource: soil,
            xValueMapper: (GraphData graph, _) => graph.x.toString(),
            yValueMapper: (GraphData graph, _) => graph.y,
            color: AppTheme.wcsOrange,
          ),
        ]);
  }

  makeGraphRealTimeHum() {
    return SfCartesianChart(
        zoomPanBehavior:
            ZoomPanBehavior(enablePanning: true, enablePinching: true),
        plotAreaBackgroundColor: Colors.white70,
        primaryYAxis: NumericAxis(
            labelFormat: '{value}',
            title: AxisTitle(
                text: '',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: AppTheme.fontName,
                    fontSize: 16,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        primaryXAxis: CategoryAxis(
            labelRotation: 60,
            title: AxisTitle(
                text: '',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: AppTheme.fontName,
                    fontSize: 20,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        // Chart title
        title: ChartTitle(
            text: 'กราฟอุณหภูมิ และความชื้นในอากาศ',
            textStyle: ChartTextStyle(
                fontFamily: AppTheme.fontName,
                color: AppTheme.wcsPrimary,
                fontSize: 14,
                fontWeight: FontWeight.w600)),
        // Enable legend
        legend: Legend(isVisible: true, position: LegendPosition.bottom),

        // Enable tooltip
        tooltipBehavior: TooltipBehavior(enable: true),
        series: <ChartSeries<GraphData, String>>[
          SplineSeries<GraphData, String>(
            markerSettings: MarkerSettings(
                isVisible: true,
                // Marker shape is set to diamond
                shape: DataMarkerType.circle),
            isVisible: true,
            animationDuration: 0.5,
            width: 2,
            name: "อุณหภูมิ °C",
            dataSource: temp,
            xValueMapper: (GraphData graph, _) => graph.x.toString(),
            yValueMapper: (GraphData graph, _) => graph.y,
            color: AppTheme.wcsBlue,
          ),
          SplineSeries<GraphData, String>(
            markerSettings: MarkerSettings(
                isVisible: true,
                // Marker shape is set to diamond
                shape: DataMarkerType.circle),
            isVisible: true,
            animationDuration: 0.5,
            width: 2,
            name: "ความชื้นในอากาศ %",
            dataSource: hum,
            xValueMapper: (GraphData graph, _) => graph.x.toString(),
            yValueMapper: (GraphData graph, _) => graph.y,
            color: AppTheme.wcsGreen,
          ),
        ]);
  }

  @override
  void initState() {

    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Download,
      isDismissible: false,
    );

    pr.style(
        message: "Please wait...",
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        insetAnimCurve: Curves.easeInOut,
        progressTextStyle: TextStyle(
            fontFamily: AppTheme.fontName,
            color: Colors.black,
            fontSize: 13.0,
            fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            fontFamily: AppTheme.fontName,
            color: Colors.black,
            fontSize: 19.0,
            fontWeight: FontWeight.w600));


    AnimationController animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);

    topBarAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: animationController,
            curve: Interval(0, 0.5, curve: Curves.fastOutSlowIn)));

    scrollController.addListener(() {
      if (scrollController.offset >= 24) {
        if (topBarOpacity != 1.0) {
          setState(() {
            topBarOpacity = 1.0;
          });
        }
      } else if (scrollController.offset <= 24 &&
          scrollController.offset >= 0) {
        if (topBarOpacity != scrollController.offset / 24) {
          setState(() {
            topBarOpacity = scrollController.offset / 24;
          });
        }
      } else if (scrollController.offset <= 0) {
        if (topBarOpacity != 0.0) {
          setState(() {
            topBarOpacity = 0.0;
          });
        }
      }
    });

    super.initState();
  }

  void callDatePicker() async {
    var order = await getDate();
    setState(() {
      print(order.toIso8601String());

      final DateFormat formatter = DateFormat('yyyy-MM-dd');
      String m = formatter.format(order);
      callGetDay(m);
    });
  }

  Future<DateTime> getDate() {
    return showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: Theme.of(context).copyWith(
            primaryColor: AppTheme.wcsPrimary,
          ),
          child: child,
        );
      },
    );
  }

  void callGetDay(String data) async {

    await pr.show();
    Response response = await get(
        "${Config.baseURL}/log/day/temp/${widget.farm}/${widget.subFarm}/${data}",
        headers: <String, String>{
          'Accept': 'application/json; charset=UTF-8',
          'Content-Type': 'application/json; charset=UTF-8',
        });

    final jsonResponseTempHum = json.decode(response.body.toString());

    response = await get(
        "${Config.baseURL}/log/day/soil/${widget.farm}/${widget.subFarm}/${data}",
        headers: <String, String>{
          'Accept': 'application/json; charset=UTF-8',
          'Content-Type': 'application/json; charset=UTF-8',
        });

    final jsonResponseSoil = json.decode(response.body.toString());

    addDataToGraphTempHum(jsonResponseTempHum);
    addDataToGraphSoil(jsonResponseSoil);

    pr.hide();
  }

  void callMonthPicker() {
    showMonthPicker(
      context: context,
      initialDate: DateTime.now(),
      locale: Locale("th"),
    ).then((date) {
      if (date != null) {
        setState(() {
          print("date ${date}");

          final DateFormat formatter = DateFormat('yyyy-MM-dd');
          String m = formatter.format(date);

          callGetMonth(m);
        });
      }
    });
  }

  void callGetMonth(String data) async {

    await pr.show();

    Response response = await get(
        "${Config.baseURL}/log/month/temp/${widget.farm}/${widget.subFarm}/${data}",
        headers: <String, String>{
          'Accept': 'application/json; charset=UTF-8',
          'Content-Type': 'application/json; charset=UTF-8',
        });

    final jsonResponseTempHum = json.decode(response.body.toString());

    response = await get(
        "${Config.baseURL}/log/month/soil/${widget.farm}/${widget.subFarm}/${data}",
        headers: <String, String>{
          'Accept': 'application/json; charset=UTF-8',
          'Content-Type': 'application/json; charset=UTF-8',
        });

    final jsonResponseSoil = json.decode(response.body.toString());

    addDataToGraphTempHum(jsonResponseTempHum);
    addDataToGraphSoil(jsonResponseSoil);

    pr.hide();
  }

  void callYear() {
    showMaterialScrollPicker(
      context: context,
      title: "เลือกปีที่ต้องการ",
      items: years,
      selectedItem: selectYear,
      onChanged: (value) => {callGetYear(value)},
    );
  }

  void callGetYear(String data) async {

    await pr.show();
    Response response = await get(
        "${Config.baseURL}/log/year/temp/${widget.farm}/${widget.subFarm}/${data}",
        headers: <String, String>{
          'Accept': 'application/json; charset=UTF-8',
          'Content-Type': 'application/json; charset=UTF-8',
        });

    final jsonResponseTempHum = json.decode(response.body.toString());

    response = await get(
        "${Config.baseURL}/log/year/soil/${widget.farm}/${widget.subFarm}/${data}",
        headers: <String, String>{
          'Accept': 'application/json; charset=UTF-8',
          'Content-Type': 'application/json; charset=UTF-8',
        });

    final jsonResponseSoil = json.decode(response.body.toString());

    addDataToGraphTempHum(jsonResponseTempHum);
    addDataToGraphSoil(jsonResponseSoil);

    pr.hide();
  }

  void callAdvanced() {


    final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm:ss');

    DatePicker.showDateTimePicker(context,
        showTitleActions: true, maxTime: new DateTime.now(), onChanged: (date) {
      print('change $date');
    }, onConfirm: (date) {
      start = formatter.format(date);

      DatePicker.showDateTimePicker(context,
          showTitleActions: true,
          maxTime: new DateTime.now(), onChanged: (date) {
        print('change $date');
      }, onConfirm: (date) {
        end = formatter.format(date);

        if (start != "" && end != "") {
          print("vvv ${start} ${end}");
          callGetAdvanced(start, end);
        }
      }, currentTime: DateTime.now(), locale: LocaleType.th);
    }, currentTime: DateTime.now(), locale: LocaleType.th);
  }

  void callGetAdvanced(String start, String end) async {

    await pr.show();
    Response response = await get(
        "${Config.baseURL}/log/ad/temp/${widget.farm}/${widget.subFarm}/${start}/${end}",
        headers: <String, String>{
          'Accept': 'application/json; charset=UTF-8',
          'Content-Type': 'application/json; charset=UTF-8',
        });

    final jsonResponseTempHum = json.decode(response.body.toString());

    response = await get(
        "${Config.baseURL}/log/ad/soil/${widget.farm}/${widget.subFarm}/${start}/${end}",
        headers: <String, String>{
          'Accept': 'application/json; charset=UTF-8',
          'Content-Type': 'application/json; charset=UTF-8',
        });

    final jsonResponseSoil = json.decode(response.body.toString());

    addDataToGraphTempHum(jsonResponseTempHum);
    addDataToGraphSoil(jsonResponseSoil);

    pr.hide();
  }

  void addDataToGraphTempHum(dynamic jsonResponse) {
    setState(() {
      temp = [];
      hum = [];
    });

    if (jsonResponse["status"] == 200) {
      for (int i = 0; i < jsonResponse["message"].length; i++) {
        setState(() {
          temp.add(GraphData(jsonResponse["message"][i]["dtf"],
              jsonResponse["message"][i]["temp"] / 1.0));
          hum.add(GraphData(jsonResponse["message"][i]["dtf"],
              jsonResponse["message"][i]["hum"] / 1.0));
        });
      }
    }
  }

  void addDataToGraphSoil(dynamic jsonResponse) {
    setState(() {
      soil = [];
    });

    if (jsonResponse["status"] == 200) {
      for (int i = 0; i < jsonResponse["message"].length; i++) {
        setState(() {
          soil.add(GraphData(jsonResponse["message"][i]["dtf"],
              jsonResponse["message"][i]["soil"] / 1.0));
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "ข้อมูลย้อนหลัง",
          style: TextStyle(fontFamily: AppTheme.fontName),
        ),
        backgroundColor: AppTheme.wcsPrimary,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
          child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  ChoiceChip(
                    label: Text(
                      "วัน",
                      style: TextStyle(
                          color: Colors.white, fontFamily: AppTheme.fontName),
                    ),
                    selected: _value == 1,
                    selectedColor: AppTheme.wcsRed,
                    onSelected: (bool value) {

                      temp = [];
                      soil = [];
                      hum = [];
                      callDatePicker();
                      setState(() {
                        _value = value ? 1 : null;
                      });
                    },
                    backgroundColor: AppTheme.wcsPrimary,
                    labelStyle: TextStyle(color: Colors.white),
                  ),
                  ChoiceChip(
                    label: Text("เดือน",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: AppTheme.fontName)),
                    selected: _value == 2,
                    selectedColor: AppTheme.wcsRed,
                    onSelected: (bool value) {
                      temp = [];
                      soil = [];
                      hum = [];

                      callMonthPicker();
                      setState(() {
                        _value = value ? 2 : null;
                      });
                    },
                    backgroundColor: AppTheme.wcsPrimary,
                    labelStyle: TextStyle(color: Colors.white),
                  ),
                  ChoiceChip(
                    label: Text("ปี",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: AppTheme.fontName)),
                    selected: _value == 3,
                    selectedColor: AppTheme.wcsRed,
                    onSelected: (bool value) {

                      temp = [];
                      soil = [];
                      hum = [];
                      
                      callYear();
                      setState(() {
                        _value = value ? 3 : null;
                      });
                    },
                    backgroundColor: AppTheme.wcsPrimary,
                    labelStyle: TextStyle(color: Colors.white),
                  ),
                  ChoiceChip(
                    label: Text("ขั้นสูง",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: AppTheme.fontName)),
                    selected: _value == 4,
                    selectedColor: AppTheme.wcsRed,
                    onSelected: (bool value) {
                      callAdvanced();
                      setState(() {
                        _value = value ? 4 : null;
                      });
                    },
                    backgroundColor: AppTheme.wcsPrimary,
                    labelStyle: TextStyle(color: Colors.white),
                  ),
                ]),
            Container(
                margin: EdgeInsets.all(0),
                child: Card(
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                      side: new BorderSide(
                          color: AppTheme.wcsPrimary, width: 2.0),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    elevation: 5,
                    child: Container(child: makeGraphRealTimeHum()))),
            Container(
                margin: EdgeInsets.all(0),
                child: Card(
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                      side: new BorderSide(
                          color: AppTheme.wcsPrimary, width: 2.0),
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    elevation: 5,
                    child: Container(child: makeGraphRealTimeSoil())))
          ],
        ),
      )),
    );
  }
}
