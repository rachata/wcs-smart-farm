import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:bottom_navigation_badge/bottom_navigation_badge.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wcs_smart_farm/app_theme.dart';
import 'package:wcs_smart_farm/config.dart';

class ResultPage extends StatefulWidget {
  final int farm;
  final int subFarm;

  const ResultPage({Key key, this.farm, this.subFarm}) : super(key: key);

  @override
  ResultPageState createState() => ResultPageState();
}

class ResultPageState extends State<ResultPage> with TickerProviderStateMixin {
  final ScrollController scrollController = ScrollController();

  Animation<double> topBarAnimation;
  double topBarOpacity = 0.0;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  File image;

  TextEditingController temp = TextEditingController(text : "");
  TextEditingController hum = TextEditingController(text : "");
  TextEditingController soil =TextEditingController(text : "");
  TextEditingController note = TextEditingController(text : "");
  TextEditingController priceIn = TextEditingController(text : "");
  TextEditingController priceOut = TextEditingController(text : "");

  ProgressDialog pr;

  cameraConnect() async {
    print('Picker is Called');
    File img = await ImagePicker.pickImage(source: ImageSource.camera);

    if (img != null) {
      image = img;
      setState(() {
        image = img;
      });
    }
  }

  @override
  void initState() {
    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Download,
      textDirection: TextDirection.ltr,
      isDismissible: false,
    );

    pr.style(
        message: "Please wait...",
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        insetAnimCurve: Curves.easeInOut,
        progressTextStyle: TextStyle(
            color: AppTheme.wcsPrimary,
            fontSize: 13.0,
            fontFamily: AppTheme.fontName,
            fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: AppTheme.wcsPrimary,
            fontSize: 19.0,
            fontFamily: AppTheme.fontName,
            fontWeight: FontWeight.w600));

    AnimationController animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);

    topBarAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: animationController,
            curve: Interval(0, 0.5, curve: Curves.fastOutSlowIn)));

    scrollController.addListener(() {
      if (scrollController.offset >= 24) {
        if (topBarOpacity != 1.0) {
          setState(() {
            topBarOpacity = 1.0;
          });
        }
      } else if (scrollController.offset <= 24 &&
          scrollController.offset >= 0) {
        if (topBarOpacity != scrollController.offset / 24) {
          setState(() {
            topBarOpacity = scrollController.offset / 24;
          });
        }
      } else if (scrollController.offset <= 0) {
        if (topBarOpacity != 0.0) {
          setState(() {
            topBarOpacity = 0.0;
          });
        }
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "เก็บผล",
          style: TextStyle(fontFamily: AppTheme.fontName),
        ),
        backgroundColor: AppTheme.wcsPrimary,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Padding(padding: EdgeInsets.only(bottom: 10)),
                      Container(
                          width: MediaQuery.of(context).size.width - 20,
                          child: new Theme(
                            data: new ThemeData(
                              primaryColor: AppTheme.wcsRed,
                              primaryColorDark: AppTheme.wcsRed,
                            ),
                            child: new TextField(
                              controller: temp,
                              keyboardType: TextInputType.number,
                              style: TextStyle(fontFamily: AppTheme.fontName),
                              decoration: new InputDecoration(
                                  border: new OutlineInputBorder(
                                      borderSide: new BorderSide(
                                          color: AppTheme.wcsPrimary)),
                                  hintText: 'อุณหภูมิ',
                                  labelText: 'อุณหภูมิ',
                                  suffix: Text("°C",
                                      style: TextStyle(
                                          fontFamily: AppTheme.fontName))),
                            ),
                          )),
                      Padding(padding: EdgeInsets.only(bottom: 10)),
                      Container(
                          width: MediaQuery.of(context).size.width - 20,
                          child: new Theme(
                            data: new ThemeData(
                              primaryColor: AppTheme.wcsRed,
                              primaryColorDark: AppTheme.wcsRed,
                            ),
                            child: new TextField(
                              controller: hum,
                              keyboardType: TextInputType.number,
                              style: TextStyle(fontFamily: AppTheme.fontName),
                              decoration: new InputDecoration(
                                  border: new OutlineInputBorder(
                                      borderSide: new BorderSide(
                                          color: AppTheme.wcsPrimary)),
                                  hintText: 'ความชื้นในอากาศ',
                                  suffix: Text("%",
                                      style: TextStyle(
                                          fontFamily: AppTheme.fontName))),
                            ),
                          )),
                      Padding(padding: EdgeInsets.only(bottom: 10)),
                      Container(
                          width: MediaQuery.of(context).size.width - 20,
                          child: new Theme(
                            data: new ThemeData(
                              primaryColor: AppTheme.wcsRed,
                              primaryColorDark: AppTheme.wcsRed,
                            ),
                            child: new TextField(
                              controller: soil,
                              keyboardType: TextInputType.number,
                              style: TextStyle(fontFamily: AppTheme.fontName),
                              decoration: new InputDecoration(
                                border: new OutlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: AppTheme.wcsPrimary)),
                                hintText: 'ความชื้นในดิน',
                                labelText: 'ความชื้นในดิน',
                              ),
                            ),
                          )),
                      Padding(padding: EdgeInsets.only(bottom: 10)),
                      Container(
                          width: MediaQuery.of(context).size.width - 20,
                          child: new Theme(
                            data: new ThemeData(
                              primaryColor: AppTheme.wcsRed,
                              primaryColorDark: AppTheme.wcsRed,
                            ),
                            child: new TextField(
                              maxLines: 5,
                              controller: note,

                              keyboardType: TextInputType.text,
                              style: TextStyle(fontFamily: AppTheme.fontName),
                              decoration: new InputDecoration(
                                border: new OutlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: AppTheme.wcsPrimary)),
                                hintText: 'อธิบาย',
                                labelText: 'อธิบาย',
                              ),
                            ),
                          )),
                      Padding(padding: EdgeInsets.only(bottom: 10)),
                      Container(
                        width: MediaQuery.of(context).size.width - 20,
                        child: new Theme(
                          data: new ThemeData(
                            primaryColor: AppTheme.wcsRed,
                            primaryColorDark: AppTheme.wcsRed,
                          ),
                          child: new TextField(
                            controller: priceOut,
                            keyboardType: TextInputType.number,
                            style: TextStyle(fontFamily: AppTheme.fontName),
                            decoration: new InputDecoration(
                                border: new OutlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: AppTheme.wcsPrimary)),
                                hintText: 'รายจ่าย',
                                labelText: 'รายจ่าย',
                                suffix: Text("บาท",
                                    style: TextStyle(
                                        fontFamily: AppTheme.fontName))),
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(bottom: 10)),
                      Container(
                        width: MediaQuery.of(context).size.width - 20,
                        child: new Theme(
                          data: new ThemeData(
                            primaryColor: AppTheme.wcsRed,
                            primaryColorDark: AppTheme.wcsRed,
                          ),
                          child: new TextField(
                            controller: priceIn,
                            keyboardType: TextInputType.number,
                            style: TextStyle(fontFamily: AppTheme.fontName),
                            decoration: new InputDecoration(
                                border: new OutlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: AppTheme.wcsPrimary)),
                                hintText: 'รายรับ',
                                labelText: 'รายรับ',
                                suffix: Text("บาท",
                                    style: TextStyle(
                                        fontFamily: AppTheme.fontName))),
                          ),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(bottom: 10)),

                      GestureDetector(
                        child: Container(
                          width: MediaQuery.of(context).size.width - 25,
                          child: DottedBorder(
                            color: Colors.redAccent,
                            strokeWidth: 1,
                            child: image == null
                                ? Text(
                              "Upload Image",
                              style: TextStyle(fontSize: 20),
                            )
                                : Image.file(image),
                          ),
                          height: 150,
                        ),
                        onTap: () => {cameraConnect()},
                      ),
                      Padding(padding: EdgeInsets.only(bottom: 10)),
                      Container(
                        height: 50,
                        child: new OutlineButton(
                          shape: StadiumBorder(),
                          textColor: AppTheme.dark_grey,
                          child: Text(
                            'บันทึกข้อมูล',
                            style: TextStyle(
                                fontFamily: AppTheme.fontName, fontSize: 20),
                          ),
                          borderSide: BorderSide(
                              color: AppTheme.wcsPrimary,
                              style: BorderStyle.solid,
                              width: 1),
                          onPressed: () {
                            updateDetail();
                          },
                        ),
                      )
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void updateDetail() async {
    FocusScope.of(context).requestFocus(FocusNode());
    await pr.show();
    final jsons = jsonEncode({
      "farm": widget.farm,
      "subFarm": widget.subFarm,
      "temp": temp.text.trim().toString(),
      "hum": hum.text.trim().toString(),
      "soil": soil.text.trim().toString(),
      "note": note.text.trim().toString(),
      "std": "1233",
      "price_out": priceOut.text.trim().toString() == "" ? double.parse("0.0") :  double.parse(priceOut.text.trim().toString()),
      "price_in": priceIn.text.trim().toString() == "" ? double.parse("0.0") :  double.parse(priceIn.text.trim().toString()),
    });

    Response response = await post("${Config.baseURL}/result",
        headers: <String, String>{
          'Accept': 'application/json; charset=UTF-8',
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsons);

    final jsonResponse = json.decode(response.body.toString());

    print(jsonResponse.toString());

    pr.hide();
    if (jsonResponse["status"] == 200) {


      print("image ${image}");
      if(image != null){


        updateProfile("${jsonResponse["message"]}");
      }else{
        _showMyDialog("บันทึกข้อมูลสำเร็จ");
      }

    } else {
      _showMyDialog("บันทึกข้อมูลไม่สำเร็จ");
    }
  }

  Future<void> _showMyDialog(String text) async {
    return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return new CupertinoAlertDialog(
            title: new Text(
              "โปรไฟล์",
              style: TextStyle(fontFamily: AppTheme.fontName),
            ),
            content: new Text("${text}"),
            actions: [
              CupertinoDialogAction(
                  isDefaultAction: true,
                  child: new Text("Ok"),
                  onPressed: () {
                    Navigator.pop(context, 'Cancel');
                  })
            ],
          );
        });
  }

  void updateProfile(String id) async {


    print("updateProfile");

    if (image.path != null || image.path != "") {
      await pr.show();

      var request =
          MultipartRequest("POST", Uri.parse("${Config.baseURL}/result/image"));

      request.fields["id"] = id;

      var pic = await MultipartFile.fromPath("image", image.path);

      request.files.add(pic);
      var response = await request.send();

      var responseData = await response.stream.toBytes();

      print(responseData);
      var responseString = String.fromCharCodes(responseData);
      print(responseString);

      final jsonResponse = json.decode(responseString.toString());

      if (jsonResponse['status'] == 200) {
        pr.hide();
        _showMyDialog("บันทึกสำเร็จ");
      } else {
        pr.hide();
        _showMyDialog("ผิดพลาดกรุณาลองใหม่อีกครั้ง");
      }
    }
  }
}
