import 'dart:async';
import 'package:bottom_navigation_badge/bottom_navigation_badge.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wcs_smart_farm/app_theme.dart';
import 'package:wcs_smart_farm/user/alert.dart';
import 'package:wcs_smart_farm/user/farm.dart';
import 'package:wcs_smart_farm/user/my_farm.dart';
import 'package:wcs_smart_farm/user/profile.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> with TickerProviderStateMixin {
  final ScrollController scrollController = ScrollController();

  Animation<double> topBarAnimation;
  double topBarOpacity = 0.0;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  BottomNavigationBadge badger = new BottomNavigationBadge(
      backgroundColor: Colors.red,
      badgeShape: BottomNavigationBadgeShape.circle,
      textColor: Colors.white,
      position: BottomNavigationBadgePosition.topRight,
      textSize: 10);

  List<BottomNavigationBarItem> items = [
    BottomNavigationBarItem(
        icon: Icon(
          Icons.home,
          color: AppTheme.white,
        ),
        title: Text(
          "โรงเรือน",
          style: TextStyle(fontFamily: AppTheme.fontName),
        )),
    BottomNavigationBarItem(
        icon: Icon(Icons.accessibility, color: AppTheme.white),
        title: Text("โรงเรือนของฉัน",
            style: TextStyle(fontFamily: AppTheme.fontName))),
    BottomNavigationBarItem(
        icon: Icon(Icons.add_alert, color: AppTheme.white),
        title:
            Text("แจ้งเตือน", style: TextStyle(fontFamily: AppTheme.fontName))),
    BottomNavigationBarItem(
        icon: Icon(Icons.account_box, color: AppTheme.white),
        title:
            Text("โปรไฟล์", style: TextStyle(fontFamily: AppTheme.fontName))),
  ];

  List<Widget> _widgetOptions = <Widget>[];

  int currentIndex = 0;

  final List<Widget> _children = [
    FarmPage(),
    MyFarmPage(),
    AlertPage(),
    ProfilePage()
  ];




  @override
  void initState() {
    currentIndex = 0;

    AnimationController animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);

    topBarAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: animationController,
            curve: Interval(0, 0.5, curve: Curves.fastOutSlowIn)));

    scrollController.addListener(() {
      if (scrollController.offset >= 24) {
        if (topBarOpacity != 1.0) {
          setState(() {
            topBarOpacity = 1.0;
          });
        }
      } else if (scrollController.offset <= 24 &&
          scrollController.offset >= 0) {
        if (topBarOpacity != scrollController.offset / 24) {
          setState(() {
            topBarOpacity = scrollController.offset / 24;
          });
        }
      } else if (scrollController.offset <= 0) {
        if (topBarOpacity != 0.0) {
          setState(() {
            topBarOpacity = 0.0;
          });
        }
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      backgroundColor: Colors.white,
      body: _children[currentIndex],
      bottomNavigationBar: BottomNavyBar(
        backgroundColor: AppTheme.wcsPrimary,
        selectedIndex: currentIndex,
        showElevation: true,
        itemCornerRadius: 8,
        curve: Curves.easeInBack,
        onItemSelected: (index) => setState(() {
          print(index);
          currentIndex = index;
        }),
        items: [
          BottomNavyBarItem(
            inactiveColor: AppTheme.white,
            icon: Icon(Icons.home),
            title: Text("โรงเรือน",
                style: TextStyle(fontFamily: AppTheme.fontName)),
            activeColor: AppTheme.wcsGreen,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            inactiveColor: AppTheme.white,
            icon: Icon(Icons.accessibility),
            title: Text("โรงเรือนของฉัน",
                style: TextStyle(fontFamily: AppTheme.fontName)),
            activeColor: AppTheme.wcsGreen,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            inactiveColor: AppTheme.white,
            icon: Icon(Icons.add_alert),
            title: Text("แจ้งเตือน",
                style: TextStyle(fontFamily: AppTheme.fontName)),
            activeColor: AppTheme.wcsGreen,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            inactiveColor: AppTheme.white,
            icon: Icon(Icons.account_box),
            title: Text("โปรไฟล์",
                style: TextStyle(fontFamily: AppTheme.fontName)),
            activeColor: AppTheme.wcsGreen,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
