import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wcs_smart_farm/app_theme.dart';
import 'package:wcs_smart_farm/config.dart';
import 'package:wcs_smart_farm/model/sub_farm.dart';
import 'package:wcs_smart_farm/user/sub_farm.dart';

class RequestPage extends StatefulWidget {
  final String farm;
  final String subFarm;

  final int idFarm;
  final int idSubFarm;

  final WCSSubFarm wcsSubFarm;

  const RequestPage(
      {Key key,
      this.idFarm,
      this.idSubFarm,
      this.farm,
      this.subFarm,
      this.wcsSubFarm})
      : super(key: key);

  @override
  RequestPageState createState() => RequestPageState();
}

class RequestPageState extends State<RequestPage>
    with TickerProviderStateMixin {
  final ScrollController scrollController = ScrollController();

  Animation<double> topBarAnimation;
  double topBarOpacity = 0.0;

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  ProgressDialog pr;

  TextEditingController stdText = new TextEditingController();

  TextEditingController note = new TextEditingController();
  List<String> std = [];

  @override
  void initState() {
    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Download,
      textDirection: TextDirection.ltr,
      isDismissible: false,
    );

    pr.style(
        message: "Please wait...",
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        insetAnimCurve: Curves.easeInOut,
        progressTextStyle: TextStyle(
            fontFamily: AppTheme.fontName,
            color: Colors.black,
            fontSize: 13.0,
            fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            fontFamily: AppTheme.fontName,
            color: Colors.black,
            fontSize: 19.0,
            fontWeight: FontWeight.w600));

    AnimationController animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);

    topBarAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: animationController,
            curve: Interval(0, 0.5, curve: Curves.fastOutSlowIn)));

    scrollController.addListener(() {
      if (scrollController.offset >= 24) {
        if (topBarOpacity != 1.0) {
          setState(() {
            topBarOpacity = 1.0;
          });
        }
      } else if (scrollController.offset <= 24 &&
          scrollController.offset >= 0) {
        if (topBarOpacity != scrollController.offset / 24) {
          setState(() {
            topBarOpacity = scrollController.offset / 24;
          });
        }
      } else if (scrollController.offset <= 0) {
        if (topBarOpacity != 0.0) {
          setState(() {
            topBarOpacity = 0.0;
          });
        }
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "ขอใช้งาน",
            style: TextStyle(fontFamily: AppTheme.fontName),
          ),
          backgroundColor: AppTheme.wcsPrimary,
        ),
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
            child: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(padding: EdgeInsets.only(bottom: 10)),
                Container(
                    width: MediaQuery.of(context).size.width - 20,
                    child: new Theme(
                      data: new ThemeData(
                        primaryColor: AppTheme.wcsRed,
                        primaryColorDark: AppTheme.wcsRed,
                      ),
                      child: new TextField(
                        readOnly: true,
                        controller: new TextEditingController(
                            text: "${widget.farm} ${widget.subFarm}"),
                        style: TextStyle(fontFamily: AppTheme.fontName),
                        decoration: new InputDecoration(
                          border: new OutlineInputBorder(
                              borderSide:
                                  new BorderSide(color: AppTheme.wcsPrimary)),
                          prefixIcon: const Icon(
                            Icons.home,
                            color: AppTheme.wcsPrimary,
                          ),
                        ),
                      ),
                    )),
                Padding(padding: EdgeInsets.only(bottom: 10)),
                Container(
                    width: MediaQuery.of(context).size.width - 20,
                    child: new Theme(
                      data: new ThemeData(
                        primaryColor: AppTheme.wcsRed,
                        primaryColorDark: AppTheme.wcsRed,
                      ),
                      child: new TextField(
                        controller: note,
                        maxLines: 3,
                        textAlign: TextAlign.left,
                        style: TextStyle(fontFamily: AppTheme.fontName),
                        decoration: new InputDecoration(
                          border: new OutlineInputBorder(
                              borderSide:
                                  new BorderSide(color: AppTheme.wcsPrimary)),
                          hintText: "คำอธิบาย",
                          prefixIcon: const Icon(
                            Icons.edit,
                            color: AppTheme.wcsPrimary,
                          ),
                        ),
                      ),
                    )),
                Padding(padding: EdgeInsets.only(bottom: 10)),
                Container(
                    width: MediaQuery.of(context).size.width - 20,
                    child: new Theme(
                        data: new ThemeData(
                          primaryColor: AppTheme.wcsRed,
                          primaryColorDark: AppTheme.wcsRed,
                        ),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "สมาชิก ${std.length}คน",
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600,
                                  color: AppTheme.wcsRed,
                                  fontFamily: AppTheme.fontName),
                            ),
                            IconButton(
                                icon: Icon(
                                  Icons.add,
                                  color: AppTheme.wcsGreen2,
                                  size: 30,
                                ),
                                onPressed: () => {_showMyDialogAdd()})
                          ],
                        ))),
                Padding(padding: EdgeInsets.only(bottom: 10)),
                Container(
                    height: MediaQuery.of(context).size.height * 0.3,
                    width: MediaQuery.of(context).size.width - 20,
                    child: new Theme(
                        data: new ThemeData(
                          primaryColor: AppTheme.wcsRed,
                          primaryColorDark: AppTheme.wcsRed,
                        ),
                        child: ListView.builder(
                            itemCount: std.length,
                            itemBuilder: (BuildContext ctxt, int index) {
                              return Container(
                                color: (index % 2 == 0)
                                    ? AppTheme.grey.withOpacity(0.7)
                                    : Colors.white,
                                child: ListTile(
                                    title: new Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "${std[index]}",
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w600,
                                          color: (index % 2 == 0)
                                              ? Colors.white
                                              : AppTheme.wcsPrimary,
                                          fontFamily: AppTheme.fontName),
                                    ),
                                    IconButton(
                                        icon: Icon(
                                          Icons.delete,
                                          color: AppTheme.wcsRed,
                                          size: 30,
                                        ),
                                        onPressed: () => {deleteSTD(index)})
                                  ],
                                )),
                              );
                            }))),
                Padding(padding: EdgeInsets.only(bottom: 10)),
                Container(
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      new OutlineButton(
                        shape: StadiumBorder(),
                        textColor: AppTheme.wcsGreen2,
                        child: Text(
                          'บันทึกข้อมูล',
                          style: TextStyle(
                              fontFamily: AppTheme.fontName, fontSize: 20),
                        ),
                        borderSide: BorderSide(
                            color: AppTheme.wcsGreen2,
                            style: BorderStyle.solid,
                            width: 1),
                        onPressed: () {
                          postToDatabase();
                        },
                      )
                    ],
                  ),
                )
              ]),
        )));
  }

  void deleteSTD(int i) {
    setState(() {
      std.removeAt(i);
    });
  }

  void addSTD() {
    setState(() {
      if (!std.contains(stdText.text.trim().toString())) {
        std.add(stdText.text.trim().toString());
      }
    });
  }

  void postToDatabase() async {
    await pr.show();

    final jsons = jsonEncode({
      "farm": widget.idFarm,
      "subFarm": widget.idSubFarm,
      "note": note.text,
      "idStd": std
    });

    Response response = await post("${Config.baseURL}/req/farm",
        headers: <String, String>{
          'Accept': 'application/json; charset=UTF-8',
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsons);


    final jsonResponse = json.decode(response.body.toString());

    print(jsonResponse.toString());
    pr.hide();

    if (jsonResponse["status"] == 200) {
      _showMyDialogCheckBill("สำเร็จ");
    } else {
      _showMyDialogCheckBill("ไม่สำเร็จ");
    }

  }

  Future<void> _showMyDialogCheckBill(String text) async {
    return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return new CupertinoAlertDialog(
            title: new Text(
              "บันทึก",
              style: TextStyle(
                  fontSize: 18,
                  fontFamily: AppTheme.fontName,
                  fontWeight: FontWeight.w500),
            ),
            content: new Text(
              "${text}",
              style: TextStyle(
                  color: AppTheme.detectorGreen,
                  fontSize: 16,
                  fontFamily: AppTheme.fontName,
                  fontWeight: FontWeight.w500),
            ),
            actions: [
              CupertinoDialogAction(
                  isDefaultAction: true,
                  child: new Text("Ok"),
                  onPressed: () {
                    Navigator.pop(context, 'Cancel');
                  })
            ],
          );
        });
  }

  Future<void> _showMyDialogAdd() async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Container(
              height: 200,
              child: Padding(
                padding: const EdgeInsets.all(19.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("ป้อนรหัสนักศึกษา",
                        style: TextStyle(
                            fontSize: 16, fontFamily: AppTheme.fontName)),
                    TextField(
                      controller: stdText,
                      style: TextStyle(fontFamily: AppTheme.fontName),
                      decoration: InputDecoration(
                          border: new OutlineInputBorder(
                              borderSide: new BorderSide(color: Colors.teal)),
                          hintText: 'ระบุ'),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: SizedBox(
                        width: 320.0,
                        child: RaisedButton(
                          onPressed: () {
                            addSTD();
                          },
                          child: Text(
                            "เพิ่ม",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: AppTheme.fontName),
                          ),
                          color: AppTheme.wcsGreen2,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }
}
