class WCSNotification {
  int status;
  List<Message> message;

  WCSNotification({this.status, this.message});

  WCSNotification.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['message'] != null) {
      message = new List<Message>();
      json['message'].forEach((v) {
        message.add(new Message.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.message != null) {
      data['message'] = this.message.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Message {
  int id;
  String title;
  String message;
  String url;
  String image;
  String form;
  String dt;

  Message(
      {this.id,
        this.title,
        this.message,
        this.url,
        this.image,
        this.form,
        this.dt});

  Message.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    message = json['message'];
    url = json['url'];
    image = json['image'];
    form = json['form'];
    dt = json['dt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['message'] = this.message;
    data['url'] = this.url;
    data['image'] = this.image;
    data['form'] = this.form;
    data['dt'] = this.dt;
    return data;
  }
}