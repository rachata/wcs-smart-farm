class MQTTSoil {
  double soil;
  int smart;
  int sensor;
  String type;

  MQTTSoil({this.soil, this.smart, this.sensor, this.type});

  MQTTSoil.fromJson(Map<String, dynamic> json) {
    soil = json['soil'] /1.0;
    smart = json['smart'];
    sensor = json['sensor'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['soil'] = this.soil;
    data['smart'] = this.smart;
    data['sensor'] = this.sensor;
    data['type'] = this.type;
    return data;
  }
}