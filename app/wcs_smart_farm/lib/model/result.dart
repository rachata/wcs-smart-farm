class WCSResult {
  int status;
  List<Message> message;

  WCSResult({this.status, this.message});

  WCSResult.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['message'] != null) {
      message = new List<Message>();
      json['message'].forEach((v) {
        message.add(new Message.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.message != null) {
      data['message'] = this.message.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Message {
  int id;
  int farm;
  int subFarm;
  String temp;
  String hum;
  String soil;
  String note;
  int priceOut;
  int priceIn;
  String img;
  String dt;

  Message(
      {this.id,
        this.farm,
        this.subFarm,
        this.temp,
        this.hum,
        this.soil,
        this.note,
        this.priceOut,
        this.priceIn,
        this.img,
        this.dt});

  Message.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    farm = json['farm'];
    subFarm = json['sub_farm'];
    temp = json['temp'];
    hum = json['hum'];
    soil = json['soil'];
    note = json['note'];
    priceOut = json['price_out'];
    priceIn = json['price_in'];
    img = json['img'];
    dt = json['dt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['farm'] = this.farm;
    data['sub_farm'] = this.subFarm;
    data['temp'] = this.temp;
    data['hum'] = this.hum;
    data['soil'] = this.soil;
    data['note'] = this.note;
    data['price_out'] = this.priceOut;
    data['price_in'] = this.priceIn;
    data['img'] = this.img;
    data['dt'] = this.dt;
    return data;
  }
}