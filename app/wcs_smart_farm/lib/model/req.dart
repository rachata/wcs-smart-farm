class Req {
  int status;
  List<Message> message;

  Req({this.status, this.message});

  Req.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['message'] != null) {
      message = new List<Message>();
      json['message'].forEach((v) {
        message.add(new Message.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.message != null) {
      data['message'] = this.message.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Message {
  int idFarm;
  String name;
  int idSubFarm;
  String idStd;
  String note;
  String dt;

  Message(
      {this.idFarm, this.name, this.idSubFarm, this.idStd, this.note, this.dt});

  Message.fromJson(Map<String, dynamic> json) {
    idFarm = json['id_farm'];
    name = json['name'];
    idSubFarm = json['id_sub_farm'];
    idStd = json['id_std'];
    note = json['note'];
    dt = json['dt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_farm'] = this.idFarm;
    data['name'] = this.name;
    data['id_sub_farm'] = this.idSubFarm;
    data['id_std'] = this.idStd;
    data['note'] = this.note;
    data['dt'] = this.dt;
    return data;
  }
}