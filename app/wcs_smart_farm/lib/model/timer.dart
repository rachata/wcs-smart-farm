class WCSTimer {
  int status;
  List<Message> message;

  WCSTimer({this.status, this.message});

  WCSTimer.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['message'] != null) {
      message = new List<Message>();
      json['message'].forEach((v) {
        message.add(new Message.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.message != null) {
      data['message'] = this.message.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Message {
  int id;
  int farm;
  int subFarm;
  String start;
  String end;
  String control;
  int statusLogi;
  int status;

  Message(
      {this.id,
        this.farm,
        this.subFarm,
        this.start,
        this.end,
        this.control,
        this.statusLogi,
        this.status});

  Message.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    farm = json['farm'];
    subFarm = json['sub_farm'];
    start = json['start'];
    end = json['end'];
    control = json['control'];
    statusLogi = json['status_logi'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['farm'] = this.farm;
    data['sub_farm'] = this.subFarm;
    data['start'] = this.start;
    data['end'] = this.end;
    data['control'] = this.control;
    data['status_logi'] = this.statusLogi;
    data['status'] = this.status;
    return data;
  }
}