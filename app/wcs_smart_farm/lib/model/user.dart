class User {
  int status;
  Message message;

  User({this.status, this.message});

  User.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message =
    json['message'] != null ? new Message.fromJson(json['message']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.message != null) {
      data['message'] = this.message.toJson();
    }
    return data;
  }
}

class Message {
  int type;
  int idUser;
  String name;
  String std;
  String level;
  String phone;
  String image;

  Message(
      {this.type,
        this.idUser,
        this.name,
        this.std,
        this.level,
        this.phone,
        this.image});

  Message.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    idUser = json['idUser'];
    name = json['name'];
    std = json['std'];
    level = json['level'];
    phone = json['phone'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['idUser'] = this.idUser;
    data['name'] = this.name;
    data['std'] = this.std;
    data['level'] = this.level;
    data['phone'] = this.phone;
    data['image'] = this.image;
    return data;
  }
}