class LogSoil {
  int status;
  List<Message> message;

  LogSoil({this.status, this.message});

  LogSoil.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['message'] != null) {
      message = new List<Message>();
      json['message'].forEach((v) {
        message.add(new Message.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.message != null) {
      data['message'] = this.message.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Message {
  double soil;
  String dtf;

  Message({this.soil, this.dtf});

  Message.fromJson(Map<String, dynamic> json) {
    soil = json['soil'] / 1.0;
    dtf = json['dtf'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['soil'] = this.soil;
    data['dtf'] = this.dtf;
    return data;
  }
}