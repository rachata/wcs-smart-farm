class WCSFarm {
  int status;
  List<Message> message;

  WCSFarm({this.status, this.message});

  WCSFarm.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['message'] != null) {
      message = new List<Message>();
      json['message'].forEach((v) {
        message.add(new Message.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.message != null) {
      data['message'] = this.message.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Message {
  int idFarm;
  String fname;
  String image;
  String event;
  int userEvent;
  String dt;
  String uname;

  Message(
      {this.idFarm,
        this.fname,
        this.image,
        this.event,
        this.userEvent,
        this.dt,
        this.uname});

  Message.fromJson(Map<String, dynamic> json) {
    idFarm = json['id_farm'];
    fname = json['fname'];

    if(json['image'] == null ){
      image = "";
    }
    image = json['image'];



    if(json['event'] == null ){
      event = "";
    }
    event = json['event'];



    userEvent = json['user_event'];



    if(json['dt'] == null ){
      dt = "";
    }
    dt = json['dt'];



    if(json['uname'] == null ){
      uname = "";
    }
    uname = json['uname'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_farm'] = this.idFarm;
    data['fname'] = this.fname;
    data['image'] = this.image;
    data['event'] = this.event;
    data['user_event'] = this.userEvent;
    data['dt'] = this.dt;
    data['uname'] = this.uname;
    return data;
  }
}