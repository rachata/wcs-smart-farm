class WCSMyFarm {
  int status;
  List<Message> message;

  WCSMyFarm({this.status, this.message});

  WCSMyFarm.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['message'] != null) {
      message = new List<Message>();
      json['message'].forEach((v) {
        message.add(new Message.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.message != null) {
      data['message'] = this.message.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Message {
  int id;
  int idFarm;
  int idSub;
  String fname;
  String event;
  String name;
  String dt;
  String temp;
  String soil;
  String water;
  String fog;
  String lamp;
  String statusTemp;
  String statusSoil;
  String statusWater;
  String statusFog;
  String statusLamp;
  bool statusT;
  bool statusS;
  bool statusW;
  bool statusF;
  bool statusL;

  Message(
      {this.id,
        this.idFarm,
        this.idSub,
        this.fname,
        this.event,
        this.name,
        this.dt,
        this.temp,
        this.soil,
        this.water,
        this.fog,
        this.lamp,
        this.statusTemp,
        this.statusSoil,
        this.statusWater,
        this.statusFog,
        this.statusLamp,
        this.statusT,
        this.statusS,
        this.statusW,
        this.statusF,
        this.statusL});

  Message.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idFarm = json['id_farm'];
    idSub = json['id_sub'];
    fname = json['fname'];
    event = json['event'];
    name = json['name'];
    dt = json['dt'];
    temp = json['temp'];
    soil = json['soil'];
    water = json['water'];
    fog = json['fog'];
    lamp = json['lamp'];
    statusTemp = json['status_temp'];
    statusSoil = json['status_soil'];
    statusWater = json['status_water'];
    statusFog = json['status_fog'];
    statusLamp = json['status_lamp'];
    statusT = json['status_t'];
    statusS = json['status_s'];
    statusW = json['status_w'];
    statusF = json['status_f'];
    statusL = json['status_l'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_farm'] = this.idFarm;
    data['id_sub'] = this.idSub;
    data['fname'] = this.fname;
    data['event'] = this.event;
    data['name'] = this.name;
    data['dt'] = this.dt;
    data['temp'] = this.temp;
    data['soil'] = this.soil;
    data['water'] = this.water;
    data['fog'] = this.fog;
    data['lamp'] = this.lamp;
    data['status_temp'] = this.statusTemp;
    data['status_soil'] = this.statusSoil;
    data['status_water'] = this.statusWater;
    data['status_fog'] = this.statusFog;
    data['status_lamp'] = this.statusLamp;
    data['status_t'] = this.statusT;
    data['status_s'] = this.statusS;
    data['status_w'] = this.statusW;
    data['status_f'] = this.statusF;
    data['status_l'] = this.statusL;
    return data;
  }
}