class MQTTTemp {
  double temperature = 0.0;
  double humidity = 0.0;
  int smart;
  int sensor;
  String type;

  MQTTTemp(
      {this.temperature, this.humidity, this.smart, this.sensor, this.type});

  MQTTTemp.fromJson(Map<String, dynamic> json) {
    temperature = json['temperature'] / 1.0;;
    humidity = json['humidity'] / 1.0;
    smart = json['smart'];
    sensor = json['sensor'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['temperature'] = this.temperature;
    data['humidity'] = this.humidity;
    data['smart'] = this.smart;
    data['sensor'] = this.sensor;
    data['type'] = this.type;
    return data;
  }
}