class LogTempHum {
  int status;
  List<Message> message;

  LogTempHum({this.status, this.message});

  LogTempHum.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['message'] != null) {
      message = new List<Message>();
      json['message'].forEach((v) {
        message.add(new Message.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.message != null) {
      data['message'] = this.message.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Message {
  double temp;
  double hum;
  String dtf;

  Message({this.temp, this.hum, this.dtf});

  Message.fromJson(Map<String, dynamic> json) {
    temp = json['temp'];
    hum = json['hum'] / 1.0;
    dtf = json['dtf'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['temp'] = this.temp;
    data['hum'] = this.hum;
    data['dtf'] = this.dtf;
    return data;
  }
}