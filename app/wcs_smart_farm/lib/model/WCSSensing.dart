class WCSSensing {
  int status;
  List<Message> message;

  WCSSensing({this.status, this.message});

  WCSSensing.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['message'] != null) {
      message = new List<Message>();
      json['message'].forEach((v) {
        message.add(new Message.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.message != null) {
      data['message'] = this.message.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Message {
  String sensor;
  int id;
  int farm;
  int subFarm;
  double start;
  double end;
  String control;
  int statusLogi;
  int status;

  Message(
      {this.sensor,
        this.id,
        this.farm,
        this.subFarm,
        this.start,
        this.end,
        this.control,
        this.statusLogi,
        this.status});

  Message.fromJson(Map<String, dynamic> json) {
    sensor = json['sensor'];
    id = json['id'];
    farm = json['farm'];
    subFarm = json['sub_farm'];
    start = json['start'] / 1.0;
    end = json['end'] / 1.0;
    control = json['control'];
    statusLogi = json['status_logi'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['sensor'] = this.sensor;
    data['id'] = this.id;
    data['farm'] = this.farm;
    data['sub_farm'] = this.subFarm;
    data['start'] = this.start;
    data['end'] = this.end;
    data['control'] = this.control;
    data['status_logi'] = this.statusLogi;
    data['status'] = this.status;
    return data;
  }
}