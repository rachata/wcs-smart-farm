import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:intl/intl.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wcs_smart_farm/app_theme.dart';
import 'package:wcs_smart_farm/config.dart';
import 'package:wcs_smart_farm/model/notification.dart';
import 'package:wcs_smart_farm/model/timer.dart';

class TimerPage extends StatefulWidget {
  final int farm;
  final int subFarm;
  final String control;

  const TimerPage({Key key , this.farm , this.subFarm , this.control}) : super(key: key);

  @override
  TimerPageState createState() => TimerPageState();
}

class TimerPageState extends State<TimerPage> with TickerProviderStateMixin {
  final ScrollController scrollController = ScrollController();

  Animation<double> topBarAnimation;
  double topBarOpacity = 0.0;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  File image;

  TextEditingController startText = TextEditingController();
  TextEditingController endText = TextEditingController();

  int typeTimer = 0;

  String start = "";
  String end = "";

  ProgressDialog pr;

  bool stateNoti = false;
  List<Widget> timer = [];
  bool stateShow = true;

  cameraConnect() async {
    print('Picker is Called');
    File img = await ImagePicker.pickImage(source: ImageSource.camera);

    if (img != null) {
      image = img;
      setState(() {
        image = img;
      });
    }
  }

  void getTimer() async {
//    await pr.show();

    Response response = await get("${Config.baseURL}/timer/${widget.farm}/${widget.subFarm}/${widget.control}");

    print("response.body.toString() ${response.body.toString()}");

    final jsonResponse = json.decode(response.body.toString());

    WCSTimer timer = WCSTimer.fromJson(jsonResponse);

    print(timer.status);
    if (timer.status == 200) {
      setState(() {
        stateNoti = true;
      });
      createListTimer(timer);

//      pr.hide();
    } else {
      setState(() {
        stateNoti = false;
      });

//      pr.hide();
    }
  }

  void createListTimer(WCSTimer timers) {

    for (int i = 0; i < timers.message.length; i++) {


      setState(() {
        timer.add(
          Container(
              child: Padding(
            padding: EdgeInsets.all(0),
            child: Card(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  side: new BorderSide(color: AppTheme.wcsPrimary, width: 2.0),
                  borderRadius: BorderRadius.circular(12.0),
                ),
                elevation: 5,
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                margin: const EdgeInsets.only(top: 5.0),
                                child: new Text(
                                  timers.message[i].statusLogi == 0
                                      ? "ปิด"
                                      : "เปิด",
                                  style: TextStyle(
                                      fontFamily: AppTheme.fontName,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700),
                                ),
                              ),
                            ],
                          ))
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                margin: const EdgeInsets.only(top: 5.0),
                                child: new Text(
                                  "เริ่ม ${timers.message[i].start}",
                                  style: TextStyle(
                                      fontFamily: AppTheme.fontName,
                                      fontSize: 16),
                                ),
                              ),
                            ],
                          ))
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                margin: const EdgeInsets.only(top: 5.0),
                                child: new Text(
                                  "สิ้นสุด ${timers.message[i].end}",
                                  style: TextStyle(
                                      fontFamily: AppTheme.fontName,
                                      fontSize: 16),
                                ),
                              ),
                            ],
                          ))
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          IconButton(
                              icon: Icon(
                                  timers.message[i].status == 1
                                      ? Icons.close
                                      : Icons.check,
                                  color: Colors.blue),
                              onPressed: () => {
                                    updateStatus(timers.message[i].id,
                                        timers.message[i].status)
                                  }),
                          GestureDetector(
                            child: Text(
                              timers.message[i].status == 1
                                  ? "ปิดการใช้งาน"
                                  : "เปิดการใช้งาน",
                              style: TextStyle(
                                  color: Colors.blue,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: AppTheme.fontName,
                                  fontSize: 16),
                            ),
                            onTap: () => {
                              updateStatus(timers.message[i].id,
                                  timers.message[i].status)
                            },
                          ),
                          IconButton(
                              icon: Icon(Icons.restore_from_trash,
                                  color: Colors.red),
                              onPressed: () => {remove(timers.message[i].id)}),
                          GestureDetector(
                            child: Text(
                              "ลบ",
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: AppTheme.fontName,
                                  fontSize: 16),
                            ),
                            onTap: () => {remove(timers.message[i].id)},
                          )
                        ],
                      ),
                    ],
                  ),
                )),
          )),
        );
      });
      stateShow = true;
    }

    print("stateShow ${stateShow} state ${stateNoti}");
  }

  void updateStatus(int id, int status) async {
    Response response = await get("${Config.baseURL}/timer/${id}/${status}");

    print("response.body.toString() ${response.body.toString()}");

    final jsonResponse = json.decode(response.body.toString());

    setState(() {
      timer = [];
      getTimer();
    });
  }

  void remove(int id) async {
    Response response = await delete("${Config.baseURL}/timer/${id}/");

    print("response.body.toString() ${response.body.toString()}");


    setState(() {
      timer = [];
      getTimer();
    });
  }

  @override
  void initState() {
    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Download,
      isDismissible: false,
    );

    pr.style(
        message: "Please wait...",
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        insetAnimCurve: Curves.easeInOut,
        progressTextStyle: TextStyle(
            color: AppTheme.wcsPrimary,
            fontSize: 13.0,
            fontFamily: AppTheme.fontName,
            fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: AppTheme.wcsPrimary,
            fontSize: 19.0,
            fontFamily: AppTheme.fontName,
            fontWeight: FontWeight.w600));

    AnimationController animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);

    topBarAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: animationController,
            curve: Interval(0, 0.5, curve: Curves.fastOutSlowIn)));

    scrollController.addListener(() {
      if (scrollController.offset >= 24) {
        if (topBarOpacity != 1.0) {
          setState(() {
            topBarOpacity = 1.0;
          });
        }
      } else if (scrollController.offset <= 24 &&
          scrollController.offset >= 0) {
        if (topBarOpacity != scrollController.offset / 24) {
          setState(() {
            topBarOpacity = scrollController.offset / 24;
          });
        }
      } else if (scrollController.offset <= 0) {
        if (topBarOpacity != 0.0) {
          setState(() {
            topBarOpacity = 0.0;
          });
        }
      }
    });

    getTimer();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "ตั้งเวลา",
          style: TextStyle(fontFamily: AppTheme.fontName),
        ),
        backgroundColor: AppTheme.wcsAdminPrimary,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Padding(padding: EdgeInsets.only(bottom: 10)),
                      Container(
                          width: MediaQuery.of(context).size.width - 20,
                          child: new Theme(
                            data: new ThemeData(
                              primaryColor: AppTheme.wcsRed,
                              primaryColorDark: AppTheme.wcsRed,
                            ),
                            child: new TextField(
                              onTap: () => {callAdvancedStart()},
                              controller: startText,
                              style: TextStyle(fontFamily: AppTheme.fontName),
                              decoration: new InputDecoration(
                                border: new OutlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: AppTheme.wcsPrimary)),
                                hintText: 'เริ่ม',
                                labelText: 'เริ่ม',
                                prefixIcon: const Icon(
                                  Icons.timer,
                                  color: AppTheme.wcsPrimary,
                                ),
                              ),
                            ),
                          )),
                      Padding(padding: EdgeInsets.only(bottom: 10)),
                      Container(
                          width: MediaQuery.of(context).size.width - 20,
                          child: new Theme(
                            data: new ThemeData(
                              primaryColor: AppTheme.wcsRed,
                              primaryColorDark: AppTheme.wcsRed,
                            ),
                            child: new TextField(
                              onTap: () => {callAdvancedEnd()},
                              controller: endText,
                              style: TextStyle(fontFamily: AppTheme.fontName),
                              decoration: new InputDecoration(
                                border: new OutlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: AppTheme.wcsPrimary)),
                                hintText: 'สิ้นสุด',
                                labelText: 'สิ้นสุด',
                                prefixIcon: const Icon(
                                  Icons.timelapse,
                                  color: AppTheme.wcsPrimary,
                                ),
                              ),
                            ),
                          )),
                      Padding(padding: EdgeInsets.only(bottom: 10)),
                      Container(
                          width: MediaQuery.of(context).size.width - 20,
                          child: Row(
                            children: [
                              Radio(
                                value: 0,
                                groupValue: typeTimer,
                                onChanged: (value) {
                                  setState(() {
                                    typeTimer = value;
                                  });
                                },
                              ),
                              Text(
                                "ปิด",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: AppTheme.fontName),
                              ),
                              Radio(
                                value: 1,
                                groupValue: typeTimer,
                                onChanged: (value) {
                                  setState(() {
                                    typeTimer = value;
                                  });
                                },
                              ),
                              Text(
                                "เปิด",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: AppTheme.fontName),
                              )
                            ],
                          )),
                      Padding(padding: EdgeInsets.only(bottom: 10)),
                      Container(
                        height: 50,
                        child: new OutlineButton(
                          shape: StadiumBorder(),
                          textColor: AppTheme.dark_grey,
                          child: Text(
                            'บันทึก',
                            style: TextStyle(
                                fontFamily: AppTheme.fontName, fontSize: 20),
                          ),
                          borderSide: BorderSide(
                              color: AppTheme.wcsPrimary,
                              style: BorderStyle.solid,
                              width: 1),
                          onPressed: () {
                            save();
                          },
                        ),
                      ),
                    ],
                  )
                ],
              ),
              Padding(padding: EdgeInsets.only(bottom: 10)),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: stateShow == false ? [] : timer,
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _showMyDialog(String text) async {
    return showDialog<void>(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return new CupertinoAlertDialog(
            title: new Text(
              "ตั้งเวลา",
              style: TextStyle(fontFamily: AppTheme.fontName),
            ),
            content: new Text("${text}"),
            actions: [
              CupertinoDialogAction(
                  isDefaultAction: true,
                  child: new Text("Ok"),
                  onPressed: () {
                    Navigator.pop(context, 'Cancel');
                  })
            ],
          );
        });
  }

  void save() async {
    print("save");

    if (start == "" || end == "") {
      _showMyDialog("โปรกรอกเวลาให้ครบถ้วน");
      return;
    }
    final jsons = jsonEncode({
      "farm": widget.farm,
      "subFarm": widget.subFarm,
      "start": start,
      "end": end,
      "type": widget.control,
      "status": typeTimer
    });

    Response response = await post("${Config.baseURL}/create-timer",
        headers: <String, String>{
          'Accept': 'application/json; charset=UTF-8',
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsons);

    final jsonResponse = json.decode(response.body.toString());

    print(jsonResponse.toString());

    pr.hide();
    if (jsonResponse["status"] == 200) {
      _showMyDialog("สำเร็จ");

      setState(() {
        timer = [];
        getTimer();
      });
    } else {
      _showMyDialog("ไม่สำเร็จ");
    }
  }

  void callAdvancedStart() {
    final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm:ss');

    DatePicker.showDateTimePicker(context,
        showTitleActions: true, onChanged: (date) {
      print('change $date');
    }, onConfirm: (date) {
      start = formatter.format(date);

      DateFormat formatterShow = DateFormat('dd-MM-yyyy HH:mm:ss');

      startText.text = formatterShow.format(date);
    }, currentTime: DateTime.now(), locale: LocaleType.th);
  }

  void callAdvancedEnd() {
    final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm:ss');

    DatePicker.showDateTimePicker(context,
        showTitleActions: true, onChanged: (date) {
      print('change $date');
    }, onConfirm: (date) {
      end = formatter.format(date);
      DateFormat formatterShow = DateFormat('dd-MM-yyyy HH:mm:ss');

      endText.text = formatterShow.format(date);
    }, currentTime: DateTime.now(), locale: LocaleType.th);
  }
}
