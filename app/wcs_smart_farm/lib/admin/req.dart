import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:http/http.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wcs_smart_farm/app_theme.dart';
import 'package:wcs_smart_farm/config.dart';
import 'package:wcs_smart_farm/model/my_farm.dart';
import 'package:wcs_smart_farm/model/req.dart';
import 'package:wcs_smart_farm/user/detail.dart';

class ReqPage extends StatefulWidget {
  const ReqPage({Key key}) : super(key: key);

  @override
  ReqPageState createState() => ReqPageState();
}

class ReqPageState extends State<ReqPage> with TickerProviderStateMixin {
  final ScrollController scrollController = ScrollController();

  Animation<double> topBarAnimation;
  double topBarOpacity = 0.0;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  bool stateFarm = false;
  List<Widget> farm = [];
  bool stateShow = true;

  ProgressDialog pr;

  @override
  void initState() {
    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Download,
      textDirection: TextDirection.ltr,
      isDismissible: false,
    );

    pr.style(
        message: "Please wait...",
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        insetAnimCurve: Curves.easeInOut,
        progressTextStyle: TextStyle(
            fontFamily: AppTheme.fontName,
            color: Colors.black,
            fontSize: 13.0,
            fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            fontFamily: AppTheme.fontName,
            color: Colors.black,
            fontSize: 19.0,
            fontWeight: FontWeight.w600));

    AnimationController animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);

    topBarAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: animationController,
            curve: Interval(0, 0.5, curve: Curves.fastOutSlowIn)));

    scrollController.addListener(() {
      if (scrollController.offset >= 24) {
        if (topBarOpacity != 1.0) {
          setState(() {
            topBarOpacity = 1.0;
          });
        }
      } else if (scrollController.offset <= 24 &&
          scrollController.offset >= 0) {
        if (topBarOpacity != scrollController.offset / 24) {
          setState(() {
            topBarOpacity = scrollController.offset / 24;
          });
        }
      } else if (scrollController.offset <= 0) {
        if (topBarOpacity != 0.0) {
          setState(() {
            topBarOpacity = 0.0;
          });
        }
      }
    });

    getReq();
    super.initState();
  }

  void getReq() async {
    Response response = await get("${Config.baseURL}/get-req/farm");

    final jsonResponse = json.decode(response.body.toString());

    print("jsonResponse ${jsonResponse.toString()}");
    Req farms = Req.fromJson(jsonResponse);

    print("farms ${farms.status}");

    if (farms.status == 200) {
      setState(() {
        stateFarm = true;
      });
      createListFarm(farms);
    } else {
      setState(() {
        stateFarm = false;
      });
    }
  }

  void createListFarm(Req farms) {
    for (int i = 0; i < farms.message.length; i++) {
      if (farms.message[i].note == null || farms.message[i].note == "") {
        setState(() {
          farms.message[i].note = "-";
        });
      }
      setState(() {
        farm.add(InkWell(
            child: Container(
                child: Padding(
          padding: EdgeInsets.all(0),
          child: Card(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                side: new BorderSide(color: AppTheme.wcsRed, width: 2.0),
                borderRadius: BorderRadius.circular(12.0),
              ),
              elevation: 5,
              child: Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Padding(padding: EdgeInsets.only(left: 0, right: 0)),
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: const EdgeInsets.only(top: 5.0),
                              child: new Text(
                                "ชื่อ ${farms.message[i].name}",
                                style: TextStyle(
                                    fontFamily: AppTheme.fontName,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w700),
                              ),
                            ),
                          ],
                        )),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Padding(padding: EdgeInsets.only(left: 0, right: 0)),
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: const EdgeInsets.only(top: 5.0),
                              child: new Text(
                                "โรงเรือนที่ ${farms.message[i].idFarm} แปลงที่ ${farms.message[i].idSubFarm}",
                                style: TextStyle(
                                    fontFamily: AppTheme.fontName,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w700),
                              ),
                            ),
                          ],
                        )),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Padding(padding: EdgeInsets.only(left: 0, right: 0)),
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: const EdgeInsets.only(top: 5.0),
                              child: new Text(
                                "หมายเหตุ ${farms.message[i].note}",
                                style: TextStyle(
                                    fontFamily: AppTheme.fontName,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                          ],
                        )),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Padding(padding: EdgeInsets.only(left: 0, right: 0)),
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Container(
                                margin: const EdgeInsets.only(top: 5.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    IconButton(
                                        icon: Icon(Icons.check,
                                            color: AppTheme.wcsGreen2),
                                        onPressed: () => {
                                              approve(
                                                  farms.message[i].idStd,
                                                  farms.message[i].idFarm,
                                                  farms.message[i].idSubFarm)
                                            }),
                                    GestureDetector(
                                      child: new Text(
                                        "อนุมติ",
                                        style: TextStyle(
                                            fontFamily: AppTheme.fontName,
                                            fontSize: 18,
                                            color: AppTheme.wcsGreen2,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      onTap: () => {
                                        approve(
                                            farms.message[i].idStd,
                                            farms.message[i].idFarm,
                                            farms.message[i].idSubFarm)
                                      },
                                    )
                                  ],
                                )),
                          ],
                        )),
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(bottom: 10)),
                  ],
                ),
              )),
        ))));
      });
    }
  }

  void approve(String idStd, int idfarm, int subFarm) async {
    await pr.show();

    Response response = await post(
      "${Config.baseURL}/get-req/approve/${idStd}/${idfarm}/${subFarm}",
      headers: <String, String>{
        'Accept': 'application/json; charset=UTF-8',
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    final jsonResponse = json.decode(response.body.toString());

    pr.hide();

    if (jsonResponse["status"] == 200) {
      _showMyDialog("สำเร็จ");

      setState(() {
        farm = [];
        getReq();
      });
    } else {
      _showMyDialog("ไม่สำเร็จ");
    }
  }

  Future<void> _showMyDialog(String text) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new CupertinoAlertDialog(
            title: new Text(
              "อนุมัติ",
              style: TextStyle(
                  fontSize: 18,
                  fontFamily: AppTheme.fontName,
                  fontWeight: FontWeight.w500),
            ),
            content: new Text(
              "${text}",
              style: TextStyle(
                  color: AppTheme.detectorGreen,
                  fontSize: 16,
                  fontFamily: AppTheme.fontName,
                  fontWeight: FontWeight.w500),
            ),
            actions: [
              CupertinoDialogAction(
                  isDefaultAction: true,
                  child: new Text("Ok"),
                  onPressed: () {
                    Navigator.pop(context, 'Cancel');

                  })
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "คำขอใช้งาน",
          style: TextStyle(fontFamily: AppTheme.fontName),
        ),
        backgroundColor: AppTheme.wcsAdminPrimary,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
          child: stateFarm == true
              ? Column(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: stateShow == false ? [] : farm,
                    ),
                  ],
                )
              : Center(
                  child: Column(
                  children: [
                    Text(
                      "ไม่มีข้อมูลในขณะนี้",
                      style: TextStyle(
                          color: AppTheme.wcsRed,
                          fontFamily: AppTheme.fontName,
                          fontSize: 18,
                          fontWeight: FontWeight.w800),
                    ),
                    Loading(
                        indicator: BallPulseIndicator(),
                        size: 100.0,
                        color: AppTheme.wcsRed),
                  ],
                ))),
    );
  }
}
