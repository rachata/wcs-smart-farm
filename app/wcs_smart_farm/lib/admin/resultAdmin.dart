import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:wcs_smart_farm/app_theme.dart';
import 'package:wcs_smart_farm/config.dart';
import 'package:wcs_smart_farm/model/farm.dart';
import 'package:wcs_smart_farm/model/result.dart';
import 'package:wcs_smart_farm/user/sub_farm.dart';

class ResultAdminPage extends StatefulWidget {

  final int farm;
  final int subFarm;
  const ResultAdminPage({Key key , this.farm , this.subFarm}) : super(key: key);

  @override
  ResultAdminPageState createState() => ResultAdminPageState();
}

class ResultAdminPageState extends State<ResultAdminPage> with TickerProviderStateMixin {
  final ScrollController scrollController = ScrollController();

  Animation<double> topBarAnimation;
  double topBarOpacity = 0.0;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  bool stateFarm = false;
  List<Widget> farm = [];
  bool stateShow = true;

  @override
  void initState() {
    AnimationController animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);

    topBarAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: animationController,
            curve: Interval(0, 0.5, curve: Curves.fastOutSlowIn)));

    scrollController.addListener(() {
      if (scrollController.offset >= 24) {
        if (topBarOpacity != 1.0) {
          setState(() {
            topBarOpacity = 1.0;
          });
        }
      } else if (scrollController.offset <= 24 &&
          scrollController.offset >= 0) {
        if (topBarOpacity != scrollController.offset / 24) {
          setState(() {
            topBarOpacity = scrollController.offset / 24;
          });
        }
      } else if (scrollController.offset <= 0) {
        if (topBarOpacity != 0.0) {
          setState(() {
            topBarOpacity = 0.0;
          });
        }
      }
    });

    this.getFarm();
    super.initState();
  }

  void getFarm() async {
    Response response = await get("${Config.baseURL}/result/${widget.farm}/${widget.subFarm}");

    final jsonResponse = json.decode(response.body.toString());

    print(jsonResponse.toString());
    WCSResult farms = WCSResult.fromJson(jsonResponse);

    print("farms ${farms.status}");
    if (farms.status == 200) {
      setState(() {
        stateFarm = true;
      });
      createListFarm(farms);
    } else {
      setState(() {
        stateFarm = false;
      });
    }
  }

  void createListFarm(WCSResult farms) {
    for (int i = 0; i < farms.message.length; i++) {


      farm.add(
        InkWell(

            child: Container(
                child: Padding(
                  padding: EdgeInsets.all(0),
                  child: Card(
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        side:
                        new BorderSide(color: AppTheme.wcsPrimary, width: 2.0),
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                      elevation: 5,
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Container(
                                  child: FadeInImage.assetNetwork(
                                      placeholder: "assets/atom.gif",
                                      image: farms.message[i].img,
                                      height: 60.0,
                                      fit: BoxFit.cover),
                                ),
                                Padding(
                                    padding: EdgeInsets.only(left: 10, right: 10)),
                                Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          margin: const EdgeInsets.only(top: 5.0),
                                          child: new Text(
                                            "${farms.message[i].dt}",
                                            style: TextStyle(
                                                fontFamily: AppTheme.fontName,
                                                fontSize: 18,
                                                fontWeight: FontWeight.w700),
                                          ),
                                        ),
                                      ],
                                    ))
                              ],
                            ),
                            Padding(padding: EdgeInsets.only(bottom: 0)),

                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          margin: const EdgeInsets.only(top: 0),
                                          child: new Text(
                                            "อุณหภูมิ : ${farms.message[i].temp}",
                                            style: TextStyle(
                                                fontFamily: AppTheme.fontName,
                                                fontSize: 16,
                                                color: AppTheme.wcsGreen2,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ),
                                      ],
                                    ))
                              ],
                            ),

                            Padding(padding: EdgeInsets.only(bottom: 0)),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          margin: const EdgeInsets.only(top: 0),
                                          child: new Text(
                                            "ความชื้นในอากาศ : ${farms.message[i].hum}",
                                            style: TextStyle(
                                                fontFamily: AppTheme.fontName,
                                                fontSize: 16,
                                                color: AppTheme.wcsGreen2,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ),
                                      ],
                                    ))
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          margin: const EdgeInsets.only(top: 0),
                                          child: new Text(
                                            "ความชื้นในดิน : ${farms.message[i].soil}",
                                            style: TextStyle(
                                                fontFamily: AppTheme.fontName,
                                                fontSize: 16,
                                                color: AppTheme.wcsGreen2,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ),
                                      ],
                                    ))
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          margin: const EdgeInsets.only(top: 0),
                                          child: new Text(
                                            "รายละเอียก : ${farms.message[i].note}",
                                            style: TextStyle(
                                                fontFamily: AppTheme.fontName,
                                                fontSize: 16,
                                                color: AppTheme.wcsGreen2,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ),
                                      ],
                                    ))
                              ],
                            ),

                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          margin: const EdgeInsets.only(top: 0),
                                          child: new Text(
                                            "รายจ่าย : ${farms.message[i].priceOut}",
                                            style: TextStyle(
                                                fontFamily: AppTheme.fontName,
                                                fontSize: 16,
                                                color: AppTheme.wcsGreen2,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ),
                                      ],
                                    ))
                              ],
                            ),

                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          margin: const EdgeInsets.only(top: 0),
                                          child: new Text(
                                            "รายรับ : ${farms.message[i].priceIn}",
                                            style: TextStyle(
                                                fontFamily: AppTheme.fontName,
                                                fontSize: 16,
                                                color: AppTheme.wcsGreen2,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ),
                                      ],
                                    ))
                              ],
                            ),


                          ],
                        ),
                      )),
                ))),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "โรงเรือน",
            style: TextStyle(fontFamily: AppTheme.fontName),
          ),
          backgroundColor: AppTheme.wcsAdminPrimary,
        ),
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
            child: stateFarm == true
                ? Column(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: stateShow == false ? [] : farm,
                ),
              ],
            )
                : Center(
                child: Column(
                  children: [
                    Text(
                      "ไม่มีข้อมูลในขณะนี้",
                      style: TextStyle(
                          color: AppTheme.wcsRed,
                          fontFamily: AppTheme.fontName,
                          fontSize: 18,
                          fontWeight: FontWeight.w800),
                    ),
                    Loading(
                        indicator: BallPulseIndicator(),
                        size: 100.0,
                        color: AppTheme.wcsRed),
                  ],
                ))));
  }
}
