import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';
import 'package:wcs_smart_farm/MQTTUtility.dart';
import 'package:wcs_smart_farm/admin/DetailAdmin.dart';
import 'package:mqtt_client/mqtt_client.dart' as mqtt;
import 'package:wcs_smart_farm/admin/GraphAdmin.dart';
import 'package:wcs_smart_farm/admin/sensing.dart';
import 'package:wcs_smart_farm/app_theme.dart';
import 'package:wcs_smart_farm/config.dart';
import 'package:wcs_smart_farm/model/MQTTSoil.dart';
import 'package:wcs_smart_farm/model/MQTTTemp.dart';
import 'package:wcs_smart_farm/model/user.dart';
import 'package:wcs_smart_farm/timer.dart';
import 'package:intl/intl.dart';

class DetailFirst1 extends StatefulWidget {

  final int farm;
  final int subFarm;
  final String title;

  final bool statusF;
  final bool statusW;

  final bool statusL;

  const DetailFirst1({Key key, this.farm, this.subFarm, this.title, this.statusF, this.statusW, this.statusL}) : super(key: key);



  @override
  _DetailFirst1State createState() => _DetailFirst1State();
}

class _DetailFirst1State extends State<DetailFirst1> with TickerProviderStateMixin  {
  final ScrollController scrollController = ScrollController();

  Animation<double> topBarAnimation;
  double topBarOpacity = 0.0;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  List<GraphData> temp = [];
  List<GraphData> hum = [];
  List<GraphData> soil = [];

  Future<mqtt.MqttClient> client;
  StreamSubscription subscription;
  MQTTUtility _mqttUtility;

  String datetime = "ยังไม่มีข้อมูล";

  double tempShow = 0.0;
  double humShow = 0.0;
  double soilShow = 0.0;

  ProgressDialog pr;

  bool select = true;

  String datetimeLamp = "";
  String datetimeWater = "";
  String datetimeFog = "";

  bool statusControlMobile = false;
  bool statusControlTimer = false;


  bool statusW = false;
  bool statusL = false;
  bool statusF = false;






  void getStatus() async {
    Response response = await get(
        "${Config.baseURL}/select-control/${widget.farm}/${widget.subFarm}",
        headers: <String, String>{
          'Accept': 'application/json; charset=UTF-8',
          'Content-Type': 'application/json; charset=UTF-8',
        });

    final jsonResponse = json.decode(response.body.toString());

    if (jsonResponse["message"] == 1) {
      setState(() {
        statusControlMobile = true;
        statusControlTimer = false;
      });
    } else {
      setState(() {
        statusControlMobile = false;
        statusControlTimer = true;
      });
    }
  }

  Future<void> _showMyDialogDelete() async {
    return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return new CupertinoAlertDialog(
            title: new Text(
              "ลบโรงเรือน",
              style: TextStyle(
                  fontSize: 18,
                  fontFamily: AppTheme.fontName,
                  fontWeight: FontWeight.w500),
            ),
            content: new Text(
              "คุณต้องการลบแปลงที่ ${widget.subFarm} โรงเรือนที่ ${widget.farm} ออกจากบัญชีของคุณ",
              style: TextStyle(
                  color: AppTheme.wcsRed,
                  fontSize: 16,
                  fontFamily: AppTheme.fontName,
                  fontWeight: FontWeight.w500),
            ),
            actions: [
              CupertinoDialogAction(
                  isDefaultAction: true,
                  child: new Text("ลบ",
                      style: TextStyle(
                        color: AppTheme.wcsRed,
                        fontFamily: AppTheme.fontName,
                      )),
                  onPressed: () {
                    deleteFarm();
                    Navigator.pop(context, 'Cancel');
                  }),
              CupertinoDialogAction(
                  isDefaultAction: true,
                  child: new Text("ยกเลิก",
                      style: TextStyle(
                        fontFamily: AppTheme.fontName,
                      )),
                  onPressed: () {
                    Navigator.pop(context, 'Cancel');
                  })
            ],
          );
        });
  }

  Future<void> _showMyDialog(String text) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new CupertinoAlertDialog(
            title: new Text(
              "ลบ",
              style: TextStyle(
                  fontSize: 18,
                  fontFamily: AppTheme.fontName,
                  fontWeight: FontWeight.w500),
            ),
            content: new Text(
              "${text}",
              style: TextStyle(
                  color: AppTheme.detectorGreen,
                  fontSize: 16,
                  fontFamily: AppTheme.fontName,
                  fontWeight: FontWeight.w500),
            ),
            actions: [
              CupertinoDialogAction(
                  isDefaultAction: true,
                  child: new Text("Ok"),
                  onPressed: () {
                    Navigator.pop(context, 'Cancel');
                    Navigator.pop(context, 'Cancel');
                  })
            ],
          );
        });
  }

  void deleteFarm() async {
    print("${Config.baseURL}/my-farm/1233/${widget.subFarm}/${widget.farm}");

    await pr.show();

    Response response = await delete(
      "${Config.baseURL}/my-farm/1233/${widget.subFarm}/${widget.farm}",
      headers: <String, String>{
        'Accept': 'application/json; charset=UTF-8',
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    final jsonResponse = json.decode(response.body.toString());

    print(jsonResponse.toString());
    pr.hide();

    if (jsonResponse["status"] == 200) {
      _showMyDialog("สำเร็จ");
    } else {
      _showMyDialog("ไม่สำเร็จ");
    }
  }

  @override
  void initState() {
    getStatus();
    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Download,
      isDismissible: false,
    );

    pr.style(
        message: "Please wait...",
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        insetAnimCurve: Curves.easeInOut,
        progressTextStyle: TextStyle(
            color: AppTheme.wcsPrimary,
            fontSize: 13.0,
            fontFamily: AppTheme.fontName,
            fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: AppTheme.wcsPrimary,
            fontSize: 19.0,
            fontFamily: AppTheme.fontName,
            fontWeight: FontWeight.w600));

    AnimationController animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);

    topBarAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: animationController,
            curve: Interval(0, 0.5, curve: Curves.fastOutSlowIn)));

    scrollController.addListener(() {
      if (scrollController.offset >= 24) {
        if (topBarOpacity != 1.0) {
          setState(() {
            topBarOpacity = 1.0;
          });
        }
      } else if (scrollController.offset <= 24 &&
          scrollController.offset >= 0) {
        if (topBarOpacity != scrollController.offset / 24) {
          setState(() {
            topBarOpacity = scrollController.offset / 24;
          });
        }
      } else if (scrollController.offset <= 0) {
        if (topBarOpacity != 0.0) {
          setState(() {
            topBarOpacity = 0.0;
          });
        }
      }
    });

    connectMQTT();
    super.initState();
  }

  void getMessage(String message, String topic) {
    final DateTime now = DateTime.now();

    final DateFormat formatter = DateFormat('dd/MM/yyyy HH:mm:ss');

    setState(() {
      datetime = formatter.format(now);
    });

    String dtGraph = DateFormat('HH:mm:ss').format(now);

    final jsonResponse = json.decode(message.toString());


    if(topic == "wcs/smart/${widget.farm}"){

      print(jsonResponse.toString());

      statusW = jsonResponse["W${widget.subFarm}"];
      statusF = jsonResponse["F"];
      statusL = jsonResponse["L"];
    }
    setState(() {
      if (topic == "temp/${widget.farm}/${widget.subFarm}") {
        MQTTTemp tempHum = MQTTTemp.fromJson(jsonResponse);
        temp.add(GraphData(dtGraph, tempShow));
        hum.add(GraphData(dtGraph, humShow));

        soil.add(GraphData(dtGraph, soilShow / 1.0));

        tempShow = tempHum.temperature;
        humShow = tempHum.humidity;
      }

      if (topic == "soil/${widget.farm}/${widget.subFarm}") {
        MQTTSoil soilMqtt = MQTTSoil.fromJson(jsonResponse);

        soilShow = soilMqtt.soil / 1.0;

        soil.add(GraphData(dtGraph, soilShow));
        temp.add(GraphData(dtGraph, tempShow));
        hum.add(GraphData(dtGraph, humShow));
      }
    });
  }

  void connectMQTT() {
    _mqttUtility = new MQTTUtility(
            (onMessageNew, topic) => getMessage(onMessageNew, topic));
    Future<bool> status = _mqttUtility.connect();

    print(status);
    status.then((value) => {
      if (value)
        {

          _mqttUtility
              .subscribeToTopic("wcs/smart/${widget.farm}"),
          _mqttUtility
              .subscribeToTopic("temp/${widget.farm}/${widget.subFarm}"),
          _mqttUtility
              .subscribeToTopic("soil/${widget.farm}/${widget.subFarm}"),
        }
      else
        {print(value)}
    });
  }

  makeGraphRealTime() {
    return SfCartesianChart(
        zoomPanBehavior:
        ZoomPanBehavior(enablePanning: true, enablePinching: true),
        plotAreaBackgroundColor: Colors.white70,
        primaryYAxis: NumericAxis(
            labelFormat: '{value}',
            title: AxisTitle(
                text: '',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: AppTheme.fontName,
                    fontSize: 16,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        primaryXAxis: CategoryAxis(
            labelRotation: 60,
            title: AxisTitle(
                text: '',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: AppTheme.fontName,
                    fontSize: 20,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        // Chart title
        title: ChartTitle(
            text: 'กราฟ RealTime',
            textStyle: ChartTextStyle(
                fontFamily: AppTheme.fontName,
                color: AppTheme.wcsPrimary,
                fontSize: 14,
                fontWeight: FontWeight.w600)),
        // Enable legend
        legend: Legend(isVisible: true, position: LegendPosition.bottom),

        // Enable tooltip
        tooltipBehavior: TooltipBehavior(enable: true),
        series: <ChartSeries<GraphData, String>>[
          SplineSeries<GraphData, String>(
            markerSettings: MarkerSettings(
                isVisible: true,
                // Marker shape is set to diamond
                shape: DataMarkerType.circle),
            isVisible: true,
            animationDuration: 0.5,
            width: 2,
            name: "อุณหภูมิ",
            dataSource: temp,
            xValueMapper: (GraphData graph, _) => graph.x.toString(),
            yValueMapper: (GraphData graph, _) => graph.y,
            color: AppTheme.wcsBlue,
          ),
          SplineSeries<GraphData, String>(
            markerSettings: MarkerSettings(
                isVisible: true,
                // Marker shape is set to diamond
                shape: DataMarkerType.circle),
            isVisible: true,
            animationDuration: 0.5,
            width: 2,
            name: "ความชื้นในอากาศ",
            dataSource: hum,
            xValueMapper: (GraphData graph, _) => graph.x.toString(),
            yValueMapper: (GraphData graph, _) => graph.y,
            color: AppTheme.wcsGreen,
          ),
          SplineSeries<GraphData, String>(
            markerSettings: MarkerSettings(
                isVisible: true,
                // Marker shape is set to diamond
                shape: DataMarkerType.circle),
            isVisible: true,
            animationDuration: 0.5,
            width: 2,
            name: "ความชื้นในดิน",
            dataSource: soil,
            xValueMapper: (GraphData graph, _) => graph.x.toString(),
            yValueMapper: (GraphData graph, _) => graph.y,
            color: AppTheme.wcsOrange,
          ),
        ]);
  }

  void updateStatus(int status) async {
    Response response = await get(
        "${Config.baseURL}/select-control/${widget.farm}/${widget.subFarm}/${status}",
        headers: <String, String>{
          'Accept': 'application/json; charset=UTF-8',
          'Content-Type': 'application/json; charset=UTF-8',
        });

    getStatus();
  }

  @override
  Widget build(BuildContext context) {
    Widget _threeItemPopup() => PopupMenuButton(
      onSelected: (value) {
        if (value == 2) {
          setState(() {
            if (statusControlMobile != true) {
              updateStatus(0);
            }
            statusControlTimer = false;
            statusControlMobile = true;
          });
        } else if (value == 3) {
          setState(() {
            if (statusControlTimer != true) {
              updateStatus(1);
            }

            statusControlTimer = true;
            statusControlMobile = false;
          });
        } else if (value == 4) {
          _showMyDialogDelete();
        }
      },
      itemBuilder: (context) {
        var list = List<PopupMenuEntry<Object>>();
        list.add(
          PopupMenuItem(
            child: Text(
              "ตั้งค่าการควบคุม",
              style: TextStyle(fontFamily: AppTheme.fontName),
            ),
            value: 1,
          ),
        );
        list.add(
          PopupMenuDivider(
            height: 10,
          ),
        );
        list.add(
          CheckedPopupMenuItem(
            child: Text("ผ่านการกดจากมือถือ",
                style: TextStyle(fontFamily: AppTheme.fontName)),
            value: 2,
            checked: statusControlMobile,
          ),
        );
        list.add(
          CheckedPopupMenuItem(
            child: Text("ผ่านการตั้งเวลา / ความสัมพันธ์",
                style: TextStyle(fontFamily: AppTheme.fontName)),
            value: 3,
            checked: statusControlTimer,
          ),
        );

        return list;
      },
      icon: Icon(
        Icons.settings,
        color: Colors.white,
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "${widget.title}",
          style: TextStyle(fontFamily: AppTheme.fontName),
        ),
        actions: [
          _threeItemPopup(),
          IconButton(
              icon: Icon(
                Icons.equalizer,
                color: Colors.white,
              ),
              onPressed: () => {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (_) => GraphAdminPage(
                      farm: widget.farm,
                      subFarm: widget.subFarm,
                    )))
              }),
        ],
        backgroundColor: AppTheme.wcsAdminPrimary,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text("ข้อมูล ณ ${datetime}",
                        style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontFamily: AppTheme.fontName,
                            fontSize: 18,
                            color: AppTheme.wcsPrimary)),
                  ],
                ),
                Container(
                    margin: EdgeInsets.all(0),
                    child: Card(
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                          side: new BorderSide(
                              color: AppTheme.wcsPrimary, width: 2.0),
                          borderRadius: BorderRadius.circular(12.0),
                        ),
                        elevation: 5,
                        child: Container(child: makeGraphRealTime()))),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                        width: (MediaQuery.of(context).size.width * 0.5) - 10,
                        height: (MediaQuery.of(context).size.width * 0.7),
                        margin: EdgeInsets.all(0),
                        child: Card(
                          color: AppTheme.wcsPrimary,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          elevation: 5,
                          child: Container(
                              child: SfRadialGauge(
                                  title: GaugeTitle(
                                      text: 'อุณหภูมิ : ${tempShow}°C',
                                      textStyle: TextStyle(
                                          fontSize: 16,
                                          color: AppTheme.white,
                                          fontStyle: FontStyle.normal,
                                          fontFamily: 'Prompt')),
                                  axes: <RadialAxis>[
                                    RadialAxis(
                                        labelsPosition: ElementsPosition.inside,
                                        axisLabelStyle: GaugeTextStyle(
                                            fontSize: 13,
                                            color: AppTheme.white,
                                            fontStyle: FontStyle.normal,
                                            fontFamily: 'Prompt'),
                                        minimum: -50,
                                        maximum: 150,
                                        ranges: <GaugeRange>[
                                          GaugeRange(
                                              startWidth: 9,
                                              endWidth: 9,
                                              startValue: -50,
                                              endValue: 0,
                                              color: AppTheme.wcsBlue),
                                          GaugeRange(
                                              startWidth: 9,
                                              endWidth: 9,
                                              startValue: 0,
                                              endValue: 25,
                                              color: AppTheme.wcsGreen),
                                          GaugeRange(
                                              startWidth: 9,
                                              endWidth: 9,
                                              startValue: 25,
                                              endValue: 75,
                                              color: AppTheme.wcsOrange),
                                          GaugeRange(
                                              startWidth: 9,
                                              endWidth: 9,
                                              startValue: 75,
                                              endValue: 150,
                                              color: AppTheme.wcsRed),
                                        ],
                                        pointers: <GaugePointer>[
                                          NeedlePointer(
                                              value: tempShow / 1.0,
                                              lengthUnit: GaugeSizeUnit.factor,
                                              needleLength: 0.6,
                                              needleEndWidth: 4,
                                              needleColor: AppTheme.white,
                                              knobStyle: KnobStyle(
                                                  knobRadius: 0.08,
                                                  sizeUnit: GaugeSizeUnit.factor,
                                                  color: AppTheme.grey))
                                        ],
                                        annotations: <GaugeAnnotation>[
                                          GaugeAnnotation(
                                              angle: 90, positionFactor: 0.5)
                                        ])
                                  ])),
                        )),
                    Container(
                        width: (MediaQuery.of(context).size.width * 0.5) - 10,
                        height: (MediaQuery.of(context).size.width * 0.7),
                        child: Card(
                          color: AppTheme.wcsPrimary,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          elevation: 5,
                          child: Container(
                              child: SfRadialGauge(
                                  title: GaugeTitle(
                                      text: 'ความชื้นในอากาศ : ${humShow}%',
                                      textStyle: TextStyle(
                                          fontSize: 16,
                                          color: AppTheme.white,
                                          fontStyle: FontStyle.normal,
                                          fontFamily: 'Prompt')),
                                  axes: <RadialAxis>[
                                    RadialAxis(
                                        labelsPosition: ElementsPosition.inside,
                                        axisLabelStyle: GaugeTextStyle(
                                            fontSize: 13,
                                            color: AppTheme.white,
                                            fontStyle: FontStyle.normal,
                                            fontFamily: 'Prompt'),
                                        minimum: 0,
                                        maximum: 100,
                                        ranges: <GaugeRange>[
                                          GaugeRange(
                                              startWidth: 9,
                                              endWidth: 9,
                                              startValue: 0,
                                              endValue: 30,
                                              color: AppTheme.wcsRed),
                                          GaugeRange(
                                              startWidth: 9,
                                              endWidth: 9,
                                              startValue: 30,
                                              endValue: 50,
                                              color: AppTheme.wcsGreen),
                                          GaugeRange(
                                              startWidth: 9,
                                              endWidth: 9,
                                              startValue: 50,
                                              endValue: 100,
                                              color: AppTheme.wcsBlue),
                                        ],
                                        pointers: <GaugePointer>[
                                          NeedlePointer(
                                              value: humShow / 1.0,
                                              lengthUnit: GaugeSizeUnit.factor,
                                              needleLength: 0.6,
                                              needleEndWidth: 4,
                                              needleColor: AppTheme.white,
                                              knobStyle: KnobStyle(
                                                  knobRadius: 0.08,
                                                  sizeUnit: GaugeSizeUnit.factor,
                                                  color: AppTheme.grey))
                                        ],
                                        annotations: <GaugeAnnotation>[
                                          GaugeAnnotation(
                                              angle: 90, positionFactor: 0.5)
                                        ])
                                  ])),
                        )),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                        width: (MediaQuery.of(context).size.width * 0.5) - 10,
                        height: (MediaQuery.of(context).size.width * 0.7),
                        margin: EdgeInsets.all(0),
                        child: Card(
                          color: AppTheme.wcsPrimary,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          elevation: 5,
                          child: Container(
                              child: SfRadialGauge(
                                  title: GaugeTitle(
                                      text: 'ความชื้นในดิน : ${soilShow}',
                                      textStyle: TextStyle(
                                          fontSize: 16,
                                          color: AppTheme.white,
                                          fontStyle: FontStyle.normal,
                                          fontFamily: 'Prompt')),
                                  axes: <RadialAxis>[
                                    RadialAxis(
                                        labelsPosition: ElementsPosition.inside,
                                        axisLabelStyle: GaugeTextStyle(
                                            fontSize: 13,
                                            color: AppTheme.white,
                                            fontStyle: FontStyle.normal,
                                            fontFamily: 'Prompt'),
                                        minimum: 0,
                                        maximum: 1023,
                                        ranges: <GaugeRange>[
                                          GaugeRange(
                                              startWidth: 9,
                                              endWidth: 9,
                                              startValue: 0,
                                              endValue: 300,
                                              color: AppTheme.wcsRed),
                                          GaugeRange(
                                              startWidth: 9,
                                              endWidth: 9,
                                              startValue: 300,
                                              endValue: 600,
                                              color: AppTheme.wcsOrange),
                                          GaugeRange(
                                              startWidth: 9,
                                              endWidth: 9,
                                              startValue: 600,
                                              endValue: 900,
                                              color: AppTheme.wcsGreen),
                                          GaugeRange(
                                              startWidth: 9,
                                              endWidth: 9,
                                              startValue: 900,
                                              endValue: 1023,
                                              color: AppTheme.wcsBlue),
                                        ],
                                        pointers: <GaugePointer>[
                                          NeedlePointer(
                                              value: soilShow / 1.0,
                                              lengthUnit: GaugeSizeUnit.factor,
                                              needleLength: 0.6,
                                              needleEndWidth: 4,
                                              needleColor: AppTheme.white,
                                              knobStyle: KnobStyle(
                                                  knobRadius: 0.08,
                                                  sizeUnit: GaugeSizeUnit.factor,
                                                  color: AppTheme.grey))
                                        ],
                                        annotations: <GaugeAnnotation>[
                                          GaugeAnnotation(
                                              angle: 90, positionFactor: 0.5)
                                        ])
                                  ])),
                        )),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        child: Padding(
                          padding: EdgeInsets.all(0),
                          child: Card(
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                side: new BorderSide(
                                    color: AppTheme.wcsPrimary, width: 2.0),
                                borderRadius: BorderRadius.circular(12.0),
                              ),
                              elevation: 5,
                              child: Padding(
                                padding: EdgeInsets.all(10),
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Text(
                                          'Sprinkler หมอก',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w800,
                                            fontSize: 16,
                                            color: AppTheme.grey,
                                          ),
                                        ),
                                        IconButton(
                                          iconSize: 50,
                                          icon:
                                          new Image.asset('assets/sprinkler1.png'),
                                        )
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        IconButton(
                                          onPressed: () => {
//                                  actionS2()

                                            sendFog()
                                          },
                                          iconSize: 50,
                                          icon: statusF == false ?  Image.asset("assets/off.png") : Image.asset("assets/power.png") ,
                                        )
                                      ],
                                    ),

                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        IconButton(
                                          onPressed: () => {
                                            Navigator.of(context)
                                                .push(MaterialPageRoute(
                                                builder: (_) => TimerPage(
                                                  farm: widget.farm,
                                                  subFarm: widget.subFarm,
                                                  control: "F",
                                                )))
                                          },
                                          iconSize: 25,
                                          icon: Icon(
                                            Icons.timer,
                                            color: Colors.blue,
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap: () => {
                                            Navigator.of(context)
                                                .push(MaterialPageRoute(
                                                builder: (_) => TimerPage(
                                                  farm: widget.farm,
                                                  subFarm: widget.subFarm,
                                                  control: "F",
                                                )))
                                          },
                                          child: Text(
                                            "ตั้งเวลา",
                                            style: TextStyle(
                                                fontFamily: AppTheme.fontName,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ),

                                        IconButton(
                                          onPressed: () => {
                                            Navigator.of(context)
                                                .push(MaterialPageRoute(
                                                builder: (_) => SensingPage(
                                                  farm: widget.farm,
                                                  subFarm: widget.subFarm,
                                                  control: "F",
                                                )))
                                          },
                                          iconSize: 25,
                                          icon: Icon(
                                            Icons.sensor_window,
                                            color: Colors.blue,
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap: () => {
                                            Navigator.of(context)
                                                .push(MaterialPageRoute(
                                                builder: (_) => SensingPage(
                                                  farm: widget.farm,
                                                  subFarm: widget.subFarm,
                                                  control: "F",
                                                )))
                                          },
                                          child: Text(
                                            "ตั้งค่าความสัมพันธ์",
                                            style: TextStyle(
                                                fontFamily: AppTheme.fontName,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        )

                                      ],
                                    ),

                                  ],
                                ),
                              )),
                        )),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        child: Padding(
                          padding: EdgeInsets.all(0),
                          child: Card(
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                side: new BorderSide(
                                    color: AppTheme.wcsPrimary, width: 2.0),
                                borderRadius: BorderRadius.circular(12.0),
                              ),
                              elevation: 5,
                              child: Padding(
                                padding: EdgeInsets.all(10),
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Text(
                                          'Sprinkler น้ำ',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w800,
                                            fontSize: 16,
                                            color: AppTheme.grey,
                                          ),
                                        ),
                                        IconButton(
                                          iconSize: 50,
                                          icon:
                                          new Image.asset('assets/sprinkler2.png'),
                                        )
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        IconButton(
                                          onPressed: () => {

                                            sendWater()
//                                  actionS2()
                                          },
                                          iconSize: 50,
                                          icon: statusW == false ?  Image.asset("assets/off.png") : Image.asset("assets/power.png") ,
                                        )
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        IconButton(
                                          onPressed: () => {
                                            Navigator.of(context)
                                                .push(MaterialPageRoute(
                                                builder: (_) => TimerPage(
                                                  farm: widget.farm,
                                                  subFarm: widget.subFarm,
                                                  control: "W",
                                                )))
                                          },
                                          iconSize: 25,
                                          icon: Icon(
                                            Icons.timer,
                                            color: Colors.blue,
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap: () => {
                                            Navigator.of(context)
                                                .push(MaterialPageRoute(
                                                builder: (_) => TimerPage(
                                                  farm: widget.farm,
                                                  subFarm: widget.subFarm,
                                                  control: "W",
                                                )))
                                          },
                                          child: Text(
                                            "ตั้งเวลา",
                                            style: TextStyle(
                                                fontFamily: AppTheme.fontName,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ),

                                        IconButton(
                                          onPressed: () => {
                                            Navigator.of(context)
                                                .push(MaterialPageRoute(
                                                builder: (_) => SensingPage(
                                                  farm: widget.farm,
                                                  subFarm: widget.subFarm,
                                                  control: "W",
                                                )))
                                          },
                                          iconSize: 25,
                                          icon: Icon(
                                            Icons.sensor_window,
                                            color: Colors.blue,
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap: () => {
                                            Navigator.of(context)
                                                .push(MaterialPageRoute(
                                                builder: (_) => SensingPage(
                                                  farm: widget.farm,
                                                  subFarm: widget.subFarm,
                                                  control: "W",
                                                )))
                                          },
                                          child: Text(
                                            "ตั้งความสัมพันธ์",
                                            style: TextStyle(
                                                fontFamily: AppTheme.fontName,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        )

                                      ],
                                    ),
                                  ],
                                ),
                              )),
                        )),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        child: Padding(
                          padding: EdgeInsets.all(0),
                          child: Card(
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                side: new BorderSide(
                                    color: AppTheme.wcsPrimary, width: 2.0),
                                borderRadius: BorderRadius.circular(12.0),
                              ),
                              elevation: 5,
                              child: Padding(
                                padding: EdgeInsets.all(10),
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Text(
                                          'แสงสว่างโรงเรือน',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            fontWeight: FontWeight.w800,
                                            fontSize: 16,
                                            color: AppTheme.grey,
                                          ),
                                        ),
                                        IconButton(
                                          iconSize: 50,
                                          icon: new Image.asset('assets/lamp.png'),
                                        )
                                      ],
                                    ),
//                            Row(
//                              children: <Widget>[
//                                Text(
//                                  "Gateway : ",
//                                  style: TextStyle(
//                                    fontWeight: FontWeight.w600,
//                                    fontSize: 16,
//                                    color: AppTheme.grey,
//                                  ),
//                                ),
//                                Image.asset(
//                                  'assets/gateway-off.png',
//                                  height: 15,
//                                  width: 15,
//                                )
//                              ],
//                            ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        IconButton(
                                          onPressed: () => {sendLamp(1)},
                                          iconSize: 50,
                                          icon: new Image.asset("assets/power.png"),
                                        ),
                                        IconButton(
                                          onPressed: () => {sendLamp(0)},
                                          iconSize: 50,
                                          icon: new Image.asset("assets/off.png"),
                                        )
                                      ],
                                    ),

                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[

                                        IconButton(
                                          onPressed: () => {
                                            Navigator.of(context)
                                                .push(MaterialPageRoute(
                                                builder: (_) => TimerPage(
                                                  farm: widget.farm,
                                                  subFarm: widget.subFarm,
                                                  control: "L",
                                                )))
                                          },
                                          iconSize: 25,
                                          icon: Icon(
                                            Icons.timer,
                                            color: Colors.blue,
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap: () => {
                                            Navigator.of(context)
                                                .push(MaterialPageRoute(
                                                builder: (_) => TimerPage(
                                                  farm: widget.farm,
                                                  subFarm: widget.subFarm,
                                                  control: "L",
                                                )))
                                          },
                                          child: Text(
                                            "ตั้งเวลา",
                                            style: TextStyle(
                                                fontFamily: AppTheme.fontName,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ),

                                        IconButton(
                                          onPressed: () => {
                                            Navigator.of(context)
                                                .push(MaterialPageRoute(
                                                builder: (_) => SensingPage(
                                                  farm: widget.farm,
                                                  subFarm: widget.subFarm,
                                                  control: "L",
                                                )))
                                          },
                                          iconSize: 25,
                                          icon: Icon(
                                            Icons.sensor_window,
                                            color: Colors.blue,
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap: () => {
                                            Navigator.of(context)
                                                .push(MaterialPageRoute(
                                                builder: (_) => SensingPage(
                                                  farm: widget.farm,
                                                  subFarm: widget.subFarm,
                                                  control: "L",
                                                )))
                                          },
                                          child: Text(
                                            "ตั้งความสัมพันธ์",
                                            style: TextStyle(
                                                fontFamily: AppTheme.fontName,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        )

                                      ],
                                    ),

                                  ],
                                ),
                              )),
                        )),
                  ],
                )
              ],
            ),
          )),
    );
  }


  void sendWater() async {

    int status = 1;
    if(statusW == true){

      status = 0;
    }


    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('dd/MM/yyyy HH:mm:ss');

    final SharedPreferences prefs = await _prefs;
    String jwt = prefs.getString("user");

    final jsonPrefs = json.decode(jwt);

    User loginUser = User.fromJson(jsonPrefs);

    setState(() {
      datetimeLamp = formatter.format(now);
    });

    final jsons = jsonEncode({"W${widget.subFarm}": 1});

    print(jsonEncode.toString());
    _mqttUtility.publishMessage(
        "control/${widget.farm}", jsons.toString());

    final jsonsLog = jsonEncode({
      "control": "water",
      "status": status,
      "user": loginUser.message.idUser,
      "farm": widget.farm,
      "subFarm": widget.subFarm,
    });

    _mqttUtility.publishMessage("log/control", jsonsLog);
  }

  void sendFog() async {

    int status = 1;
    if(statusF == true){

      status = 0;
    }
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('dd/MM/yyyy HH:mm:ss');

    final SharedPreferences prefs = await _prefs;
    String jwt = prefs.getString("user");

    final jsonPrefs = json.decode(jwt);

    User loginUser = User.fromJson(jsonPrefs);

    setState(() {
      datetimeLamp = formatter.format(now);
    });

    final jsons = jsonEncode({"F": 1});

    print(jsonEncode.toString());
    _mqttUtility.publishMessage(
        "control/${widget.farm}", jsons.toString());

    final jsonsLog = jsonEncode({
      "control": "fog",
      "status": status,
      "user": loginUser.message.idUser,
      "farm": widget.farm,
      "subFarm": widget.subFarm,
    });

    _mqttUtility.publishMessage("log/control", jsonsLog);
  }




  void sendLamp(int status) async {
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('dd/MM/yyyy HH:mm:ss');

    final SharedPreferences prefs = await _prefs;
    String jwt = prefs.getString("user");

    final jsonPrefs = json.decode(jwt);

    User loginUser = User.fromJson(jsonPrefs);

    setState(() {
      datetimeLamp = formatter.format(now);
    });

    final jsons = jsonEncode({"lamp": status});

    print(jsonEncode.toString());
    _mqttUtility.publishMessage(
        "control/lamp/${widget.farm}", jsons.toString());

    final jsonsLog = jsonEncode({
      "control": "Lamp",
      "status": status,
      "user": loginUser.message.idUser,
      "farm": widget.farm,
      "subFarm": widget.subFarm,
    });

    _mqttUtility.publishMessage("log/control", jsonsLog);
  }
}
