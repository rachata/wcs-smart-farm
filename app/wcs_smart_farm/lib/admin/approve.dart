import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:http/http.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wcs_smart_farm/app_theme.dart';
import 'package:wcs_smart_farm/config.dart';
import 'package:wcs_smart_farm/model/my_farm.dart';
import 'package:wcs_smart_farm/model/req.dart';
import 'package:wcs_smart_farm/user/detail.dart';

class ApprovePage extends StatefulWidget {
  const ApprovePage({Key key}) : super(key: key);

  @override
  ApprovePageState createState() => ApprovePageState();
}

class ApprovePageState extends State<ApprovePage>
    with TickerProviderStateMixin {
  final ScrollController scrollController = ScrollController();

  Animation<double> topBarAnimation;
  double topBarOpacity = 0.0;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  bool stateFarm = false;
  List<Widget> farm = [];
  bool stateShow = true;

  ProgressDialog pr;

  @override
  void initState() {
    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Download,
      textDirection: TextDirection.ltr,
      isDismissible: false,
    );

    pr.style(
        message: "Please wait...",
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        insetAnimCurve: Curves.easeInOut,
        progressTextStyle: TextStyle(
            fontFamily: AppTheme.fontName,
            color: Colors.black,
            fontSize: 13.0,
            fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            fontFamily: AppTheme.fontName,
            color: Colors.black,
            fontSize: 19.0,
            fontWeight: FontWeight.w600));

    AnimationController animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);

    topBarAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: animationController,
            curve: Interval(0, 0.5, curve: Curves.fastOutSlowIn)));

    scrollController.addListener(() {
      if (scrollController.offset >= 24) {
        if (topBarOpacity != 1.0) {
          setState(() {
            topBarOpacity = 1.0;
          });
        }
      } else if (scrollController.offset <= 24 &&
          scrollController.offset >= 0) {
        if (topBarOpacity != scrollController.offset / 24) {
          setState(() {
            topBarOpacity = scrollController.offset / 24;
          });
        }
      } else if (scrollController.offset <= 0) {
        if (topBarOpacity != 0.0) {
          setState(() {
            topBarOpacity = 0.0;
          });
        }
      }
    });

    getReq();
    super.initState();
  }

  void getReq() async {
    Response response = await get("${Config.baseURL}/approve/farm");

    final jsonResponse = json.decode(response.body.toString());

    print("jsonResponse ${jsonResponse.toString()}");
    Req farms = Req.fromJson(jsonResponse);

    print("farms ${farms.status}");

    if (farms.status == 200) {
      setState(() {
        stateFarm = true;
      });
      createListFarm(farms);
    } else {
      setState(() {
        stateFarm = false;
      });
    }
  }

  void createListFarm(Req farms) {
    for (int i = 0; i < farms.message.length; i++) {
      if (farms.message[i].note == null || farms.message[i].note == "") {
        setState(() {
          farms.message[i].note = "-";
        });
      }
      setState(() {
        farm.add(InkWell(
            child: Container(
                child: Padding(
          padding: EdgeInsets.all(0),
          child: Card(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                side: new BorderSide(color: AppTheme.wcsRed, width: 2.0),
                borderRadius: BorderRadius.circular(12.0),
              ),
              elevation: 5,
              child: Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Padding(padding: EdgeInsets.only(left: 0, right: 0)),
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: const EdgeInsets.only(top: 5.0),
                              child: new Text(
                                "ชื่อ ${farms.message[i].name}",
                                style: TextStyle(
                                    fontFamily: AppTheme.fontName,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w700),
                              ),
                            ),
                          ],
                        )),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Padding(padding: EdgeInsets.only(left: 0, right: 0)),
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: const EdgeInsets.only(top: 5.0),
                              child: new Text(
                                "โรงเรือนที่ ${farms.message[i].idFarm} แปลงที่ ${farms.message[i].idSubFarm}",
                                style: TextStyle(
                                    fontFamily: AppTheme.fontName,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w700),
                              ),
                            ),
                          ],
                        )),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Padding(padding: EdgeInsets.only(left: 0, right: 0)),
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: const EdgeInsets.only(top: 5.0),
                              child: new Text(
                                "หมายเหตุ ${farms.message[i].note}",
                                style: TextStyle(
                                    fontFamily: AppTheme.fontName,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                          ],
                        )),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Padding(padding: EdgeInsets.only(left: 0, right: 0)),
                        Expanded(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Container(
                                margin: const EdgeInsets.only(top: 5.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    IconButton(
                                        icon: Icon(Icons.restore_from_trash,
                                            color: AppTheme.wcsRed),
                                        onPressed: () => {
                                              _showMyDialogDelete(
                                                  farms.message[i].name,
                                                  farms.message[i].idStd,
                                                  farms.message[i].idFarm,
                                                  farms.message[i].idSubFarm)
                                            }),
                                    GestureDetector(
                                      child: new Text(
                                        "ลบ",
                                        style: TextStyle(
                                            fontFamily: AppTheme.fontName,
                                            fontSize: 18,
                                            color: AppTheme.wcsRed,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      onTap: () => {
                                        _showMyDialogDelete(
                                            farms.message[i].name,
                                            farms.message[i].idStd,
                                            farms.message[i].idFarm,
                                            farms.message[i].idSubFarm)
                                      },
                                    )
                                  ],
                                )),
                          ],
                        )),
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(bottom: 10)),
                  ],
                ),
              )),
        ))));
      });
    }
  }

  Future<void> _showMyDialogDelete(
      String name, String idStd, int farm, int subFarm) async {
    return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return new CupertinoAlertDialog(
            title: new Text(
              "ลบการใช้งาน",
              style: TextStyle(
                  fontSize: 18,
                  fontFamily: AppTheme.fontName,
                  fontWeight: FontWeight.w500),
            ),
            content: new Text(
              "คุณต้องลบการใช้งานของ โรงเรือนที่ ${farm} แปลงที่ ${subFarm} ของ ${name} ",
              style: TextStyle(
                  color: AppTheme.wcsRed,
                  fontSize: 16,
                  fontFamily: AppTheme.fontName,
                  fontWeight: FontWeight.w500),
            ),
            actions: [
              CupertinoDialogAction(
                  isDefaultAction: true,
                  child: new Text("ลบ",
                      style: TextStyle(
                        color: AppTheme.wcsRed,
                        fontFamily: AppTheme.fontName,
                      )),
                  onPressed: () {
                    deleteReq(idStd, farm, subFarm);
                    Navigator.pop(context, 'Cancel');
                  }),
              CupertinoDialogAction(
                  isDefaultAction: true,
                  child: new Text("ยกเลิก",
                      style: TextStyle(
                        fontFamily: AppTheme.fontName,
                      )),
                  onPressed: () {
                    Navigator.pop(context, 'Cancel');
                  })
            ],
          );
        });
  }

  void deleteReq(String idStd, int idfarm, int subFarm) async {

    print("deleteReq");
    await pr.show();

    Response response = await delete(
      "${Config.baseURL}/my-farm/${idStd}/${subFarm}/${idfarm}",
      headers: <String, String>{
        'Accept': 'application/json; charset=UTF-8',
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    final jsonResponse = json.decode(response.body.toString());

    pr.hide();

    if (jsonResponse["status"] == 200) {
      _showMyDialog("สำเร็จ");

      setState(() {
        farm = [];
        getReq();
      });
    } else {
      _showMyDialog("ไม่สำเร็จ");
    }
  }

  Future<void> _showMyDialog(String text) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new CupertinoAlertDialog(
            title: new Text(
              "ลบ",
              style: TextStyle(
                  fontSize: 18,
                  fontFamily: AppTheme.fontName,
                  fontWeight: FontWeight.w500),
            ),
            content: new Text(
              "${text}",
              style: TextStyle(
                  color: AppTheme.detectorGreen,
                  fontSize: 16,
                  fontFamily: AppTheme.fontName,
                  fontWeight: FontWeight.w500),
            ),
            actions: [
              CupertinoDialogAction(
                  isDefaultAction: true,
                  child: new Text("Ok"),
                  onPressed: () {
                    Navigator.pop(context, 'Cancel');
                    Navigator.pop(context, 'Cancel');

                  })
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "การใช้งาน",
          style: TextStyle(fontFamily: AppTheme.fontName),
        ),
        backgroundColor: AppTheme.wcsAdminPrimary,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
          child: stateFarm == true
              ? Column(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: stateShow == false ? [] : farm,
                    ),
                  ],
                )
              : Center(
                  child: Column(
                  children: [
                    Text(
                      "ไม่มีข้อมูลในขณะนี้",
                      style: TextStyle(
                          color: AppTheme.wcsRed,
                          fontFamily: AppTheme.fontName,
                          fontSize: 18,
                          fontWeight: FontWeight.w800),
                    ),
                    Loading(
                        indicator: BallPulseIndicator(),
                        size: 100.0,
                        color: AppTheme.wcsRed),
                  ],
                ))),
    );
  }
}
