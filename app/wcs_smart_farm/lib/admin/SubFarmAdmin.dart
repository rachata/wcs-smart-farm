import 'dart:async';
import 'dart:convert';
import 'package:wcs_smart_farm/admin/DetailAdmin.dart';
import 'package:wcs_smart_farm/config.dart';
import 'package:wcs_smart_farm/model/sub_farm.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wcs_smart_farm/admin/detail_farm/detail_first_1.dart';
import 'package:wcs_smart_farm/app_theme.dart';

class SubFarmAdminPage extends StatefulWidget {
  final String name;
  final int idFarm;

  const SubFarmAdminPage({Key key, this.name, this.idFarm}) : super(key: key);

  @override
  SubFarmAdminPageState createState() => SubFarmAdminPageState();
}

class SubFarmAdminPageState extends State<SubFarmAdminPage>
    with TickerProviderStateMixin {
  final ScrollController scrollController = ScrollController();

  Animation<double> topBarAnimation;
  double topBarOpacity = 0.0;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  bool stateFarm = false;
  List<Widget> farm = [];
  bool stateShow = true;

  @override
  void initState() {
    AnimationController animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);

    topBarAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: animationController,
            curve: Interval(0, 0.5, curve: Curves.fastOutSlowIn)));

    scrollController.addListener(() {
      if (scrollController.offset >= 24) {
        if (topBarOpacity != 1.0) {
          setState(() {
            topBarOpacity = 1.0;
          });
        }
      } else if (scrollController.offset <= 24 &&
          scrollController.offset >= 0) {
        if (topBarOpacity != scrollController.offset / 24) {
          setState(() {
            topBarOpacity = scrollController.offset / 24;
          });
        }
      } else if (scrollController.offset <= 0) {
        if (topBarOpacity != 0.0) {
          setState(() {
            topBarOpacity = 0.0;
          });
        }
      }
    });

    this.getFarm();
    super.initState();
  }

  void getFarm() async {
    Response response =
        await get("${Config.baseURL}/sub-farm/${widget.idFarm}");

    final jsonResponse = json.decode(response.body.toString());

    WCSSubFarm farms = WCSSubFarm.fromJson(jsonResponse);

    if (farms.status == 200) {
      setState(() {
        stateFarm = true;
      });
      createListFarm(farms);
    } else {
      setState(() {
        stateFarm = false;
      });
    }
  }

  void createListFarm(WCSSubFarm farms) {
    for (int i = 0; i < farms.message.length; i++) {
      String ev = "-";

      if (farms.message[i].name != "" &&
          farms.message[i].event != "" &&
          farms.message[i].name != null &&
          farms.message[i].event != null) {
        ev =
            "${farms.message[i].event} โดย ${farms.message[i].name} \nเวลา ${farms.message[i].dt}";
      }

      farm.add(
        InkWell(
            onTap: () => {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (_) => DetailAdminPage(
                            statusF: farms.message[i].statusF,
                            statusL: farms.message[i].statusL,
                            statusW: farms.message[i].statusW,
                            farm: farms.message[i].idFarm,
                            subFarm: farms.message[i].idSub,
                            title:
                                "โรงเรือน ที่ ${farms.message[i].idFarm} แปลง ที่ ${farms.message[i].idSub}",
                          )))
                },
            child: Container(
                child: Padding(
              padding: EdgeInsets.all(0),
              child: Card(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    side: new BorderSide(
                        color: AppTheme.wcsAdminPrimary, width: 2.0),
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  elevation: 5,
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.only(left: 0, right: 0)),
                            Expanded(
                                child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  margin: const EdgeInsets.only(top: 5.0),
                                  child: new Text(
                                    "${farms.message[i].fname}",
                                    style: TextStyle(
                                        fontFamily: AppTheme.fontName,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ],
                            ))
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(bottom: 10)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width - 30,
                              child: Wrap(
                                children: [
                                  Image.asset(
                                    'assets/temp.png',
                                    height: 25,
                                    width: 25,
                                  ),
                                  Padding(padding: EdgeInsets.only(right: 10)),
                                  Text(
                                    farms.message[i].statusT == true
                                        ? "ทำงาน"
                                        : "ไม่ทำงาน",
                                    style: TextStyle(
                                        color: farms.message[i].statusT == true
                                            ? AppTheme.wcsGreen2
                                            : AppTheme.wcsRed,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: AppTheme.fontName,
                                        fontSize: 16),
                                  ),
                                  Padding(padding: EdgeInsets.only(right: 5)),
                                  Image.asset(
                                    'assets/soil.png',
                                    height: 25,
                                    width: 25,
                                  ),
                                  Padding(padding: EdgeInsets.only(right: 10)),
                                  Text(
                                    farms.message[i].statusS == true
                                        ? "ทำงาน"
                                        : "ไม่ทำงาน",
                                    style: TextStyle(
                                        color: farms.message[i].statusS == true
                                            ? AppTheme.wcsGreen2
                                            : AppTheme.wcsRed,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: AppTheme.fontName,
                                        fontSize: 16),
                                  ),
                                  Padding(padding: EdgeInsets.only(right: 5)),
                                  Image.asset(
                                    'assets/sprinkler2.png',
                                    height: 25,
                                    width: 25,
                                  ),
                                  Padding(padding: EdgeInsets.only(right: 10)),
                                  Text(
                                    farms.message[i].statusW == true
                                        ? "ทำงาน"
                                        : "ไม่ทำงาน",
                                    style: TextStyle(
                                        color: farms.message[i].statusW == true
                                            ? AppTheme.wcsGreen2
                                            : AppTheme.wcsRed,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: AppTheme.fontName,
                                        fontSize: 16),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        Padding(padding: EdgeInsets.only(bottom: 10)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width - 30,
                              child: Wrap(
                                runAlignment: WrapAlignment.center,
                                children: [
                                  Image.asset(
                                    'assets/sprinkler.png',
                                    height: 25,
                                    width: 25,
                                  ),
                                  Padding(padding: EdgeInsets.only(right: 10)),
                                  Text(
                                    farms.message[i].statusF == true
                                        ? "ทำงาน"
                                        : "ไม่ทำงาน",
                                    style: TextStyle(
                                        color: farms.message[i].statusF == true
                                            ? AppTheme.wcsGreen2
                                            : AppTheme.wcsRed,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: AppTheme.fontName,
                                        fontSize: 16),
                                  ),
                                  Padding(padding: EdgeInsets.only(right: 5)),
                                  Image.asset(
                                    'assets/lamp.png',
                                    height: 25,
                                    width: 25,
                                  ),
                                  Padding(padding: EdgeInsets.only(right: 10)),
                                  Text(
                                    farms.message[i].statusL == true
                                        ? "ทำงาน"
                                        : "ไม่ทำงาน",
                                    style: TextStyle(
                                        color: farms.message[i].statusL == true
                                            ? AppTheme.wcsGreen2
                                            : AppTheme.wcsRed,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: AppTheme.fontName,
                                        fontSize: 16),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
//                        Padding(padding: EdgeInsets.only(bottom: 10)),
//                        Row(
//                          mainAxisAlignment: MainAxisAlignment.start,
//                          children: <Widget>[
//                            Expanded(
//                                child: Column(
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: <Widget>[
//                                Container(
//                                  margin: const EdgeInsets.only(top: 0),
//                                  child: new Text(
//                                    "ดำเนินการล่าสุด : ${ev}",
//                                    style: TextStyle(
//                                        fontFamily: AppTheme.fontName,
//                                        fontSize: 16,
//                                        color: AppTheme.wcsGreen2,
//                                        fontWeight: FontWeight.w600),
//                                  ),
//                                ),
//                              ],
//                            ))
//                          ],
//                        ),
                      ],
                    ),
                  )),
            ))),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "แปลง ${widget.name}",
            style: TextStyle(fontFamily: AppTheme.fontName),
          ),
          backgroundColor: AppTheme.wcsAdminPrimary,
        ),
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
            child: stateFarm == true
                ? Column(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: stateShow == false ? [] : farm,
                      ),
                    ],
                  )
                : Center(
                    child: Column(
                    children: [
                      Text(
                        "ไม่มีข้อมูลในขณะนี้",
                        style: TextStyle(
                            color: AppTheme.wcsRed,
                            fontFamily: AppTheme.fontName,
                            fontSize: 18,
                            fontWeight: FontWeight.w800),
                      ),
                      Loading(
                          indicator: BallPulseIndicator(),
                          size: 100.0,
                          color: AppTheme.wcsRed),
                    ],
                  ))));
  }

  checkFarm(WCSSubFarm farms, int i) {
    if (farms.message[i].idFarm == 1 && farms.message[i].idSub == 1) {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (_) => DetailFirst1(
                statusF: farms.message[i].statusF,
                statusL: farms.message[i].statusL,
                statusW: farms.message[i].statusW,
                farm: farms.message[i].idFarm,
                subFarm: farms.message[i].idSub,
                title:
                    "โรงเรือน ที่ ${farms.message[i].idFarm} แปลง ที่ ${farms.message[i].idSub}",
              )));
    }
  }
}
