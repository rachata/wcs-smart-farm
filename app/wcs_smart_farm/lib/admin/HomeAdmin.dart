import 'dart:async';
import 'package:bottom_navigation_badge/bottom_navigation_badge.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wcs_smart_farm/admin/AlertAdmin.dart';
import 'package:wcs_smart_farm/admin/FarmAdmin.dart';
import 'package:wcs_smart_farm/admin/approve.dart';
import 'package:wcs_smart_farm/admin/profile.dart';
import 'package:wcs_smart_farm/admin/req.dart';
import 'package:wcs_smart_farm/app_theme.dart';

class HomeAdminPage extends StatefulWidget {
  const HomeAdminPage({Key key}) : super(key: key);

  @override
  HomeAdminPageState createState() => HomeAdminPageState();
}

class HomeAdminPageState extends State<HomeAdminPage> with TickerProviderStateMixin {
  final ScrollController scrollController = ScrollController();

  Animation<double> topBarAnimation;
  double topBarOpacity = 0.0;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  BottomNavigationBadge badger = new BottomNavigationBadge(
      backgroundColor: Colors.red,
      badgeShape: BottomNavigationBadgeShape.circle,
      textColor: Colors.white,
      position: BottomNavigationBadgePosition.topRight,
      textSize: 10);


  List<Widget> _widgetOptions = <Widget>[];

  int currentIndex = 0;

  final List<Widget> _children = [
    FarmAdminPage(),
    ApprovePage(),
    ReqPage(),
    AlertAdminPage(),
    ProfileAdminPage()
  ];


  void setUpOneSignal() async {
    OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);

    OneSignal.shared.init("6447352b-abed-44d0-9300-36473501dac4", iOSSettings: {
      OSiOSSettings.autoPrompt: true,
      OSiOSSettings.inAppLaunchUrl: true
    });
    OneSignal.shared
        .setInFocusDisplayType(OSNotificationDisplayType.notification);

    await OneSignal.shared
        .promptUserForPushNotificationPermission(fallbackToSettings: true);

  }

  @override
  void initState() {
    currentIndex = 0;

    AnimationController animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);

    topBarAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: animationController,
            curve: Interval(0, 0.5, curve: Curves.fastOutSlowIn)));

    scrollController.addListener(() {
      if (scrollController.offset >= 24) {
        if (topBarOpacity != 1.0) {
          setState(() {
            topBarOpacity = 1.0;
          });
        }
      } else if (scrollController.offset <= 24 &&
          scrollController.offset >= 0) {
        if (topBarOpacity != scrollController.offset / 24) {
          setState(() {
            topBarOpacity = scrollController.offset / 24;
          });
        }
      } else if (scrollController.offset <= 0) {
        if (topBarOpacity != 0.0) {
          setState(() {
            topBarOpacity = 0.0;
          });
        }
      }
    });


    setUpOneSignal();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      backgroundColor: Colors.white,
      body: _children[currentIndex],
      bottomNavigationBar: BottomNavyBar(
        backgroundColor: AppTheme.wcsAdminPrimary,
        selectedIndex: currentIndex,
        showElevation: true,
        itemCornerRadius: 8,
        curve: Curves.easeInBack,
        onItemSelected: (index) => setState(() {
          print(index);
          currentIndex = index;
        }),
        items: [
          BottomNavyBarItem(
            inactiveColor: AppTheme.white,
            icon: Icon(Icons.home),
            title: Text("โรงเรือน",
                style: TextStyle(fontFamily: AppTheme.fontName)),
            activeColor: AppTheme.wcsPrimary,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            inactiveColor: AppTheme.white,
            icon: Icon(Icons.check),
            title: Text("การใช้งาน",
                style: TextStyle(fontFamily: AppTheme.fontName)),
            activeColor: AppTheme.wcsPrimary,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            inactiveColor: AppTheme.white,
            icon: Icon(Icons.supervised_user_circle),
            title: Text("คำขอใช้งาน",
                style: TextStyle(fontFamily: AppTheme.fontName)),
            activeColor: AppTheme.wcsPrimary,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            inactiveColor: AppTheme.white,
            icon: Icon(Icons.add_alert),
            title: Text("แจ้งเตือน",
                style: TextStyle(fontFamily: AppTheme.fontName)),
            activeColor: AppTheme.wcsPrimary,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            inactiveColor: AppTheme.white,
            icon: Icon(Icons.accessibility),
            title: Text("โปรไฟล์",
                style: TextStyle(fontFamily: AppTheme.fontName)),
            activeColor: AppTheme.wcsPrimary,
            textAlign: TextAlign.center,
          ),

        ],
      ),
    );
  }
}
