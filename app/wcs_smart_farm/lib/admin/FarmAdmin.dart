import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:wcs_smart_farm/admin/SubFarmAdmin.dart';
import 'package:wcs_smart_farm/app_theme.dart';
import 'package:wcs_smart_farm/config.dart';
import 'package:wcs_smart_farm/model/farm.dart';

class FarmAdminPage extends StatefulWidget {
  const FarmAdminPage({Key key}) : super(key: key);

  @override
  FarmAdminPageState createState() => FarmAdminPageState();
}

class FarmAdminPageState extends State<FarmAdminPage> with TickerProviderStateMixin {
  final ScrollController scrollController = ScrollController();

  Animation<double> topBarAnimation;
  double topBarOpacity = 0.0;

  bool stateFarm = false;
  List<Widget> farm = [];
  bool stateShow = true;

  @override
  void initState() {
    AnimationController animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);

    topBarAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: animationController,
            curve: Interval(0, 0.5, curve: Curves.fastOutSlowIn)));

    scrollController.addListener(() {
      if (scrollController.offset >= 24) {
        if (topBarOpacity != 1.0) {
          setState(() {
            topBarOpacity = 1.0;
          });
        }
      } else if (scrollController.offset <= 24 &&
          scrollController.offset >= 0) {
        if (topBarOpacity != scrollController.offset / 24) {
          setState(() {
            topBarOpacity = scrollController.offset / 24;
          });
        }
      } else if (scrollController.offset <= 0) {
        if (topBarOpacity != 0.0) {
          setState(() {
            topBarOpacity = 0.0;
          });
        }
      }
    });

    this.getFarm();
    super.initState();
  }

  void getFarm() async {
    Response response = await get("${Config.baseURL}/farm");

    final jsonResponse = json.decode(response.body.toString());


    print(jsonResponse.toString());
    WCSFarm farms = WCSFarm.fromJson(jsonResponse);

    print("farms ${farms.status}");
    if (farms.status == 200) {
      setState(() {
        stateFarm = true;
      });
      createListFarm(farms);
    } else {
      setState(() {
        stateFarm = false;
      });
    }
  }

  void createListFarm(WCSFarm farms) {
    for (int i = 0; i < farms.message.length; i++) {
      String ev = "-";

      if (farms.message[i].uname != "" && farms.message[i].event != "" &&
          farms.message[i].uname != null && farms.message[i].event != null) {
        ev =
        "${farms.message[i].event} โดย ${farms.message[i].uname} \nเวลา ${farms
            .message[i].dt}";
      }


      farm.add(

        InkWell(
            onTap: () => {


              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => SubFarmAdminPage(name: farms.message[i].fname, idFarm:  farms.message[i].idFarm,)))
            },
            child: Container(
                child: Padding(
                  padding: EdgeInsets.all(0),
                  child: Card(
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        side: new BorderSide(
                            color: AppTheme.wcsAdminPrimary, width: 2.0),
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                      elevation: 5,
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[

                                Container(
                                  child: FadeInImage.assetNetwork(

                                      placeholder: "assets/atom.gif",
                                      image: farms.message[i].image,
                                      height: 60.0,
                                      fit: BoxFit.cover
                                  ),
                                ),
                                Padding(padding: EdgeInsets.only(
                                    left: 10, right: 10)),
                                Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment
                                          .start,
                                      children: <Widget>[
                                        Container(
                                          margin: const EdgeInsets.only(
                                              top: 5.0),
                                          child: new Text(
                                            "${farms.message[i].fname}",
                                            style: TextStyle(
                                                fontFamily: AppTheme.fontName,
                                                fontSize: 18,
                                                fontWeight: FontWeight.w700),
                                          ),
                                        ),
                                      ],
                                    ))
                              ],
                            ),
                            Padding(padding: EdgeInsets.only(bottom: 0)),
//                            Row(
//                              mainAxisAlignment: MainAxisAlignment.start,
//                              children: <Widget>[
//                                Expanded(
//                                    child: Column(
//                                      crossAxisAlignment: CrossAxisAlignment
//                                          .start,
//                                      children: <Widget>[
//                                        Container(
//                                          margin: const EdgeInsets.only(top: 0),
//                                          child: new Text(
//                                            "ดำเนินการล่าสุด : ${ev}",
//                                            style: TextStyle(
//                                                fontFamily: AppTheme.fontName,
//                                                fontSize: 16,
//                                                color: AppTheme.wcsGreen2,
//                                                fontWeight: FontWeight.w600),
//                                          ),
//                                        ),
//                                      ],
//                                    ))
//                              ],
//                            ),
                          ],
                        ),
                      )),
                ))),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "โรงเรือน",
            style: TextStyle(fontFamily: AppTheme.fontName),
          ),
          backgroundColor: AppTheme.wcsAdminPrimary,
        ),
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
            child: stateFarm == true
                ? Column(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: stateShow == false ? [] : farm,
                ),
              ],
            )
                : Center(
                child: Column(
                  children: [
                    Text(
                      "ไม่มีข้อมูลในขณะนี้",
                      style: TextStyle(
                          color: AppTheme.wcsRed,
                          fontFamily: AppTheme.fontName,
                          fontSize: 18,
                          fontWeight: FontWeight.w800),
                    ),
                    Loading(
                        indicator: BallPulseIndicator(),
                        size: 100.0,
                        color: AppTheme.wcsRed),
                  ],
                ))));
  }

  checkFarm() {


  }
}
