import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wcs_smart_farm/app_theme.dart';
import 'package:wcs_smart_farm/config.dart';

class CrateNotiPage extends StatefulWidget {
  const CrateNotiPage({Key key}) : super(key: key);

  @override
  CrateNotiPageState createState() => CrateNotiPageState();
}

class CrateNotiPageState extends State<CrateNotiPage>
    with TickerProviderStateMixin {
  final ScrollController scrollController = ScrollController();

  Animation<double> topBarAnimation;
  double topBarOpacity = 0.0;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  File image;

  TextEditingController name = TextEditingController();
  TextEditingController title = TextEditingController();
  TextEditingController message = TextEditingController();


  ProgressDialog pr;

  cameraConnect() async {
    print('Picker is Called');
    File img = await ImagePicker.pickImage(source: ImageSource.camera);

    if (img != null) {
      image = img;
      setState(() {
        image = img;
      });
    }
  }

  @override
  void initState() {
    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Download,
      textDirection: TextDirection.ltr,
      isDismissible: false,
    );

    pr.style(
        message: "Please wait...",
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        insetAnimCurve: Curves.easeInOut,
        progressTextStyle: TextStyle(
            color: AppTheme.wcsPrimary,
            fontSize: 13.0,
            fontFamily: AppTheme.fontName,
            fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: AppTheme.wcsPrimary,
            fontSize: 19.0,
            fontFamily: AppTheme.fontName,
            fontWeight: FontWeight.w600));

    AnimationController animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);

    topBarAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: animationController,
            curve: Interval(0, 0.5, curve: Curves.fastOutSlowIn)));

    scrollController.addListener(() {
      if (scrollController.offset >= 24) {
        if (topBarOpacity != 1.0) {
          setState(() {
            topBarOpacity = 1.0;
          });
        }
      } else if (scrollController.offset <= 24 &&
          scrollController.offset >= 0) {
        if (topBarOpacity != scrollController.offset / 24) {
          setState(() {
            topBarOpacity = scrollController.offset / 24;
          });
        }
      } else if (scrollController.offset <= 0) {
        if (topBarOpacity != 0.0) {
          setState(() {
            topBarOpacity = 0.0;
          });
        }
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "แจ้งเตือน",
          style: TextStyle(fontFamily: AppTheme.fontName),
        ),
        backgroundColor: AppTheme.wcsAdminPrimary,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Padding(padding: EdgeInsets.only(bottom: 10)),
                      Container(
                          width: MediaQuery.of(context).size.width - 20,
                          child: new Theme(
                            data: new ThemeData(
                              primaryColor: AppTheme.wcsRed,
                              primaryColorDark: AppTheme.wcsRed,
                            ),
                            child: new TextField(
                              controller: name,
                              style: TextStyle(fontFamily: AppTheme.fontName),
                              decoration: new InputDecoration(
                                border: new OutlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: AppTheme.wcsPrimary)),
                                hintText: 'จาก',
                                labelText: 'จาก',
                                prefixIcon: const Icon(
                                  Icons.accessibility,
                                  color: AppTheme.wcsPrimary,
                                ),
                              ),
                            ),
                          )),
                      Padding(padding: EdgeInsets.only(bottom: 10)),
                      Container(
                          width: MediaQuery.of(context).size.width - 20,
                          child: new Theme(
                            data: new ThemeData(
                              primaryColor: AppTheme.wcsRed,
                              primaryColorDark: AppTheme.wcsRed,
                            ),
                            child: new TextField(
                              controller: title,
                              style: TextStyle(fontFamily: AppTheme.fontName),
                              decoration: new InputDecoration(
                                border: new OutlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: AppTheme.wcsPrimary)),
                                hintText: 'หัวข้อ',
                                labelText: 'หัวข้อ',
                                prefixIcon: const Icon(
                                  Icons.title,
                                  color: AppTheme.wcsPrimary,
                                ),
                              ),
                            ),
                          )),
                      Padding(padding: EdgeInsets.only(bottom: 10)),
                      Container(
                          width: MediaQuery.of(context).size.width - 20,
                          child: new Theme(
                            data: new ThemeData(
                              primaryColor: AppTheme.wcsRed,
                              primaryColorDark: AppTheme.wcsRed,
                            ),
                            child: new TextField(
                              maxLines: 5,
                              controller: message,
                              style: TextStyle(fontFamily: AppTheme.fontName),
                              decoration: new InputDecoration(
                                border: new OutlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: AppTheme.wcsPrimary)),
                                hintText: 'ข้อความ',
                                labelText: 'ข้อความ',
                                prefixIcon: const Icon(
                                  Icons.message,
                                  color: AppTheme.wcsPrimary,
                                ),
                              ),
                            ),
                          )),
                      Padding(padding: EdgeInsets.only(bottom: 10)),
                      Container(
                        height: 50,
                        child: new OutlineButton(
                          shape: StadiumBorder(),
                          textColor: AppTheme.dark_grey,
                          child: Text(
                            'ส่งการแจ้งเตือน',
                            style: TextStyle(
                                fontFamily: AppTheme.fontName, fontSize: 20),
                          ),
                          borderSide: BorderSide(
                              color: AppTheme.wcsPrimary,
                              style: BorderStyle.solid,
                              width: 1),
                          onPressed: () {
                            sendNoti();
                          },
                        ),
                      )
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void sendNoti() async {
    FocusScope.of(context).requestFocus(FocusNode());
    await pr.show();

    final jsons = jsonEncode({
      "form": name.text.trim().toString(),
      "title": title.text.trim().toString(),
      "message" : message.text.trim().toString()
    });

    Response response = await post("${Config.baseURL}/notification",
        headers: <String, String>{
          'Accept': 'application/json; charset=UTF-8',
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsons);

    final jsonResponse = json.decode(response.body.toString());

    print(jsonResponse.toString());

    pr.hide();
    if (jsonResponse["status"] == 200) {
      _showMyDialog("ส่งการแจ้งเตือนสำเร็จ");
    } else {
      _showMyDialog("ส่งการแจ้งเตือนไม่สำเร็จ");
    }
  }

  Future<void> _showMyDialog(String text) async {
    return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return new CupertinoAlertDialog(
            title: new Text(
              "โปรไฟล์",
              style: TextStyle(fontFamily: AppTheme.fontName),
            ),
            content: new Text("${text}"),
            actions: [
              CupertinoDialogAction(
                  isDefaultAction: true,
                  child: new Text("Ok"),
                  onPressed: () {
                    Navigator.pop(context, 'Cancel');
                  })
            ],
          );
        });
  }

  void updateProfile() async {
    await pr.show();
    if (image.path != null || image.path != "") {
      await pr.show();

      var request =
      MultipartRequest("POST", Uri.parse("${Config.baseURL}/std/image"));

      request.fields["idStd"] = "159404140014";

      var pic = await MultipartFile.fromPath("image", image.path);

      request.files.add(pic);
      var response = await request.send();

      var responseData = await response.stream.toBytes();
      var responseString = String.fromCharCodes(responseData);
      print(responseString);

      final jsonResponse = json.decode(responseString.toString());

      pr.hide();
      if (jsonResponse['status'] == 200) {
        pr.hide();
        _showMyDialog("บันทึกสำเร็จ");
      } else {
        pr.hide();
        _showMyDialog("ผิดพลาดกรุณาลองใหม่อีกครั้ง");
      }
    }
  }
}
