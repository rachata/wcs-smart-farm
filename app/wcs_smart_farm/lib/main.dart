import 'package:flutter/material.dart';
import 'package:wcs_smart_farm/admin/HomeAdmin.dart';
import 'package:wcs_smart_farm/admin/approve.dart';
import 'package:wcs_smart_farm/admin/req.dart';
import 'package:wcs_smart_farm/home.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:wcs_smart_farm/login.dart';
import 'package:wcs_smart_farm/timer.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WCS Smart Farm',
      theme: ThemeData(

        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: LoginPage(),
    );
  }
}

class HexColor extends Color {
  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));

  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');
    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }
}


