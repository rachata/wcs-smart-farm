import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'package:wcs_smart_farm/admin/HomeAdmin.dart';
import 'package:wcs_smart_farm/app_theme.dart';
import 'package:wcs_smart_farm/config.dart';
import 'package:wcs_smart_farm/home.dart';
import 'package:wcs_smart_farm/model/user.dart';
import 'package:wcs_smart_farm/register.dart';

class LoginPage extends StatefulWidget {
  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  checkLogin() async {
    final SharedPreferences prefs = await _prefs;
    String jwt = prefs.getString("user");

    if (jwt != "" && jwt != null) {
      final jsonPrefs = json.decode(jwt);

      User loginUser = User.fromJson(jsonPrefs);

      if (loginUser.message.type == 0) {
        setUpOneSignal();

        Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (_) => HomePage()));
      } else {
        setUpOneSignal();

        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (_) => HomeAdminPage()));
      }
    }
  }

  @override
  void initState() {
    checkLogin();
  }

  void login() async {
    final jsons = jsonEncode({
      "username": username.text.toString(),
      "password": password.text.toString()
    });

    Response response = await post("${Config.baseURL}/login",
        headers: <String, String>{
          'Accept': 'application/json; charset=UTF-8',
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsons);

    final jsonResponse = json.decode(response.body.toString());

    User loginUser = User.fromJson(jsonResponse);

    print(jsonResponse.toString());
    if (loginUser.status == 200) {
      final SharedPreferences prefs = await _prefs;

      setState(() {
        prefs
            .setString("user", response.body.toString())
            .then((bool success) async {
          if (success) {
            if (loginUser.message.type == 0) {
              setUpOneSignal();

              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (_) => HomePage()));
            } else {
              setUpOneSignal();

              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (_) => HomeAdminPage()));
            }
          }
        });
      });
    } else {
      _showMyDialog("Wrong username or password");
    }
  }

  void setUpOneSignal() async {


    OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);

    OneSignal.shared.init("6447352b-abed-44d0-9300-36473501dac4", iOSSettings: {
      OSiOSSettings.autoPrompt: true,
      OSiOSSettings.inAppLaunchUrl: true
    });
    OneSignal.shared
        .setInFocusDisplayType(OSNotificationDisplayType.notification);

    await OneSignal.shared
        .promptUserForPushNotificationPermission(fallbackToSettings: true);
  }

  Future<void> _showMyDialog(String text) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Login',
            style: TextStyle(fontFamily: 'Prompt'),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  "${text}",
                  style: TextStyle(fontFamily: 'Prompt'),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'ตกลง',
                style: TextStyle(fontFamily: 'Prompt'),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget welcomeBack = Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Image.asset("assets/logo.png", height: 150, fit: BoxFit.cover),
        Text(
          'WCS Smart Farm',
          style: TextStyle(
              fontFamily: AppTheme.fontName,
              color: Colors.white,
              fontSize: 34.0,
              fontWeight: FontWeight.w600,
              shadows: [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.15),
                  offset: Offset(0, 5),
                  blurRadius: 10.0,
                )
              ]),
        )
      ],
    );

    Widget subTitle = Padding(
        padding: const EdgeInsets.only(right: 56.0),
        child: Text(
          '',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
          ),
        ));

    Widget loginButton = Positioned(
      left: MediaQuery.of(context).size.width / 4,
      bottom: 40,
      child: InkWell(
        onTap: () {
          login();
//          Navigator.of(context)
//              .pushReplacement(MaterialPageRoute(builder: (_) => MyHomePage()));
        },
        child: Container(
          width: MediaQuery.of(context).size.width / 2,
          height: 80,
          child: Center(
              child: new Text("Log In",
                  style: const TextStyle(
                      fontFamily: 'Prompt',
                      color: const Color(0xfffefefe),
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal,
                      fontSize: 20.0))),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [
                    Color.fromRGBO(236, 60, 3, 1),
                    Color.fromRGBO(234, 60, 3, 1),
                    Color.fromRGBO(216, 78, 16, 1),
                  ],
                  begin: FractionalOffset.topCenter,
                  end: FractionalOffset.bottomCenter),
              boxShadow: [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.16),
                  offset: Offset(0, 5),
                  blurRadius: 10.0,
                )
              ],
              borderRadius: BorderRadius.circular(9.0)),
        ),
      ),
    );

    Widget loginForm = Container(
      height: 240,
      child: Stack(
        children: <Widget>[
          Container(
            height: 160,
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.only(left: 32.0, right: 12.0),
            decoration: BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.8),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                    bottomLeft: Radius.circular(10))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: TextField(
                    decoration: InputDecoration(hintText: 'Username'),
                    controller: username,
                    style: TextStyle(fontSize: 16.0, fontFamily: 'Prompt'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: TextField(
                    decoration: InputDecoration(hintText: 'Password'),
                    controller: password,
                    style: TextStyle(fontSize: 16.0, fontFamily: 'Prompt'),
                    obscureText: true,
                  ),
                ),
              ],
            ),
          ),
          loginButton,
        ],
      ),
    );

    Widget forgotPassword = Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Forgot your password? ',
            style: TextStyle(
              fontFamily: 'Prompt',
              fontStyle: FontStyle.italic,
              color: Color.fromRGBO(255, 255, 255, 0.5),
              fontSize: 14.0,
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => RegisterPage()));
            },
            child: Text(
              'Register',
              style: TextStyle(
                fontFamily: 'Prompt',
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 14.0,
              ),
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/bg.jpg'), fit: BoxFit.cover)),
          ),
          Container(
            decoration: BoxDecoration(
              color: Color.fromRGBO(87, 191, 149, 0.7),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(3),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 50,
                ),
                welcomeBack,
//                Spacer(),
//                subTitle,
                Spacer(flex: 1),
                loginForm,
                Spacer(flex: 2),
                forgotPassword
              ],
            ),
          )
        ],
      ),
    );
  }
}
