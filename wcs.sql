show databases

SET  sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));
use wcs_smart_farm


drop table noti



select * from noti


show tables



drop table timer
create table timer(
	id int not null primary key auto_increment,
    farm int,
    sub_farm int ,
	start_dt datetime,
    end_dt datetime,
    control varchar(10),
    status_logi bool ,
    status bool default true
)


delete from noti where  id = 
create table log_temp_hum(
	id int not null primary key auto_increment,
    farm int ,
    sub_farm int , 
    temp double,
    hum double ,
    dt datetime default CURRENT_TIMESTAMP
)

delete from log_temp_hum where id >=1
select * from log_temp_hum order by id desc


insert into log_temp_hum (farm , sub_farm ,  temp , hum , dt) values (1 ,1 , 99 , 22 , '2020-12-22 22:00:00') 

SELECT  avg(temp) as temp  , avg(hum) as hum , DATE_FORMAT(dt,'%Y-%m-%d %H') as dtf  FROM log_temp_hum  where dt between '2020/11/20 00:00:00' AND '2020/11/20 23:59:59'  AND farm =  1 AND sub_farm = 1 GROUP BY dtf order by dtf  ASC



SELECT avg(temp) as temp  , avg(hum) as hum ,  DATE_FORMAT(dt,'%Y-%m-%d') as dtf   FROM log_temp_hum  where dt between '2020/11/01' AND CONCAT(LAST_DAY('2020/11/01')) AND farm =  1 AND sub_farm = 1 GROUP BY dtf order by dtf  ASC 
   

SELECT avg(temp) as temp  , avg(hum) as hum ,  DATE_FORMAT(dt,'%Y-%m') as dtf  FROM log_temp_hum  where dt between '2020-01-01'  AND '2020-12-31' AND farm =  1 AND sub_farm = 1 GROUP BY dtf order by dtf  ASC


   
    
    select * from log_soil
    
    SELECT  avg(soil) as soil ,   DATE_FORMAT(dt,'%Y-%m-%d %H') as dtf  FROM log_soil  where dt between '2020/11/20 00:00:00' AND '2020/11/20 23:59:59'  AND farm =  1 AND sub_farm = 1 GROUP BY dtf order by dtf  ASC


SELECT  avg(soil) as soil ,   DATE_FORMAT(dt,'%Y-%m-%d') as dtf   FROM log_soil  where dt between '2020/11/01' AND CONCAT(LAST_DAY('2020/11/01')) AND farm =  1 AND sub_farm = 1 GROUP BY dtf order by dtf  ASC 
   
   
SELECT avg(soil) as soil ,  DATE_FORMAT(dt,'%Y-%m') as dtf  FROM log_soil  where dt between '2020-01-01'  AND '2020-12-31' AND farm =  1 AND sub_farm = 1 GROUP BY dtf order by dtf  ASC


    


drop table log_soil
create table log_soil(
	id int not null primary key auto_increment,
    farm int ,
     sub_farm int , 
    soil double,
    dt datetime default CURRENT_TIMESTAMP
)

select * from log_soil



select * from log_temp_hum

create table noti(
	id int not null primary key auto_increment,
    title text,
    message text ,
    url text,
    image text,
    form text,
    dt timestamp
    
)

select * from noti


drop table farm

drop table user


alter table user
	add column name text
create table user(
	id int not null primary key auto_increment,
    username varchar(255),
    password varchar(255),
    type int default 0
)


drop table detail_user

desc detail_user
create table detail_user (
    id_std text , 
    name text,
    level text ,
    phone text,
    image text 
)

select * from detail_user where id_std = "1212"

insert into user (id_std , name , level , phone) values ("159404140014" , "นายรัชฏ สุรียะ" , "ม6/2" , "0805041301") 


update user set name = '' , level = '' , phone = '' WHERE id_std = ''

select id_farm , farm.name as 'fname' , image , event , user_event , DATE_FORMAT(dt, '%d-%m-%Y %T') as 'dt' , user.name as 'uname'  from farm left join user on farm.user_event = user.id

select * from detail_user
drop table farm

create table farm(
	id int not null primary key auto_increment,
    id_farm int ,
    name text ,
    image text,
    event text,
    user_event text , 
    dt timestamp
)

alter table farm modify user_event int

select * from farm


select * from sub_farm
insert into farm (id_farm , name , image) value (3 , "โรงเรือน ที่3" , "https://miro.medium.com/max/12342/1*MBUfRSWKdHi6Fl_bKOxG1Q.jpeg")



select farm.id , id_farm , farm.name as 'fname' , image , event , user_event , DATE_FORMAT(dt, "%d-%m-%Y %T") as "dt" , user.name as 'uname'  from farm left join user on farm.user_event = user.id

drop table sub_farm


update sub_farm set dt_gateway_temp = '' WHERE id_farm = AND id_sub = 
create table sub_farm(
	id int not null primary key auto_increment,
    id_farm int ,
    id_sub int ,
    name text ,
	event text,
    user_event int , 
    dt timestamp ,
    status boolean,
    dt_gateway_temp datetime,
    dt_gateway_soil datetime,
	dt_gateway_water datetime,
    dt_gateway_fog datetime,
    dt_gateway_lamp datetime
    
)

select * from sub_farm

alter table sub_farm
	add column select_control bool default false 
    
create table sub_farm_timer(
	id int not null primary key auto_increment,
    id_farm int ,
    id_sub int ,
    start_dt datetime,
    end_dt datetime,
    control_type varchar(50),
	status_lamp bool,
	status_water bool,
    status_fog bool
    
)

select * from sub_farm

alter table sub_farm
	add column status bool default false

select sub_farm.id , id_farm , id_sub , sub_farm.name as 'fname' , sub_farm.status ,  event , user.name , DATE_FORMAT(sub_farm.dt, "%d-%m-%Y %T") as "dt" , status  ,
DATE_FORMAT(dt_gateway_temp, "%Y-%m-%d %T") as "temp" , DATE_FORMAT(dt_gateway_soil, "%Y-%m-%d %T") as "soil" , DATE_FORMAT(dt_gateway_water, "%Y-%m-%d %T") as "water",
DATE_FORMAT(dt_gateway_fog, "%Y-%m-%d %T") as "fog" , DATE_FORMAT(dt_gateway_lamp, "%Y-%m-%d %T") as "lamp"
from sub_farm  
left join user on user.id = sub_farm.user_event
where  id_farm = 1

update sub_farm set status = false where id = >=1 

insert into sub_farm  (id_farm , id_sub , name )
values (3, 3 , "แปลง ที่ 3" )


update sub_farm set dt_gateway_temp = '2020-11-20 00:29:00' where id = 2

select * from sub_farm


select id , title , message , url , image , form , DATE_FORMAT(dt, "%d-%m-%Y %T") as "dt"  from noti order by id desc


select * from request_sub_farm where id_farm  = AND  id_sub_farm = AND id_std = ''

insert into request_sub_farm (id_farm , id_sub_farm , id_std , note) values ( , , , ,)



select * from request_sub_farm

delete from request_sub_farm where id_sub_farm = AND id_std = 

select sub_farm.id , sub_farm.id_farm , sub_farm.id_sub , sub_farm.name  as 'fname', event , detail_user.name , DATE_FORMAT(sub_farm.dt, '%d/%m/%Y %T') as 'dt'   , DATE_FORMAT(dt_gateway_temp, '%Y-%m-%d %T') as 'temp' , DATE_FORMAT(dt_gateway_soil, '%Y-%m-%d %T') as 'soil' , DATE_FORMAT(dt_gateway_water, '%Y-%m-%d %T') as 'water', DATE_FORMAT(dt_gateway_fog, '%Y-%m-%d %T') as 'fog' , DATE_FORMAT(dt_gateway_lamp, '%Y-%m-%d %T') as 'lamp' from request_sub_farm inner join sub_farm on request_sub_farm.id_farm = sub_farm.id_farm AND request_sub_farm.id_sub_farm =  sub_farm.id_sub inner join detail_user on request_sub_farm.id_std = detail_user.id_std where request_sub_farm.id_std ='1233'

create table request_sub_farm(
		id int not null primary key auto_increment,
		id_farm int ,
        id_sub_farm int ,
        id_std text,
        note text ,
        status boolean default 0,
		dt datetime default CURRENT_TIMESTAMP
)
SET SQL_SAFE_UPDATES = 0;


select sub_farm.id , sub_farm.id_farm , sub_farm.id_sub , sub_farm.name  as 'fname', event , detail_user.name , DATE_FORMAT(sub_farm.dt, '%d/%m/%Y %T') as 'dt'   , DATE_FORMAT(dt_gateway_temp, '%Y-%m-%d %T') as 'temp' , DATE_FORMAT(dt_gateway_soil, '%Y-%m-%d %T') as 'soil' , DATE_FORMAT(dt_gateway_water, '%Y-%m-%d %T') as 'water', DATE_FORMAT(dt_gateway_fog, '%Y-%m-%d %T') as 'fog' , DATE_FORMAT(dt_gateway_lamp, '%Y-%m-%d %T') as 'lamp' from request_sub_farm inner join sub_farm on request_sub_farm.id_farm = sub_farm.id_farm AND request_sub_farm.id_sub_farm =  sub_farm.id_sub inner join detail_user on request_sub_farm.id_std = detail_user.id_std where request_sub_farm.id_std ='1233'

select * from detail_user


select * from request_sub_farm
update detail_user set id_std = '1233' where id_std = '159404140014'

delete from request_sub_farm where id = 2


select id_farm , id_sub_farm , detail_user.name , request_sub_farm.id_std , note  ,   DATE_FORMAT(dt, '%d-%m-%Y %T') as 'dt' from request_sub_farm
inner join detail_user on detail_user.id_std = request_sub_farm.id_std
where status =0

update request_sub_farm set status = 1 WHERE id_std = '' AND id_farm = AND id_sub_farm = 

select * from request_sub_farm
where id_sub_farm = 7


select * from sub_farm

select * from 

update request_sub_farm set id_sub_farm = 1 where  id  >=7


select sub_farm.status , sub_farm.id , 
		id_farm , id_sub , 
        sub_farm.name  as 'fname' , 
        DATE_FORMAT(sub_farm.dt, '%d/%m/%Y %T') as 'dt' , 
        DATE_FORMAT(dt_gateway_temp, '%Y-%m-%d %T') as 'temp' , 
        DATE_FORMAT(dt_gateway_soil, '%Y-%m-%d %T') as 'soil' , 
        DATE_FORMAT(dt_gateway_water, '%Y-%m-%d %T') as 'water', 
        DATE_FORMAT(dt_gateway_fog, '%Y-%m-%d %T') as 'fog' , 
        DATE_FORMAT(dt_gateway_lamp, '%Y-%m-%d %T') as 'lamp' 
 
from  sub_farm

select sub_farm.status , sub_farm.id , id_farm , id_sub , sub_farm.name  as 'fname', event , user.name , DATE_FORMAT(sub_farm.dt, '%d/%m/%Y %T') as 'dt' , status  , DATE_FORMAT(dt_gateway_temp, '%Y-%m-%d %T') as 'temp' , DATE_FORMAT(dt_gateway_soil, '%Y-%m-%d %T') as 'soil' , DATE_FORMAT(dt_gateway_water, '%Y-%m-%d %T') as 'water', DATE_FORMAT(dt_gateway_fog, '%Y-%m-%d %T') as 'fog' , DATE_FORMAT(dt_gateway_lamp, '%Y-%m-%d %T') as 'lamp' from sub_farm   
left join user on user.id = sub_farm.user_event where  id_farm = 1
